--
-- Author: msdnet
-- Date: 2014-03-13 19:43:23
-- 
local ConfigData = class("ConfigData")
 
function ConfigData:ctor() 
	
end      

-- function ConfigData.checkCode(str,bShowMsg)
-- 	bShowMsg = ( (bShowMsg~=false) and true ) or false  
-- 	if(#str<6) then
-- 		if(bShowMsg) then managers.AlertManager:showCenterMsgTip('密碼長度不能小於６位') end;
-- 		return false;--小于六位
-- 	end
-- 	if( ConfigData.isChinese(str)) then
-- 		if(bShowMsg) then managers.AlertManager:showCenterMsgTip('密碼中不能有漢字') end;
-- 		return false;
-- 	end
-- 	if(ConfigData.isCharAndNum(str)==false) then
-- 		if(bShowMsg) then managers.AlertManager:showCenterMsgTip('密碼必須以數字和非數字組合') end;
-- 		return false;--数字和字母
-- 	end
-- 	if(ConfigData.isContinue(str,1,4)==true or ConfigData.isContinue(str,-1,4)==true) then
-- 		if(bShowMsg) then managers.AlertManager:showCenterMsgTip('密碼中不能有４位連續數字或字母組合,比如1234,4321,abcd,dcba') end;
-- 		return false;--连接递增
-- 	end
-- 	if(ConfigData.isContinue(str,0,4)==true) then
-- 		if(bShowMsg) then managers.AlertManager:showCenterMsgTip('密碼中不能有４位連續相同字符串組合,比如1111,aaaa,bbbb') end;
-- 		return false;--连接相同
-- 	end
-- 	return true;
-- end 

function ConfigData.isContinue(str,ref,max)
	local tmp=1;
	local fstCode=0;
	ref = ref or 1
	max = max or 4 
    local i = 1  
    while true do
        c = string.sub(str,i,i)
        if fstCode == 0 then
        	fstCode = string.byte(c) 
        else
        	local now = string.byte(c) 
			if((fstCode+ref)==now and ( ConfigData.isNumber( c ) or ConfigData.isLetter(c)) ) then
				tmp = tmp+1;
				fstCode=fstCode + ref;
				if(tmp>=max) then
					return true;
				end
			else
				tmp=1;
				fstCode=now;
			end
        end 
        b = string.byte(c) 
        if b > 128 then 
            i = i + 3 
        else  
            i = i + 1 
        end   
        if i > #str then 
            break 
        end 
    end	 
	return false;
end
function ConfigData.isCharAndNum(str)
	local hasNum=false;
	local hasChar=false;
    local i = 1  
    while true do
        c = string.sub(str,i,i) 
        b = string.byte(c) 
        if b > 128 then 
            i = i + 3 
        else  
            i = i + 1 
        end  
		if( ConfigData.isNumber(c)) then hasNum=true end;
		if( ConfigData.isChar(c)) then hasChar=true end;        
        if i > #str then 
            break 
        end 
    end
	return hasNum and hasChar;
end
function ConfigData.isNumber(str)
	return string.byte(str)>=48 and string.byte(str) <=57;
end
function ConfigData.isChar(str)
	return not ConfigData.isNumber(str);
end
function ConfigData.isLetter(str)
	return ( string.byte(str) >=65 and string.byte(str) <=90) or ( string.byte(str) >=97 and string.byte(str)<=122);
end 

local function testStr(str1 )
    str1 = "OK 我赢了" 
    i = 1  
    while true do
        c = string.sub(str1,i,i) 
        b = string.byte(c) 
        if b > 128 then
            print(string.sub(str1,i,i+2))
            i = i + 3 
        else 
            if b == 32 then 
                print("empty") 
            else 
                print(c)
            end 
            i = i + 1 
        end 

        if i > #str1 then 
            break 
        end 
    end 
end

function testChineStr ( str )
    local len  = #str
    local left = 0
    local arr  = {0, 0xc0, 0xe0, 0xf0, 0xf8, 0xfc}
    local t = {}
    local start = 1
    local wordLen = 0
    while len ~= left do
        local tmp = string.byte(str, start)
        local i   = #arr
        while arr[i] do
            if tmp >= arr[i] then
                break
            end
            i = i - 1
        end
        wordLen = i + wordLen
        local tmpString = string.sub(str, start, wordLen)
        start = start + i
        left = left + i
        t[#t + 1] = tmpString
    end
    return t
end

-- http://anyexxx.diandian.com/post/2013-07-30/40053147587
-- http://ideone.com/bOpT5p --计算字符串宽度 
function ConfigData.isChinese(str) 
	-- return testChineStr(str)
    local i = 1
    while true do
        c = string.sub(str,i,i) 
        b = string.byte(c) 
        if b > 128 then  
        	-- local cc = string.sub(str,i,i+2)
         --    i = i + 3   
            -- print("···ss",cc,string.byte(cc) )
            -- if ( string.byte(cc) > 127) then
           		return true
            -- end
        else  
        	-- print("..s",string.byte(c))
            i = i + 1 
        end
        if i > #str then 
            break 
        end 
    end 
	return false;
end 
return ConfigData 