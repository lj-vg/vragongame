--语言包配置
-- Author: anjun
-- Date: 2014-07-24 23:01:21
--
local LanuageConfig = {} 

-- tips 提示语  
-- 
LanuageConfig.LoginDatabaseFail = {}
LanuageConfig.LoginDatabaseFail[0] = '驗證失敗，請重試！！！'
LanuageConfig.LoginDatabaseFail[-9999] = '網絡出錯,請重試!!!'
LanuageConfig.LoginDatabaseFail[-888] = '客戶端版本需要更新,請關閉程序重新打開！！！'
LanuageConfig.LoginDatabaseFail[-1] = '用戶帳號被停用！！！'
LanuageConfig.LoginDatabaseFail[-100] = '登入時間限制,請稍後再試'

LanuageConfig.LoginDatabaseFail[-9] = '連接超時,請重試'
LanuageConfig.LoginDatabaseFail[-2] = '上級已經被限制，因此\n您也不能進入，請聯系\n您的上級'
LanuageConfig.LoginDatabaseFail[-9998] = '網絡數據出錯!!!'

LanuageConfig.LoginDatabaseFail[-101] = '過帳中,登入限制!!!'
LanuageConfig.LoginDatabaseFail[-100] = '登入時間限制,請稍後再試'
LanuageConfig.LoginDatabaseFail[-3] = 'IP登入限制!!!'
LanuageConfig.LoginDatabaseFail[-200] = '驗證失敗，請確認站點是\n否正確！！！'
 
return LanuageConfig
