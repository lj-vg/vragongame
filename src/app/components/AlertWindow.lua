--弹出窗口类 
-- Author: anjun
-- Date: 2014-01-26 19:00:51
--
local Window = import(".Window")
local AlertWindow = class("AlertWindow", Window) 

function AlertWindow:ctor(options)   
	self.okButton_ = nil
    self.cancelButton_ = nil 
    self.buttonToBottom_ = 30
    self.buttonGape_ = 50
    self.showOK_ = false
	self.showCancel_ = false
	self.callBackFunction_ = nil 
	AlertWindow.super.ctor(self,options)
	self._titleContentGap = 0
end	

function AlertWindow:setCallBackFunction(value) 
	self.callBackFunction_ = value
 	return self
end 
		
function AlertWindow:setCancelButton(value) 
  	if ( self.cancelButton_ ) then 
		self.cancelButton_:removeSelf()
		self.cancelButton_ = nil 		
 	end 
	self.cancelButton_ = value;
	self.showCancel_ = true;
	if (self._isInitial) then 
		self:addContentChild( self.cancelButton_ )
	 	self:invalidate()
	end  	 	
 	self.cancelButton_:addNodeEventListener(cc.NODE_TOUCH_EVENT,handler(self, self.__cancelTouch)) 
 	return self
end

function AlertWindow:setOkButton(value) 
  	if ( self.okButton_ ) then
		self.okButton_:removeSelf()
		self.okButton_ = nil 		
 	end 
	self.okButton_ = value;
	self.showOK_ = true;
	if (self._isInitial) then
		self:addContentChild( self.okButton_ ) 
	 	self:invalidate()
	end
 	self.okButton_:addNodeEventListener(cc.NODE_TOUCH_EVENT,handler(self, self.__okTouch)) 
 	return self
end 
		
function AlertWindow:setShowOK(value) 
	if ( self.showOK_ == value) then return self end
	self.showOK_ = value;
	if (self.okButton_) then			
		if ( not self.showOK_ ) then
			self.contentSprite_:removeChild( self.okButton_);
		else
			if (self._isInitial) then
				self:addContentChild( self.okButton_ ) 
			end			
		end
	end
 	self:invalidate()  	
 	return self
end

function AlertWindow:setShowCancel(value)  
	if ( self.showCancel_ == value) then return self end
	self.showCancel_ = value;
	if (self.cancelButton_) then			
		if (not self.showCancel_ ) then 
			self.contentSprite_:removeChild( self.cancelButton_);
		else 
			if (self._isInitial) then
				self:addContentChild( self.cancelButton_ )
			end	
			
		end
	end
 	self:invalidate()  	
 	return self
end
 	
 -- 确定与取消之间的距离 
function AlertWindow:setButtonGape(value) 
	if ( self.buttonGape_ == value) then return self end
	self.buttonGape_ = value;
 	self:invalidate()  	
 	return self
end

 -- 按纽到底部的距离 
function AlertWindow:setButtonToBottom(value) 
	if ( self.buttonToBottom_ == value) then return self end
	self.buttonToBottom_ = value; 
 	self:invalidate()  	
 	return self
end 
		
function AlertWindow:addChildren() 
	AlertWindow.super.addChildren(self);  
	if ( self.okButton_ and self.showOK_) then 	
		self:addContentChild( self.okButton_ )	
	end
	if (self.cancelButton_ and self.showCancel_) then 
		self:addContentChild( self.cancelButton_ )	
	end
	self:addEventListener(Window.CLOSE, handler(self, self.__alertClose) ); 	
end 

function AlertWindow:__cancelTouch( event) 
    if event.name == "began" then
        return true
    elseif event.name == "ended" then   
		if (self.callBackFunction_) then 
			self.callBackFunction_( {self,false} )
		end	
		if (self) then
			self:closeWindow()
		end		
    end  
end

function AlertWindow:__okTouch(event)   
    if event.name == "began" then
        return true
    elseif event.name == "ended" then  
		if (self.callBackFunction_) then 
			self.callBackFunction_( {self,true} )
		end	 
		self:closeWindow() 
    end  
end 

function AlertWindow:__alertClose(event)    
	if (self.callBackFunction_) then 
		self.callBackFunction_( {self,false} )
	end		    
	self:closeWindow()	
end
		
function AlertWindow:render() 
	AlertWindow.super.render( self);  
end 
 
function AlertWindow:fixPos() 
	AlertWindow.super.fixPos( self); 
	local x,y   
    if (self.showOK_ and self.showCancel_ ) then 	--2个按纽同时存在
    	y = self.buttonToBottom_ + self.okButton_:getContentSize().height/2 
    	x = self.contentSprite_:getContentSize().width - self.cancelButton_:getContentSize().width - self.okButton_:getContentSize().width
		x = (x-self.buttonGape_)/2 
		self.cancelButton_:align(display.BOTTOM_LEFT , x, y)  
		-- right btn
		x = x +self.cancelButton_:getContentSize().width+ self.buttonGape_ 
		self.okButton_:align(display.BOTTOM_LEFT , x, y)  
    elseif self.showOK_ then   						--  only ok btn 
		x =  (self.contentSprite_:getContentSize().width  - self.okButton_:getContentSize().width)/2
		y = self.buttonToBottom_
		self.okButton_:align(display.BOTTOM_LEFT,x,y)  
	elseif self.showCancel_ then   					-- only cancel btn 
		x = (self.contentSprite_:getContentSize().width  - self.cancelButton_:getContentSize().width)/2 
		y = self.buttonToBottom_
		self.cancelButton_:align(display.BOTTOM_LEFT,x,y)   
    end
end
		
function AlertWindow:dispose()
	AlertWindow.super.dispose( self);
end

return AlertWindow