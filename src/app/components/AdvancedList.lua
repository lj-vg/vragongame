--高級ＬＩＳＴ表格
-- Author: anjun
-- Date: 2014-08-11 13:51:46
-- 
local ComboxItem = require("app.scenes.items.ComboxItem") --test

local AdvancedList = class("AdvancedList", function()
	return display.newNode()
end)

function AdvancedList:ctor(option)
    self._dataSource = {} 
	self._listSize = option.listSize or CCSize(display.width,display.height) 
	self._itemSize = option.itemSize or CCSize( self._listSize.width,58)
	self._item = option.item or ComboxItem
	self._isMultipleSelect = option.isMultipleSelect or false 
	self._callBack = option.callBack or nil 
	self:setDataSource( option.info )
	self._listPos = option.listPos or nil 

	self._buttonSize = option.buttonSize or CCSize(100, 50) 
	self._currInfo = option.defaultInfo or nil 
    self._enable = true
end  

function AdvancedList:initialization( ) --初始化  
	self:initView()  
    self:initEvent() 	
end

function AdvancedList:initView()      
	local fontSize = 30
    self._buttonItem = cc.ui.UIPushButton.new("common/combox_input.png", {scale9 = true})
        :setButtonSize(self._buttonSize.width, self._buttonSize.height)
        :setButtonLabel(ui.newTTFLabel({
            text = "",
            size = fontSize,
            color = display.COLOR_BLACK
        }))
        :onButtonPressed(function(event) 
        end)
        :onButtonRelease(function(event) 
        end)
        :onButtonClicked(function(event) 
            if (self._enable) then
                self:show()
            end 
        end)        
        :addTo(self)	

    self._buttonItem:setContentSize( self._buttonSize ) 
    self:updateButtonLabel()
    self._buttonItem:align(display.BOTTOM_LEFT,0,0 )   

	-- self:createItemList()
end

function AdvancedList:enable( flag ) 
    self._enable = flag
    self._buttonItem:setButtonEnabled(flag)
end

function AdvancedList:updateButtonLabel(txt)
	if (not self._buttonItem) then return end 
	local fontSize = 30
	txt = txt or ""
    local txtSize,txt = extendUI.Common.TestStringSize(txt,fontSize,self:getContentSize().width)
    self._buttonItem:setButtonLabelString( txt )
end

function AdvancedList:getContentSize() 
	return self._buttonItem:getContentSize()
end

function AdvancedList:__itemListCallBack(info)    
    self._currInfo = info
	if self._callBack then
		self._callBack(info)
	end 
    -- self:enableScrollTableView(true) 
    -- self._buttonItem:setButtonLabelString( info[1].value ) 
end

function AdvancedList:createItemList()  
    local option = {info=self:getDataSource(),size=self._listSize,itemSize =self._itemSize,Item=self._item,isMultipleSelect = self._isMultipleSelect}
    self._itemList = extendUI.CustomListView.new(option )
    self._itemList:getListView():setDirection(kCCScrollViewDirectionVertical) 
    local pos = self:getListPos()
    self._itemList:align(display.BOTTOM_LEFT,pos.x,pos.y )   
    self._itemList:getListView():reloadData()
    self._itemList:setCallBack( handler(self, self.__itemListCallBack)  )   
    -- self._itemList:addTo(self)
    managers.LayerManager:addChildToLayer(UI_LAYER,self._itemList,false,false ) 
    self._itemList:setVisible(true)
end

function AdvancedList:getListValue() 
	return self._currInfo 
end

function AdvancedList:setListValue(value) 
	self._currInfo = value 
end

function AdvancedList:getListPos()
	if self._listPos then return self._listPos end  
    local pos =   self._buttonItem:getParent():convertToWorldSpace( self._buttonItem:getPositionInCCPoint() ) 
    return CCPoint(display.left, pos.y-self._listSize.height ) 
end

function AdvancedList:show()  
	if (not  self._itemList ) then
		self:createItemList()
	end
    if ( (not self._dataSource) or (#self._dataSource == 0) ) then 
        self._itemList:show(false,false) 
    else
        self._itemList:show(true,true) 
    end      
    -- self:enableScrollTableView(false)
    self._itemList.currSelectInfo = ( self._currInfo or {} ) 
    self._itemList:updateListSelectLight() 	 
end

function AdvancedList:hide()
	self._itemList:hide()
	-- managers.LayerManager:removeChildFromLayer(self)
end

function AdvancedList:initEvent()  
end 

function AdvancedList:setDataSource(data) --設置數據源
	self._dataSource = clone( data  )
    self:update()  
end

function AdvancedList:update()
    if(self._itemList) then
        self._itemList:getListView():reloadData()
    end 
end

function AdvancedList:getDataSource()
    return self._dataSource
end 
 

return AdvancedList 


