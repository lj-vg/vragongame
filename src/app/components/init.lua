--- init components package 
-- Author: anjun
-- Date: 2014-01-26 10:31:12
--
Common = import(".Common")
TextField = import(".TextField")
scheduler = require("framework.scheduler") 
Window = import(".Window")
AlertWindow = import(".AlertWindow")
-- extendUI.NormalWindow = import(".NormalWindow")
-- extendUI.NormalWindow = import(".CommonWindow")
-- extendUI.CustomDataWindow = import(".CustomDataWindow")
-- extendUI.NormalAlertWindow = import(".NormalAlertWindow")
-- extendUI.NormalAlertPanle = import(".NormalAlertPanle")

-- extendUI.IconButton = import(".IconButton")
-- extendUI.IconTextButton = import(".IconTextButton") 
-- extendUI.TableView  = import(".TableView")  
-- extendUI.ExtendSlider  = import(".ExtendSlider") 
-- extendUI.ProgressBar = import(".ProgressBar")

-- extendUI.Item  = import(".Item")
-- extendUI.CocosItem  = import(".CocosItem")
-- extendUI.ExtendUIBoxLayout = import(".ExtendUIBoxLayout")
-- extendUI.TabGroup  = import(".TabGroup")
-- extendUI.SelectButton  = import(".SelectButton") 
-- extendUI.SelectIconButton  = import(".SelectIconButton")
 
-- --  
-- extendUI.CustomTableView  = import(".CustomTableView") 
-- extendUI.CustomTableViewTitle  = import(".CustomTableViewTitle")
-- extendUI.CustomDataGrid  = import(".CustomDataGrid")
-- extendUI.DrawNode  = import(".DrawNode")
-- extendUI.ListView  = import(".ListView")
-- extendUI.CustomListView  = import(".CustomListView")
-- extendUI.AdvancedList = import(".AdvancedList")
-- extendUI.ScrollView  = import(".ScrollView")
-- extendUI.CustomScrollView  = import(".CustomScrollView")
-- extendUI.AdvancedGrid = import(".AdvancedGrid")
-- extendUI.SlipButton = import(".SlipButton")
-- extendUI.Stepper = import(".Stepper")
-- extendUI.LabelStepper = import(".LabelStepper")
-- extendUI.GameItemNameList = import(".GameItemNameList")

return Common,TextField,Window,AlertWindow
