--高級網格
-- Author: anjun
-- Date: 2014-08-02 00:21:22
--
local DataItem = require("app.scenes.items.DataItem")
local Titleitems = require("app.scenes.items.Titleitems")

local AdvancedGrid = class("AdvancedGrid", function()
	return display.newNode()
end)

function AdvancedGrid:ctor(option)
	local size = option.size or CCSize(0,0)
	self:setContentSize(size)
	self._lTitleFieldName = option.lTitleFieldName or {}
	self._rTitleFieldName = option.rTitleFieldName or {} 
	self._lItemFieldName = option.lItemFieldName or {}
	self._rItemFieldName = option.rItemFieldName or {}
	self._lTitleItem = Titleitems.new() --左抬頭
	self._rTitleItem = Titleitems.new() --右抬頭
	self._rTitleItem._type = "R"
	self.LItem = DataItem 				--左數據item
	self.RItem = DataItem 				--右數據item
    self._countSprite = nil  --全計欄 
	self._dataSource = {}
    self._amountData= {}
    self._functiondata={}  --回調方法字段集
end  

function AdvancedGrid:initialization( gridConfig ) --初始化
	if ( gridConfig ) then
		local lTitleFieldName = {}
		local rTitleFieldName = {}

		local lItemFieldName = {}
		local rItemFieldName =  {}

		for i=1,#gridConfig do  
			local info = gridConfig[i]
            info.visible = ( (info.visible~=false) and true ) or false 
			if (info.visible) then
			    if info.direction == "L" then
				    lTitleFieldName[#lTitleFieldName+1] = info
				    lItemFieldName[#lItemFieldName+1] = info
			    elseif info.direction == "R" then 
				    rTitleFieldName[#rTitleFieldName+1] = info
				    rItemFieldName[#rItemFieldName+1] = info
			    end
                if (info.labelFunction) then --回調
                    self._functiondata[info.fieldName] = info 
                end
                if (info.amount or info.amountLabel ) then --合計欄
                    self:parseAmount(info)
                end    
                if ( not self._isCountID ) then 
                    self._isCountID = info.fieldName=='LocalID' or false --自增長     
                end 
			end 
		end
		self:setLTitleFieldName(lTitleFieldName)
		self:setRTitleFieldName(rTitleFieldName)
		self:setLItemFieldName( lItemFieldName )
		self:setRItemFieldName( rItemFieldName )
	end
	self:initView()  
    self:initEvent() 	
end

function AdvancedGrid:initView()    
	local size = self:getContentSize()
    local countSpH = 0--FinanceUtils.LTitleItemSize.height
    self._grid = extendUI.CustomDataGrid.new(CCSize(size.width,size.height-countSpH),handler(self, self.__gridHandle),self._lTitleItem,self._rTitleItem )
    self:addChild( self._grid)   
    self._grid:align(display.BOTTOM_LEFT,0,countSpH) 
    -- self._countSprite = display.newScale9Sprite("common/listbackground.png",0,0,CCSize(size.width, countSpH ) ) --全計欄
    --                     :addTo(self)
    --                     :align(display.BOTTOM_LEFT,0, 0) 
end

function AdvancedGrid:initEvent()  
end 

function AdvancedGrid:setDataSource(data) --設置數據源
	self._dataSource = data
	self:update()
end

function AdvancedGrid:getDataSource()
    return self._dataSource
end

function AdvancedGrid:update() --更新
    self:countID() 
    self:countAmount() 
    self._grid:reloadData()
end
 

function AdvancedGrid:parseAmount(info)  --解析合計欄
    if ( not self:isHasFieldname(info.fieldName) ) then
        self._amountData[#self._amountData+1] = {fieldName =info.fieldName,amount=info.amount,amountLabel = info.amountLabel,labelFunction=info.labelFunction } 
        -- self._amountData[info.fieldName] = {fieldName =info.fieldName,amount=info.amount,amountLabel = info.amountLabel,labelFunction=info.labelFunction }             
    end 
end
function AdvancedGrid:isHasFieldname( fieldname ) 
    local result = false
    for i=1,#self._amountData do 
        local vo = self._amountData[i]
        if (vo.fieldName ==fieldname and fieldname~="" ) then
            result = true
            break;
        end
    end

    return result
end

function AdvancedGrid:countID() --計算自增長ID 
    if not self._isCountID then return   end
    for j=1,#self._dataSource do    
        self._dataSource[j]["LocalID"] = j  
    end     
end

function AdvancedGrid:countAmount() --計算合計欄
    if (#self._amountData == 0 ) then return end

    local tmpVO = {}
    local flag = false
    if (#self._dataSource>1) then  
        tmpVO = table.keys(self._dataSource[1]) --clone( self._dataSource[1] )  
    else
        for k,v in pairs(self._lItemFieldName) do
            tmpVO[#tmpVO+1] = v.fieldName
        end        
        for k,v in pairs(self._rItemFieldName) do 
            tmpVO[#tmpVO+1] = v.fieldName
        end      
        flag = true
    end  
    local amountVO = {} 
    for i=1,#tmpVO do 
        amountVO[tmpVO[i]] = "" 
    end   
    amountVO.ISMY_AMOUNT_TOTAL = true --是否總計行標誌
    
    for i=1,#self._amountData do 
        local v = self._amountData[i]
        local k = self._amountData[i].fieldName   

        for j=1,#self._dataSource do  
            if ( self._dataSource[j][k] ) then 
                if (v.amount) then 
                    flag = true
                    amountVO[k]= toint(amountVO[k]) + toint( self._dataSource[j][k] )
                    -- print("總計:",amountVO[k], toint(amountVO[k]) , toint( self._dataSource[j][k] ) )
                elseif v.amountLabel then
                    amountVO[k]= v.amountLabel  
                    flag = true 
                end  
            else --字段為"" or nil
                if ( v.amountLabel ) then
                    amountVO[k]= v.amountLabel  
                    flag = true                     
                end
            end
        end 
        if (flag and #self._dataSource ==0 ) then --木數據
                if (v.amount) then  
                    amountVO[k]= toint(amountVO[k])
                elseif v.amountLabel then
                    amountVO[k]= v.amountLabel   
                end 
        end
    end

    if flag then 
        self._dataSource[#self._dataSource+1] = amountVO  
    end 
end 

function AdvancedGrid:setLTitleFieldName( fieldnames ) --設置左抬頭字段
	self._lTitleFieldName = fieldnames
	if (self._lTitleItem) then
		self._lTitleItem:setTitleLabl( fieldnames )
	end
end 
function AdvancedGrid:setRTitleFieldName( fieldnames ) --設置右抬頭字段
	self._rTitleFieldName = fieldnames
	if (self._rTitleItem) then
		self._rTitleItem:setTitleLabl( fieldnames )
	end
end 

function AdvancedGrid:setLItemFieldName( fieldnames ) --設置左item字段＋渲染
	self._lItemFieldName = fieldnames  
end 
function AdvancedGrid:setRItemFieldName( fieldnames )--設置右item字段＋渲染
	self._rItemFieldName = fieldnames
end 

function AdvancedGrid:refreshCells()   --刷新cells
    local cellL,cellR
    local itemL,itemR
    local index = 1
    for i=1,#self._dataSource do 
        cellL,cellR = self._grid:gridCellAtIndex(i-1) 
        if cellR then
            local itemR = cellR:getChildByTag(123) 
            itemR:refresh() 
        end
        if cellL then
            local itemL = cellL:getChildByTag(123) 
            itemL:refresh()             
        end
    end
end
 
function AdvancedGrid:__gridHandle(type,fn, table, a1, a2,a3) 
    local function _handleCell()
        local item = nil
        local result = nil
        if not a2 then
            a2 = CCTableViewCell:new()  
            result = a2
            if (type == "L") then   --左邊
                item = self.LItem.new()
                item:setFieldName( self._lItemFieldName ) 
            elseif (type == "R" ) then  --右邊
                item = self.RItem.new() 
                item._type = "R"
                item:setFieldName( self._rItemFieldName )
            end 
            a2:addChild(item,0,123) 
            item:setAnchorPoint(CCPoint(0,0)) 
        end

        item = a2:getChildByTag(123)
        if nil ~= item then 
            if (self._dataSource[a1+1]) then 
                item:update( self._dataSource[a1+1] )
            end
        else
            a2 = CCTableViewCell:new()  
            result = a2
            if (type == "L") then   --左邊
                item = self.LItem.new()
                item:setFieldName( self._lItemFieldName )
            elseif (type == "R" ) then  --右邊
                item = self.RItem.new() 
                item._type = "R"
                item:setFieldName( self._rItemFieldName )
            end 
            a2:addChild(item,0,123) 
            item:setAnchorPoint(CCPoint(0,0))
            if (self._dataSource[a1+1]) then 
                item:update( self._dataSource[a1+1] ) 
            end
        end   
        return result;   
    end

    local r
    if fn == "cellSize" then  
        if type=="L" then
            local h = FinanceUtils.LTitleItemSize.height
            local w = FinanceUtils.LTitleItemSize.width 
            if self._lItemFieldName[a1+1] then 
                w = self._lItemFieldName[a1+1].width
            end
            r = CCSizeMake(w,h)       
        elseif type=="R" then 
            local h = FinanceUtils.RTitleItemSize.height
            local w = 0
            for i=1,#self._rItemFieldName do 
                if self._rItemFieldName[i].visible then
                    w = w+self._rItemFieldName[i].width
                end 
            end             
            r = CCSizeMake(w,h)
        end  
    elseif fn=="scroll" then
    elseif fn == "cellAtIndex" then 
        local cell = _handleCell()     
        r = a2
    elseif fn == "numberOfCells" then 
        r = ( #self._dataSource)    -- 
    -- 表格事件：
    elseif fn == "cellTouched" then         -- A cell was touched, a1 is cell that be touched. This is not necessary.
    elseif fn == "cellTouchBegan" then      -- A cell is touching, a1 is cell, a2 is CCTouch
    elseif fn == "cellTouchEnded" then      -- A cell was touched, a1 is cell, a2 is CCTouch
    elseif fn == "cellHighlight" then       -- A cell is highlighting, coco2d-x 2.1.3 or above
    elseif fn == "cellUnhighlight" then     -- A cell had been unhighlighted, coco2d-x 2.1.3 or above
    elseif fn == "cellWillRecycle" then     -- A cell will be recycled, coco2d-x 2.1.3 or above
        -- print("a1",a1,a2,a3) 
    end
    return r    
end

function AdvancedGrid:enableControl( flag ) 
    if self._grid then
        self._grid:enableControl(flag)
    end
    
end 

function AdvancedGrid:isScrolling() 
    return  self._grid:isScrolling()
end

return AdvancedGrid