--
-- Author: msdnet
-- Date: 2014-03-26 17:28:30
--
local Utils={}  
 
function Utils.DateAdd(datepart,number, date)
	datepart = datepart or ""
	number = number or 0
    if (date == nil)  then
        date = os.date("*t")
    end
    local returnDate = os.date("*t", os.time(date)) 
    datepart = string.lower(datepart)
    assert(datepart == "year"
        or datepart == "month"
        or datepart == "day"
        or datepart == "hour" 
        or datepart == "min"
        or datepart == "sec",
        string.format("Unknown date part, do nothing %s", tostring(datepart)))  
    	local returDateTime = os.time(returnDate)
    	local rateSecod = 0
    	if datepart == "day" then
    		rateSecod = 24*60*60
	        returDateTime = returDateTime + number*rateSecod 
	    	returnDate = os.date("*t", returDateTime)     		
    	elseif datepart == "month" then 
    		returnDate[datepart] = returnDate[datepart] + number
    	end 

    return returnDate;
end

function Utils.StringToDate(_S)
    _S= _S or '2009-01-01'
    local arr= string.split(_S,'-')
    local day = toint(arr[3])
    local month = toint(arr[2])
    local year = toint(arr[1])
    -- print("···",day,month,year)
    rtDate=os.date("*t", os.time({day=day, month=month,year=year, hour=0, min=0, sec=0}))    
    return rtDate;
end

function Utils.DateToString(_D)
    if (_D) then 
    	return os.date("%Y-%m-%d", os.time(_D) )
    end
    return "1970-01-01"; 
end

function Utils.getNumStr(nV,nLen,sRep)
    nLen = nLen or 2
    sRep = sRep or '0'
    local str=tostring( nV ); 
    for i=#str+1,nLen do 
        str=sRep..str;
    end 
    return str;
end

function Utils.getHourMinSec(str) --返回時分秒 
    local index = 1
    local hour = string.sub(str,index,index+1) 
    index = index +2
    local min = string.sub(str,index,index+1) 
    index = index +2
    local sec = string.sub(str,index,index+1)   
    return hour,min,sec
end

function Utils.setHourMinSec(date) --返回時分秒
    date = date or os.date("*t")
    local result = ""
    result = result..Utils.getNumStr(date.hour)
    result = result..Utils.getNumStr(date.min)
    result = result..Utils.getNumStr(date.sec)
    return result
end 

function Utils.getDateTime( date ) --返回时间戳(年，月，日是，时，分，秒)
    date = date or os.date("*t")
    return os.time( date )
end 
function Utils.getDateYearMonthDay(date) --返回时间戳(年，月，日)    
    date = date or os.date("*t")
    date.hour=0
    date.min=0
    date.sec=0
    return os.time( date )
end 


function Utils:getTimeDiff( date,diffsec )  --返回相差时间戳
    date = date or os.date("*t")
    date = os.date("*t", os.time(date)+diffsec ) 
    return os.time( date )
end

function Utils.getCurrWeek()  --本周
    local tDate = os.date("*t")
    local tDate1,tDate2,tDate3;
    local tDay=0; 
    tDate.hour = 0 
    tDate.min = 0
    tDate.sec = 0  
    tDay=tDate.wday; 
 
    tDay= tDay==1 and 7 or tDay-1;
    tDate1=Utils.DateAdd("day",1-tDay,tDate) 
    tDate2=Utils.DateAdd('day',6,tDate1); 

    return {os.time(tDate1),os.time(tDate2)} 
end

function Utils.getLastWeek()  --上周
    local tDate = os.date("*t")
    local tDate1,tDate2,tDate3;
    local tDay=0; 
    tDate.hour = 0 
    tDate.min = 0
    tDate.sec = 0
    tDay=tDate.wday; 
    tDay= tDay==1 and 7 or tDay-1;
    tDate1=Utils.DateAdd('day',1-tDay-7,tDate);
    tDate2=Utils.DateAdd('day',6,tDate1);   
    return {os.time(tDate1),os.time(tDate2)} 
end

function Utils.getCurrMonth()  --本月
    local tDate = os.date("*t")
    local tDate1,tDate2,tDate3;
    local tDay=0; 
    tDate.hour = 0 
    tDate.min = 0
    tDate.sec = 0

    tDate1=os.date("*t", os.time({day=1, month=tDate.month,
        year=tDate.year, hour=0, min=0, sec=0})) 
    tDate2=Utils.DateAdd('month',1,tDate1);
    local tDate3=Utils.DateAdd('day',-1,tDate2);    
    return {os.time(tDate1),os.time(tDate3)}   
end 

function Utils.getLastMonth()  --上月
    local tDate = os.date("*t")
    local tDate1,tDate2,tDate3;
    local tDay=0; 
    tDate.hour = 0 
    tDate.min = 0
    tDate.sec = 0
    tDate1=os.date("*t", os.time({day=1, month=tDate.month,
        year=tDate.year, hour=0, min=0, sec=0})) 
    tDate2=Utils.DateAdd('month',-1,tDate1);
    tDate3=Utils.DateAdd('day',-1,tDate1);   
    return {os.time(tDate2),os.time(tDate3)} 
end
--取字節流長度
function Utils.GetStringLen(s,len)
    len = len or 255
    local index = 1 
    for i=1,len do 
        if(string.byte(s,index) == 0 ) then
            return index -1
        end 
        index = index+1
    end
    return index-1 
end
--取整數部分
function Utils.getIntPart(x)
    if x <= 0 then
       return math.ceil(x);
    end

    if math.ceil(x) == x then
       x = math.ceil(x);
    else
       x = math.ceil(x) - 1;
    end
    return x;
end
function Utils.getString(buffstr,len)
   len = len or 255
   local strlen = Utils.GetStringLen(buffstr,len) ;
   local realstr = string.sub(buffstr,1,strlen) ;
   return realstr
end
  
return Utils