--動畫播放管理器 
-- Author: anjun
-- Date: 2014-04-02 18:54:19
--
local MVManager = class("MVManager")
MVManager.MVManager_instance = nil

function MVManager.getInstance()  
    if(not MVManager_instance) then 
        MVManager_instance = MVManager.new() 
    end
    return MVManager_instance
end

function MVManager:ctor()

	-- 聲音
	self._allSoundsSwitchOn = true  --所有聲音的總開關
	self._soundsVolume = 1 		-- 音效声音音量
	self._isSoundsSwitchOn = true  --音效开关

	self._musicVolume  = 1 		-- 音乐音量
	self._isMusicSwitchOn = false  --音乐开关

	-- 遊戲中
	self._gameAutoBuy = false    -- 遊戲幣為零時自動買入
	self._cardTypeTip = true    -- 牌型提示
end 

--播放動畫
function MVManager:playMV( swfname,position,parent,callBack,time)
	local frames = extendUI.Common.newFrames2( swfname ) 
	local animation = display.newAnimation(frames, 0.5 / 12) -- 0.5 秒播放 8 帧
	position = position or ccp(0, 0)
	local sp = display.newSprite()
	if (not parent) then
		managers.LayerManager:addChildToLayer(UI_LAYER, sp )
	else
		parent:addChild(sp)
	end  
	sp:pos(position.x, position.y)
	time = time or 0
	-- local action = sp:playAnimationOnce(animation,true,function()
	-- 			        sp:removeSelf() -- 从父对象中删除自己  
	-- 			        sp = nil 
	-- 			        if (callBack) then
	-- 			        	callBack()
	-- 			        end
	-- 			end,0)

if ( time >0 ) then
	transition.playAnimationForever(sp,animation)
	local function __callBack() 
		if (sp) then
	 		sp:stopAllActions()
	        sp:removeSelf()  
		end
        sp = nil 
        if (callBack) then
        	callBack()
        end
	end
	extendUI.scheduler.performWithDelayGlobal( __callBack ,time)
else
	local action = sp:playAnimationOnce(animation,true,function()
				        sp:removeSelf() -- 从父对象中删除自己  
				        sp = nil 
				        if (callBack) then
				        	callBack()
				        end
				end,0) 
end


end

-- 移动动画
function MVManager:moveMV(filename,fromPos,toPos,callBack)
	local sprite = display.newSprite(filename)
	managers.LayerManager:addChildToLayer(UI_LAYER, sprite )
	sprite:pos(fromPos.x, fromPos.y)

	transition.execute(sprite, CCMoveTo:create(1.5, toPos), {
	    delay = 0,
	    easing = "In",
	    onComplete = function()
			sprite:removeSelf() -- 从父对象中删除自己  
			sprite = nil	    
			if (callBack) then
				callBack()
			end
	    end,
	})
end

-- -------------------------------------声音-------------------------------
-- 設置遊戲所有聲音開關
function MVManager:setAllSoundsSwitchOn( flag )
	self._allSoundsSwitchOn = flag
	if ( not self._allSoundsSwitchOn ) then
		self:stopBackMusic()
	end
end

-- 设置游戏音效开关
function MVManager:setSoundsSwitchOn( flag )
	self._isSoundsSwitchOn = flag
	if ( not self._isSoundsSwitchOn ) then
		self:stopEffectSound()
	end
end
-- -- 设置背景音乐开关
function MVManager:setMusicSwitchOn( flag )
	self._isMusicSwitchOn = flag
	if ( not self._isMusicSwitchOn ) then
		self:stopBackMusic()
	else
		self:playBackMusic()
	end
end
-- 设置背景音乐音量
function MVManager:setMusicVolume( val )
	self._musicVolume = val
	audio.setMusicVolume(val)
end  
-- 设置游戏音效音量
function MVManager:setGameSoundVolume( val )
	self._soundsVolume = val
	audio.setSoundsVolume(val)
end

-- get 
function MVManager:getAllSoundsSwitchOn()
	return self._allSoundsSwitchOn
end

function MVManager:getSoundsVolume()
	return self._soundsVolume
end
function MVManager:getIsSoundsSwitchOn()
	return self._isSoundsSwitchOn
end
function MVManager:getMusicVolume()
	return self._musicVolume
end
function MVManager:getIsMusicSwitchOn()
	return self._isMusicSwitchOn
end 

-- 播放游戏音效
function MVManager:playEffectSound(audioName) 
	-- if ( not self._allSoundsSwitchOn) then return end

	-- audioName=audioName or AUDIO__EFFECT1_WAV
	-- if (self._isSoundsSwitchOn) then
	-- 	audio.playSound(audioName) 
	-- 	-- log("playEffectSound:"..audioName)
	-- end
end
function MVManager:stopEffectSound()
	audio.stopAllSounds()
end

--播放游戏背景音乐
function MVManager:playBackMusic() 
	if ( not self._allSoundsSwitchOn) then return end

    if (self._isMusicSwitchOn) then
    	audio.playMusic( AUDIO__BACKGROUND_MP3,true)
    end
end
function MVManager:stopBackMusic()
	audio.stopMusic()
end
-- -------------------------------------声音end-------------------------------

-- game set start
function MVManager:setGameAutoBuy( val )
	self._gameAutoBuy = val 
end 

function MVManager:getGameAutoBuy()
	return self._gameAutoBuy
end

-- 
function MVManager:setCardTypeTip( val )
	self._cardTypeTip = val 
end

function MVManager:getCardTypeTip()
	return self._cardTypeTip
end
 
-- game set end

return MVManager 