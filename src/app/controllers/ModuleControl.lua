--
-- Author: msdnet
-- Date: 2014-01-22 13:54:24
--
local ModuleControl =class('ModuleControl')

function ModuleControl:ctor()
  -- 模塊緩存集
   self.objects_ = {}  
end

function ModuleControl:addModule(id, object)
    assert(self.objects_[id] == nil, string.format("ModuleControl:setObject() - id \"%s\" already exists", id))
    self.objects_[id] = object
end

function ModuleControl:removeModule(id)   
     if(self.objects_[id]~=nil)then
        self.objects_[id] = nil 
    end 
end

function ModuleControl:getModule(id)
    assert(self.objects_[id] ~= nil, string.format("ModuleControl:getObject() - id \"%s\" not exists", id))
    return self.objects_[id]
end

function ModuleControl:isModuleExists(id)
    return self.objects_[id] ~= nil
end

function ModuleControl:exec(type,parameters)
	 local obj = self.objects_
     for key, value in pairs(obj) do  
     	if self:isModuleExists(key) then
             self:getModule(key):exec(type,parameters)
     	end
     end
end	

return ModuleControl