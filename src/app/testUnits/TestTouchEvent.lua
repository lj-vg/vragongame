--测试触摸事件
-- Author: anjun
-- Date: 2014-09-23 11:46:01
--

function createTouchableSprite(p)
    local sprite = display.newScale9Sprite(p.image)
    sprite:setContentSize(p.size)

    local cs = sprite:getContentSize()
    local label = cc.ui.UILabel.new({
            UILabelType = 2,
            text = p.label,
            color = p.labelColor})
    label:align(display.CENTER)
    label:setPosition(cs.width / 2, label:getContentSize().height)
    sprite:addChild(label)
    sprite.label = label

    return sprite
end

function createSimpleButton(imageName, name, movable, listener)
    local sprite = display.newSprite(imageName)

    if name then
        local cs = sprite:getContentSize()
        local label = cc.ui.UILabel.new({
            UILabelType = 2,text = name, color = display.COLOR_BLACK})
        label:setPosition(cs.width / 2, cs.height / 2)
        -- sprite:addChild(label)
    end

    sprite:setTouchEnabled(true) -- enable sprite touch
    -- sprite:setTouchMode(cc.TOUCH_ALL_AT_ONCE) -- enable multi touches
    sprite:setTouchSwallowEnabled(false)
    sprite:addNodeEventListener(cc.NODE_TOUCH_EVENT, function(event)
        local name, x, y, prevX, prevY = event.name, event.x, event.y, event.prevX, event.prevY
        if name == "began" then
            sprite:setOpacity(128)
            -- return cc.TOUCH_BEGAN -- stop event dispatching
            return cc.TOUCH_BEGAN_NO_SWALLOWS -- continue event dispatching
        end

        local touchInSprite = cc.rectContainsPoint(sprite:getCascadeBoundingBox(), cc.p(x, y))
        if name == "moved" then
            sprite:setOpacity(128)
            if movable then
                local offsetX = x - prevX
                local offsetY = y - prevY
                local sx, sy = sprite:getPosition()
                sprite:setPosition(sx + offsetX, sy + offsetY)
                return cc.TOUCH_MOVED_RELEASE_OTHERS -- stop event dispatching, remove others node
                -- return cc.TOUCH_MOVED_SWALLOWS -- stop event dispatching
            end

        elseif name == "ended" then
            if touchInSprite then listener() end
            sprite:setOpacity(255)
        else
            sprite:setOpacity(255)
        end
    end)

    return sprite
end

function drawBoundingBox(parent, target, color)
    local cbb = target:getCascadeBoundingBox()
    local left, bottom, width, height = cbb.origin.x, cbb.origin.y, cbb.size.width, cbb.size.height
    local points = {
        {left, bottom},
        {left + width, bottom},
        {left + width, bottom + height},
        {left, bottom + height},
        {left, bottom},
    }
    local box = display.newPolygon(points, {borderColor = color})
    parent:addChild(box, 1000)
end


-- ---------------------------------------------------------------------------
local TestTouchEvent = class("TestTouchEvent", function()
    return display.newScene("TestTouchEvent")
end)   

function TestTouchEvent:ctor()  
	self:touch2()
end   

function TestTouchEvent:touch1() --"单点触摸测试 - 响应触摸事件"
    -- createTouchableSprite() 定义在 includes/functions.lua 中
    self.sprite = createTouchableSprite({
            image = "TEST/GreenButton.png",
            size = cc.size(500, 300),
            label = "TOUCH ME !",
            labelColor = cc.c3b(255, 0, 0)})
        :pos(display.cx, display.cy)
        :addTo(self)
    drawBoundingBox(self, self.sprite, cc.c4f(0, 1.0, 0, 1.0))

    -- 启用触摸
    self.sprite:setTouchEnabled(true)
    -- 添加触摸事件处理函数
    self.sprite:addNodeEventListener(cc.NODE_TOUCH_EVENT, function(event)
        -- event.name 是触摸事件的状态：began, moved, ended, cancelled
        -- event.x, event.y 是触摸点当前位置
        -- event.prevX, event.prevY 是触摸点之前的位置
        local label = string.format("sprite: %s x,y: %0.2f, %0.2f", event.name, event.x, event.y)
        self.sprite.label:setString(label)

        -- 返回 true 表示要响应该触摸事件，并继续接收该触摸事件的状态变化
        return true
    end) 
end	

function TestTouchEvent:touch2()  --"单点触摸测试 - 事件穿透和事件捕获"
    -- parentButton 是 button1 的父节点
    self.parentButton = createTouchableSprite({
            image = "TEST/GreenButton.png",
            size = cc.size(600, 500),
            label = "TOUCH ME !",
            labelColor = cc.c3b(255, 0, 0)})
        :pos(display.cx, display.cy)
        :addTo(self)
    self.parentButton.name = "parentButton"
    drawBoundingBox(self, self.parentButton, cc.c4f(0, 1.0, 0, 1.0))
    self.parentButton:setTouchEnabled(true)
    self.parentButton:addNodeEventListener(cc.NODE_TOUCH_EVENT, function(event)
        local label = string.format("parentButton: %s x,y: %0.2f, %0.2f", event.name, event.x, event.y)
        self.parentButton.label:setString(label)
        return true
    end)

    -- button1 响应触摸后，会吞噬掉触摸事件
    self.button1 = createTouchableSprite({
            image = "TEST/PinkButton.png",
            size = cc.size(400, 160),
            label = "TOUCH ME !"})
        :pos(300, 300)
        :addTo(self.parentButton)
    cc.ui.UILabel.new({text = "SWALLOW = YES\n事件在当前对象处理后被吞噬", size = 22})
        :align(display.CENTER, 200, 90)
        :addTo(self.button1)
    drawBoundingBox(self, self.button1, cc.c4f(1.0, 0, 0, 1.0))

    self.button1:setTouchEnabled(true)
    self.button1:setTouchSwallowEnabled(true) -- 是否吞噬事件，默认值为 true
    self.button1:addNodeEventListener(cc.NODE_TOUCH_EVENT, function(event)
        local label = string.format("button1: %s x,y: %0.2f, %0.2f", event.name, event.x, event.y)
        self.button1.label:setString(label)
        return true
    end)

    -- button2 响应触摸后，不会吞噬掉触摸事件
    self.button2 = createTouchableSprite({
            image = "TEST/PinkButton.png",
            size = cc.size(400, 160),
            label = "TOUCH ME !"})
        :pos(300, 400)
        :addTo(self.parentButton)
    cc.ui.UILabel.new({text = "SWALLOW = NO\n事件会传递到下层对象", size = 24})
        :align(display.CENTER, 200, 90)
        :addTo(self.button2)
    drawBoundingBox(self, self.button2, cc.c4f(0, 0, 1.0, 1.0))

    self.button2:setTouchEnabled(true)
    self.button2:setTouchSwallowEnabled(false) -- 当不吞噬事件时，触摸事件会从上层对象往下层对象传递，称为“穿透”
    self.button2:addNodeEventListener(cc.NODE_TOUCH_EVENT, function(event)
        local label = string.format("button1: %s x,y: %0.2f, %0.2f", event.name, event.x, event.y)
        self.button2.label:setString(label)
        return true
    end)

    -- 放置一个开关按钮在屏幕上
    local labels = {}
    labels[true] = "父对象【可以】捕获触摸事件"
    labels[false] = "父对象【不能】捕获触摸事件"
    local images = {on = "TEST/CheckBoxButton2On.png", off = "TEST/CheckBoxButton2Off.png"}
    self.captureEnabledButton = cc.ui.UICheckBoxButton.new(images)
        :setButtonLabel(cc.ui.UILabel.new({text = labels[true], size = 24}))
        :setButtonLabelOffset(40, 0)
        :setButtonSelected(true)
        :onButtonStateChanged(function(event)
            local button = event.target
            button:setButtonLabelString(labels[button:isButtonSelected()])
        end)
        :onButtonClicked(function(event)
            local button = event.target
            self.parentButton:setTouchCaptureEnabled(button:isButtonSelected())
        end)
        :pos(display.cx - 160, display.top - 80)
        :addTo(self)

    cc.ui.UILabel.new({
        text = "当不允许父对象捕获触摸事件时，\n父对象及其包含的所有子对象都将得不到触摸事件",
        size= 24})
        :align(display.CENTER, display.cx, display.top - 140)
        :addTo(self)

    -- 
end

function TestTouchEvent:touch3() --"单点触摸测试 - 在事件捕获阶段决定是否接受事件"
   -- 这个标志变量用于在触摸事件捕获阶段决定是否接受事件
    self.isTouchCaptureEnabled_ = true

    -- parentButton 是 button1 的父节点
    self.parentButton = createTouchableSprite({
            image = "TEST/WhiteButton.png",
            size = cc.size(600, 500),
            label = "TOUCH ME !",
            labelColor = cc.c3b(255, 0, 0)})
        :pos(display.cx, display.cy)
        :addTo(self)
    drawBoundingBox(self, self.parentButton, cc.c4f(0, 1.0, 0, 1.0))

    self.parentButton.label2 = cc.ui.UILabel.new({text = "", size = 24, color = cc.c3b(0, 0, 255)})
        :align(display.CENTER, 300, 60)
        :addTo(self.parentButton)

    self.parentButton:setTouchEnabled(true)
    self.parentButton:addNodeEventListener(cc.NODE_TOUCH_EVENT, function(event)
        local label = string.format("parentButton: %s x,y: %0.2f, %0.2f", event.name, event.x, event.y)
        self.parentButton.label:setString(label)
        printf("%s %s [TARGETING]", "parentButton", event.name)
        if event.name == "ended" or event.name == "cancelled" then
            print("-----------------------------")
        else
            print("")
        end
        return true
    end)

    -- 可以动态捕获触摸事件，并在捕获触摸事件开始时决定是否接受此次事件
    self.parentButton:addNodeEventListener(cc.NODE_TOUCH_CAPTURE_EVENT, function(event)
        if event.name == "began" then
            print("-----------------------------")
        end

        local label = string.format("parentButton CAPTURE: %s x,y: %0.2f, %0.2f", event.name, event.x, event.y)
        self.parentButton.label2:setString(label)
        printf("%s %s [CAPTURING]", "parentButton", event.name)
        if event.name == "began" or event.name == "moved" then
            return self.isTouchCaptureEnabled_
        end
    end)

    -- button1 响应触摸后，会吞噬掉触摸事件
    self.button1 = createTouchableSprite({
            image = "TEST/GreenButton.png",
            size = cc.size(400, 160),
            label = "TOUCH ME !"})
        :pos(300, 400)
        :addTo(self.parentButton)
    cc.ui.UILabel.new({text = "SWALLOW = YES\n事件在当前对象处理后被吞噬", size = 24})
        :align(display.CENTER, 200, 90)
        :addTo(self.button1)
    drawBoundingBox(self, self.button1, cc.c4f(1.0, 0, 0, 1.0))

    self.button1:setTouchEnabled(true)
    self.button1:setTouchSwallowEnabled(true) -- 是否吞噬事件，默认值为 true
    self.button1:addNodeEventListener(cc.NODE_TOUCH_EVENT, function(event)
        local label = string.format("button1: %s x,y: %0.2f, %0.2f", event.name, event.x, event.y)
        self.button1.label:setString(label)
        printf("%s %s [TARGETING]", "button1", event.name)
        if event.name == "ended" or event.name == "cancelled" then
            print("-----------------------------")
        else
            print("")
        end
        return true
    end)

    -- button2 响应触摸后，不会吞噬掉触摸事件
    self.button2 = createTouchableSprite({
            image = "TEST/PinkButton.png",
            size = cc.size(400, 160),
            label = "TOUCH ME !"})
        :pos(300, 200)
        :addTo(self.parentButton)
    cc.ui.UILabel.new({text = "SWALLOW = NO\n事件会传递到下层对象", size = 24})
        :align(display.CENTER, 200, 90)
        :addTo(self.button2)
    drawBoundingBox(self, self.button2, cc.c4f(0, 0, 1.0, 1.0))

    self.button2:setTouchEnabled(true)
    self.button2:setTouchSwallowEnabled(false) -- 当不吞噬事件时，触摸事件会从上层对象往下层对象传递，称为“穿透”
    self.button2:addNodeEventListener(cc.NODE_TOUCH_EVENT, function(event)
        local label = string.format("button1: %s x,y: %0.2f, %0.2f", event.name, event.x, event.y)
        self.button2.label:setString(label)
        printf("%s %s [TARGETING]", "button2", event.name)
        return true
    end)

    -- 即便父对象在捕获阶段阻止响应事件，但子对象仍然可以捕获到事件，只是不会触发事件
    self.button2:addNodeEventListener(cc.NODE_TOUCH_CAPTURE_EVENT, function(event)
        printf("%s %s [CAPTURING]", "button2", event.name)
        return true
    end)

    -- 放置一个开关按钮在屏幕上
    local labels = {}
    labels[true] = "父对象【可以】捕获触摸事件"
    labels[false] = "父对象【不能】捕获触摸事件"
    local images = {on = "TEST/CheckBoxButton2On.png", off = "TEST/CheckBoxButton2Off.png"}
    self.captureEnabledButton = cc.ui.UICheckBoxButton.new(images)
        :setButtonLabel(cc.ui.UILabel.new({text = labels[true], size = 24}))
        :setButtonLabelOffset(40, 0)
        :setButtonSelected(true)
        :onButtonStateChanged(function(event)
            local button = event.target
            button:setButtonLabelString(labels[button:isButtonSelected()])
        end)
        :onButtonClicked(function(event)
            local button = event.target
            self.isTouchCaptureEnabled_ = button:isButtonSelected()
        end)
        :pos(display.cx - 160, display.top - 80)
        :addTo(self)

    cc.ui.UILabel.new({
        text = "事件处理流程：\n1. 【捕获】阶段：从父到子\n2. 【目标】阶段\n3. 【传递】阶段：尝试传递给下层对象",
        size= 24})
        :align(display.CENTER_TOP, display.cx, display.top - 120)
        :addTo(self)

    -- 
end

function TestTouchEvent:touch4()  --单点触摸测试 - 容器的触摸区域由子对象决定
    -- touchableNode 是启用触摸的 Node
    self.touchableNode = display.newNode()
    self.touchableNode:setPosition(display.cx, display.cy)
    self:addChild(self.touchableNode)

    -- 在 touchableNode 中加入一些 sprite
    local count = math.random(3, 8)
    local images = {"TEST/WhiteButton.png", "TEST/BlueButton.png", "TEST/GreenButton.png", "TEST/PinkButton.png"}
    for i = 1, count do
        local sprite = display.newScale9Sprite(images[math.random(1, 4)])
        sprite:setContentSize(cc.size(math.random(100, 200), math.random(100, 200)))
        sprite:setPosition(math.random(-200, 200), math.random(-200, 200))
        self.touchableNode:addChild(sprite)
    end

    self.stateLabel = cc.ui.UILabel.new({text = ""})
    self.stateLabel:align(display.CENTER, display.cx, display.top - 100)
    self:addChild(self.stateLabel)

    -- 启用触摸
    self.touchableNode:setTouchEnabled(true)
    -- 添加触摸事件处理函数
    self.touchableNode:addNodeEventListener(cc.NODE_TOUCH_EVENT, function(event)
        local label = string.format("touchableNode: %s x,y: %0.2f, %0.2f", event.name, event.x, event.y)
        self.stateLabel:setString(label)
        return true
    end)
    drawBoundingBox(self, self.touchableNode, cc.c4f(0, 1.0, 0, 1.0))	
end

return TestTouchEvent

 
