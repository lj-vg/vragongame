--Item基类 
-- Author: anjun
-- Date: 2014-03-06 23:05:51
--
local Item = class("Item", function(size)  
    local node = display.newSprite() 
    -- print("--Item基类:",tolua.type(size) )
    if tolua.type(size) == "CCSize" then
        node:setContentSize(size)
    end 
    return node
end)

function Item:ctor(size,info) 	
    self._info = info
    makeUIControl_(self)
    if size then self:setContentSize(size) end 
    require("framework.api.EventProtocol").extend(self)    
    self:setNodeEventEnabled(true)
    self:initiation()
end

function Item:initiation()  
    self:initView()
    self:initEvent()    
end

function Item:initView()

end 

function Item:initEvent()

end

function Item:onTouch(event, x, y)

end

function Item:onTap(x, y)

end

function Item:fixPos() 
end

function Item:update(info)
	self._info = info
end 

function Item:onExit() 
    self:removeAllEventListeners()
end

function Item:width()
    return self:getContentSize().width
end

function Item:height()
    return self:getContentSize().height
end

function Item:onExit()
    self:removeAllEventListeners()
end

function Item:info()
    return self._info
end

return Item