--常用
-- Author: anjun
-- Date: 2014-02-11 14:21:21
--
local sharedDirector         = cc.Director:getInstance()
local sharedSpriteFrameCache = cc.SpriteFrameCache:getInstance()

local Common = {} 
Common.LEFT     = -1
Common.RIGHT    = 1
Common.UP       = 2
Common.DOWN     = -2
Common.CENTER = 3
-- 添加声音按纽
function Common.makeSoundBtn( btn,audioName )  
    btn:onButtonClicked(function(event) 
        managers.MVManager:playEffectSound(audioName)
    end)  
end 

-- 常用btn按下透明一半
function Common.makeNormalBtn( btn )
    btn:onButtonPressed(function(event)
          event.target:setOpacity(128)
    end)
    :onButtonRelease(function(event)
          event.target:setOpacity(255)
    end)
end

function Common.getNormalBtnSize( btn,force) --button Size 真是纠结
    local maxWidth,maxHeight = 1,1
    local btnSize 
    force = ( (force~=false) and true ) or false
    for i,v in ipairs(btn.sprite_) do
        local size = v:getContentSize(); 
        maxWidth = size.width > maxWidth  and size.width or maxWidth  
        maxHeight = size.height > maxHeight  and size.height or maxHeight  
    end
    btnSize = cc.size(maxWidth,maxHeight)
    if force then
        btn:setContentSize( btnSize )
    end
    return btnSize
end

function Common.Spawn(actions) --同时执行
    if #actions < 1 then return end
    if #actions < 2 then return actions[1] end

    local prev = actions[1] 
    for i = 2, #actions do
        prev = cc.Spawn:create(prev, actions[i])
    end
    return prev
end

-- 显示窗体效果
function Common.showWindowEffect( window,posX,posY ) 
    local sequence = transition.sequence({ 
        CCShow:create(), 
        CCFadeIn:create(0.3),
        CCMoveTo:create(0.5, ccp(posX,posY)), 
    })
    transition.execute(window, sequence, args)
end 
-- 隐藏窗体效果
function Common.hideWindowEffect( window,args )
    -- body
    local sequence = transition.sequence({ 
        CCFadeIn:create(0.3), 
    })
    transition.execute(window, sequence, args)
end

-- 判断方向
function Common.getDirection( startPos,endPos) 
    local direction = 0 
    local offsetx = endPos.x - startPos.x
    local offsety = endPos.y - startPos.y 
    -- 1,只考虑上，下，左，右四个方向（可以八方向）
    -- 2,x,y同时有偏移，则依据偏移量大为准
    if ( math.abs(offsetx) > math.abs(offsety) )then 
        if ( offsetx > 0) then
            direction = Common.RIGHT
        elseif(offsetx < 0) then     
            direction = Common.LEFT
        end         
    else
        if ( offsety > 0) then
            direction = Common.UP
        elseif(offsety < 0) then     
            direction = Common.DOWN
        end          
    end   
    return direction,offsetx,offsety
end 

-- 校正offsetX,offsetY归零
function Common.newFrames(pattern, begin, length, isReversed)
    local frames = {}
    local step = 1
    local last = begin + length - 1
    if isReversed then
        last, begin = begin, last
        step = -1
    end

    for index = begin, last, step do
        local frameName = string.format(pattern, index)
        local frame = sharedSpriteFrameCache:spriteFrameByName(frameName)
        -- anjun start
        frame:setOffset(CCPointMake(0,0));
        -- anjun end
        if not frame then
            echoError("Common.newFrames() - invalid frame, name %s", tostring(frameName))
            return
        end

        frames[#frames + 1] = frame
    end 
    return frames
end

--不需要指定长度
function Common.newFrames2(pattern,begin) 
    local index = begin or 1;
    local frames = {}
    repeat      
        local frameName  = string.format( pattern , index); 
        local frame =  sharedSpriteFrameCache:getSpriteFrame(frameName); 
        if(frame == nil) then 
            print("miss newFrames2:",frameName)
            break;
        end
        frame:setOffset(cc.p(0,0)); 
        frames[#frames + 1] = frame 
        index = index + 1;
    until (false);  
    return frames
end
-- 画矩形 调试用
function Common.drawRect(rect,color,borderwidth)
    local shape4 = display.newRect( rect,
        {fillColor = cc.c4f(1,0,0,1), borderColor = cc.c4f(0,1,0,1), borderWidth = 1})
    return shape4
end

function Common.Rand(n,m)
    -- math.randomseed(os.time())
    -- see http://blog.csdn.net/zhangxaochen/article/details/8095007
    math.randomseed(tostring(os.time()):reverse():sub(1, 6)) 
    ----然后不断产生随机数
    return math.random(n,m)
end 

function Common.scaleSize(sprite,orignSize) 
    local boundingSize = sprite:getContentSize()
    local sx = orignSize.width / (boundingSize.width / 1)
    local sy = orignSize.height / (boundingSize.height / 1)
    if sx > 0 and sy > 0 then
        sprite:setScaleX(sx)
        sprite:setScaleY(sy)
    end 
end

function Common.debugDraw(sprite)  
    local posX = sprite:getPositionX()
    local posY = sprite:getPositionY();
    local rectU = Common.drawRect( cc.rect( posX,posY,sprite:getContentSize().width,sprite:getContentSize().height) )
    sprite:getParent():addChild(rectU) 
    local ap = sprite:getAnchorPoint()
    rectU:setAnchorPoint(ap)
end 

function Common.TestStringSize(str,fontSize,maxW)  --那我建议你不要预先设置固定的宽和高，如果你硬要设置，也是有办法的。
                                            -- 你直接创建一个label，不要设置宽高直接获取这个label的宽高，然后再把这个label对象扔掉，这个label对象就单纯用来获取宽度，不用添加到任何节点。
                                            -- 这样虽然可以解决问题，但是label的效率是很低的，希望你有更好的方法。 
    str = tostring(str)                                        
    local testTxt = ui.newTTFLabel({
                text =str,
                size = fontSize, 
            })
    local txtSize =  testTxt:getContentSize()    
    testTxt:release()
    testTxt = nil

    local resutlStr = ""
    if maxW then
        if (txtSize.width >maxW) then 
            local remain = (#str)/(txtSize.width/maxW)   
            local i = 1  
            while true do
                c = string.sub(str,i,i) 
                b = string.byte(c) 
                if b > 128 then 
                    resutlStr = resutlStr..string.sub(str,i,i+2)
                    i = i + 3 
                else 
 
                    resutlStr = resutlStr..c
                    i = i + 1 
                end 

                if i > toint(remain) then 
                    break 
                end 
            end  
            -- str = string.sub(str,1,toint(remain)-2).."..."  --如果是中文字符(２个)，取中文的一半会有乱码　　？
         end        
    end  
    if ( #resutlStr==0 ) then 
        resutlStr = str
    end
    return txtSize,resutlStr                            
end 

function Common.TestStringSize2(str,fontSize,maxW)

    local resutlStr = ""
    if maxW then
        if (txtSize.width >maxW) then 
            local remain = (#str)/(txtSize.width/maxW)   
            local i = 1  
            while true do
                c = string.sub(str,i,i) 
                b = string.byte(c) 
                if b > 128 then 
                    resutlStr = resutlStr..string.sub(str,i,i+2)
                    i = i + 3 
                else 
 
                    resutlStr = resutlStr..c
                    i = i + 1 
                end 

                if i > toint(remain) then 
                    break 
                end 
            end  
            -- str = string.sub(str,1,toint(remain)-2).."..."  --如果是中文字符(２个)，取中文的一半会有乱码　　？
         end        
    end  
    if ( #resutlStr==0 ) then 
        resutlStr = str
    end
    return txtSize,resutlStr                            
end 

--計算字符串長度
function Common.CountStringWidth(str,fontSize)
    str = str 
    fontSize = fontSize or 30
    lenInByte = string.len(str)
    local width = 0
    for i=1,lenInByte do
        local curByte = string.byte(str, i)
        local byteCount = 1;
        if curByte>0 and curByte<=127 then
            byteCount = 1
        elseif curByte>=192 and curByte<223 then
            byteCount = 2
        elseif curByte>=224 and curByte<239 then
            byteCount = 3
        elseif curByte>=240 and curByte<=247 then
            byteCount = 4
        end 
        local char = string.sub(str, i, i+byteCount-1)
        i = i + byteCount -1
        
        if byteCount == 1 then
            width = width + fontSize * 0.5
        else
            width = width + fontSize
            print(char)
        end
    end 
    print("寬度"..width)
end  

return Common