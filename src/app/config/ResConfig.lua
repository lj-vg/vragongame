--资源配置

-- 热更新会用到的……
-- 后续看看有没更好的办法……。^_^
-- Author: anjun
-- Date: 2014-05-25 01:04:30
--
local RES_PATH = makeSysDir().."upd/"

-- new
--  login folder
	LOGIN__LOGIN_BGP_PNG = "login/login_bgp.png"
	LOGIN__JIERI_LOGIN2_PNG = "login/jieri_login2.png"
	LOGIN__JIERI_LOGINBG_PNG ="login/jieri_loginbg.png"
	LOGIN__LOG_PNG = "login/log.png"
	LOGIN__LOGIN_RIGHT_PNG = "login/login_right.png"
	LOGIN__LOGIN_VIRLINE_PNG ="login/login_virline.png"
	LOGIN__LOGIN_RIGHT2_PNG = "login/login_right2.png"
	LOGIN__LOGIN_NOTICE_PNG ="login/LoginTextInput.png" 
	LOGIN__LOGIN_BUTBG_PNG = "login/login_butbg.png"
	LOGIN__LOGIN_SET_PNG = "login/login_set.png" 
	
	COMMON__SETTING_OFF_JPG = "login/switchOff.png"
	COMMON__SETTING_ON_JPG = "login/switchOn.png"	

-- commonAssets folder
	COMMONASSETS__GREEN_EDIT_PNG = "commonAssets/green_edit.png"


--组件常用配置 
	-- button config
	COMMON_BUTTON_YELLOW = "common/common_button_yellow.png"
	COMMON_BUTTON_GREEN = "common/common_button_purple.png"
	COMMON_BUTTON_PURPLE = "common/common_button_purple.png"
	COMMON_BUTTON_EXIT = "common/exit.png"
	COMMON_BUTTON_CLOSE = "common/close.png" 

	-- Window Config
	COMMON_WINDOW_A_BG9 = "common/a_bg.9.png"
	COMMON_WINDOW_A_TITLE9 = "common/a_title.9.png"
	COMMON_WINDOW_A_PANEL9 = "common/a_panel.9.png"

	-- hint 
	ROOM_HINT_BG9 = "common/room_hint_bg.9.png"

	-- game Layer config
	-- DEBUG_LAYER = 10000
	-- UI_LAYER 	= 10001
	-- TOP_LAYER	= 10002

	-- slider compont config   
	COMMON_SLIDER_IMAGES = {
	    bar = "common/SliderBar.png",
	    button = "common/SliderButton.png",
	} 

	-- checkbox compont config
	COMMON_CHECKBOX_IMAGES = {
	    off = "common/chose_no.png",
	    on = "common/chose_yes.png",
	}

	COMMON_RADIO_BUTTON_IMAGES = {
	    on = "common/radio_hall_quan_click.png",
	    -- off_pressed = "RadioButtonOffPressed.png",
	    -- off_disabled = "RadioButtonOffDisabled.png",
	    off = "common/radio_hall_quan.png",
	    -- on_pressed = "RadioButtonOnPressed.png",
	    -- on_disabled = "RadioButtonOnDisabled.png",
	}	 
	COMMON_CHECKBOX_BUTTON_IMAGES = {
	    off = "common/chose_no.png",
	    on = "common/chose_yes.png",
	}
	 
	COMMON_TABBUTTON_CONFIRM_IMAGES = {
	    -- off = "login/button.png",
	    off = "login/button.png",
	    on = "login/buttonHighlighted.png", 
	}

	BUTTON_CONFIRM_IMAGES2 = {
	    normal = "login/button.png",
	    pressed ="login/buttonHighlighted.png", 
	    disabled = "common/button_gray_on.png", 
	}

	BUTTON_CONFIRM_IMAGES = {
	    normal = COMMON_BUTTON_YELLOW,
	    pressed = COMMON_BUTTON_GREEN,
	    disabled = COMMON_BUTTON_PURPLE,
	}
 
-- tips
-- __:/
-- .:_
-- 9:scale9
-- PNG:.png
-- JPG:.jpg

-- cards resource
GAME_TEXTURE_DATA_CARDS  = "cards/cards.plist"
GAME_TEXTURE_IMAGE_CARDS = "cards/cards.png" 


-- express folder
EXPRESS__EXP_BUT0_PNG = "express/exp_but0.png"
EXPRESS__EXP_BUT1_PNG = "express/exp_but1.png"
EXPRESS__EXP_BUT2_PNG = "express/exp_but2.png"
EXPRESS__EXP_BUT3_PNG = "express/exp_but3.png"
EXPRESS__EXP_BUT4_PNG = "express/exp_but4.png"
EXPRESS__EXP_BUT5_PNG = "express/exp_but5.png" 
EXPRESS__EXPRESSION_CHECKED9_PNG = "express/expression_checked.9.png"

-- expressII folder
-- 用戶表情
	EXPRESSII__EXPRESSION_DATA = {"expressII/expression1.plist","expressII/expression2.plist","expressII/expression3.plist",
									"expressII/expression4.plist","expressII/expression19.plist","expressII/expression20.plist",				
									"expressII/expression5.plist","expressII/expression18.plist","expressII/expression21.plist",
									"expressII/expression6.plist","expressII/expression17.plist","expressII/expression22.plist",
									"expressII/expression7.plist","expressII/expression16.plist","expressII/expression23.plist",
									"expressII/expression8.plist","expressII/expression15.plist","expressII/expression24.plist",
									"expressII/expression9.plist","expressII/expression14.plist","expressII/expression25.plist",
									"expressII/expression10.plist","expressII/expression13.plist","expressII/expression26.plist",
									"expressII/expression11.plist","expressII/expression12.plist","expressII/expression27.plist",
								}	
	EXPRESSII__EXPRESSION_IMAGE = {"expressII/expression1.png","expressII/expression2.png","expressII/expression3.png",
									"expressII/expression4.png","expressII/expression19.png","expressII/expression20.png",				
									"expressII/expression5.png","expressII/expression18.png","expressII/expression21.png",
									"expressII/expression6.png","expressII/expression17.png","expressII/expression22.png",
									"expressII/expression7.png","expressII/expression16.png","expressII/expression23.png",
									"expressII/expression8.png","expressII/expression15.png","expressII/expression24.png",
									"expressII/expression9.png","expressII/expression14.png","expressII/expression25.png",
									"expressII/expression10.png","expressII/expression13.png","expressII/expression26.png",
									"expressII/expression11.png","expressII/expression12.png","expressII/expression27.png",
								}

-- interactive folder
-- 交互表情
	INTERACTIVE__INTERACTIVEPRO_DATA = {"interactive/interactivepro1.plist","interactive/interactivepro2.plist","interactive/interactivepro3.plist",
										"interactive/interactivepro4.plist","interactive/interactivepro5.plist","interactive/interactivepro6.plist"}
	INTERACTIVE__INTERACTIVEPRO_IMAGE  = {"interactive/interactivepro1.png","interactive/interactivepro2.png","interactive/interactivepro3.png",
										"interactive/interactivepro4.png","interactive/interactivepro5.png","interactive/interactivepro6.png"}
 

-- audio folder
	AUDIO__EFFECT1_WAV = "audio/effect1.wav"
	AUDIO__BACKGROUND_MP3 = "audio/background.mp3"
	AUDIO__SOUND14_MP3 = "audio/sound 14.mp3"
	AUDIO__SOUND10_MP3 = "audio/sound 10.mp3" 
	AUDIO_SOUND4_MP3 = "audio/sound 4.mp3"
	AUDIO__SOUND8_MP3 = "audio/sound 8.mp3"
	AUDIO__SOUND6_MP3 = "audio/sound 6.mp3"
	AUDIO__SOUND7_MP3= "audio/sound 7.mp3"
	AUDIO__SOUND9_MP3 = "audio/sound 9.mp3"

-- shop folder
	SHOP__MARKET_ARROW_UP_PNG = "shop/market_arrow_up.png"
	SHOP__MARKET_ARROW_DOWN_PNG = "shop/market_arrow_down.png"
	SHOP__CMLARGE_PNG = "shop/cmlarge.png"

-- common folder
	COMMON__LIEBIAO_TAOTAI_LIANG_PNG = "common/liebiao_taotai_liang.png"
	COMMON__COMMON_BANNER_PNG = "common/common_banner.png"
	COMMON__COMMON_KUANG_9PNG = "common/common_kuang.9.png"
	COMMON__TCL_UPAY_KEYBOARD_BTN_SELECTED_9PNG = "common/tcl_upay_keyboard_btn_selected.9.png"
	COMMON__BACKPIC_PNG = "common/backpic.png" 

	COMMON__SLIP_BTN_PNG = "common/slip_btn.png"
	COMMON__NEWFEATURE_FRAME_9PNG = "common/newfeature_frame.9.png"
	COMMON__FUNCTION_SBUTTON_BLUE_NORMAL_9PNG ="common/function_sbutton_blue_normal.9.png"
	COMMON__BACKBG9_PNG = "common/backbg.9.png"
	COMMON__COMMON_HUIKUANG_PNG ="common/common_huikuang.png"
	COMMON__MESSAGE_ICON_PNG = "common/message_icon.png"
	COMMON__OKBUTTON_NORMAL_PNG = "common/okbutton_normal.png"
	COMMON__NOPRIZE_PNG = "common/noprize.png"
	COMMON__ADD_BUT9_PNG = "common/add_but.9.png"
	COMMON__MARKET_BUTTON_PURCHASE_PNG = "common/market_button_purchase.png"	
	COMMON__ACTIVITY_TAB_WEI_PNG = "common/activity_tab_wei.png"
	COMMON__SEEKBARBGROUND_PNG = "common/seekbarbground.png"
	COMMON__SEEKBART_PNG = "common/seekbart.png"
	COMMON__INPUT_BG_PNG = "common/input_bg.png"
	COMMON__BLUE_BTN_PNG = "common/blue_btn.png"
	COMMON__YELLOW_BTN_PNG = "common/yellow_btn.png"
	COMMON__TCL_UPAY_TITLE_BG9_PNG = "common/tcl_upay_title_bg.9.png"

-- progress folder
	PROGRESS__PROGRESS_NEW_DOWN_PNG = "progress/progress_new_down.png"
	PROGRESS__PROGRESS_YELLOW_MIDDLE_PNG = "progress/progress_yellow_middle.png"

-- hall folder
	HALL__HALLLIST_BBG_9PNG = "hall/halllist_bbg.9.png"
	HALL__BACKBG_9PNG = "hall/backbg.9.png"
	HALL__FUNCTION_COIN_PNG = "hall/function_coin.png"
	HALL__FUNCTION_SBUTTON_OUT_PNG = "hall/function_sbutton_out.png"
	HALL__FUN_SETIMG_PNG = "hall/fun_setimg.png"
	HALL__ID_ICON_PNG = "hall/id_icon.png"
	HALL__ZHANJI_ICON_PNG = "hall/zhanji_icon.png"
	HALL__ZUIDAPAI_ICON_PNG = "hall/zuidapai_icon.png"
	HALL__ZUIDAYING_ICON_PNG = "hall/zuidaying_icon.png"
	HALL__HALLLIST_BG_JPG = "hall/halllist_bg.jpg"
	HALL__DTHT_1_PNG = "hall/dtht_1.png"
	HALL__DTHT_2_PNG = "hall/dtht_2.png"
	HALL__HALLLIST_CENTER_PNG ="hall/halllist_center.png"
	HALL__HALLLIST_LEFT_PNG = "hall/halllist_left.png"
	HALL__HALLLIST_RIGHT_PNG = "hall/halllist_right.png"
	HALL__ORDER_ASC_PNG = "hall/order_asc.png"
	HALL__ORDER_DESC_PNG ="hall/order_desc.png"
	HALL__HALLLIST_FILTERBG_PNG = "hall/halllist_filterbg.png"
	HALL__SEX_MAN_PNG ="hall/sex_man.png"
	HALL__SEX_WOMEN_PNG ="hall/sex_women.png"
	HALL__GUEST_PNG = "hall/guest.png"
	HALL__MENU_BG_PNG  = "hall/menu_bg.png"
	HALL__FUNCTION_LOGOBG9_PNG = "hall/function_logobg.9.png"
	HALL__FUNCTION_LOGO_PNG = "hall/function_logo.png"
	HALL__FUNCTION_LOGOBG_BOTTOM_PNG = "hall/function_logobg_bottom.png"
	HALL__JIERI_HALL3_PNG = "hall/jieri_hall3.png"
	HALL__JIERI_HALL1_PNG = "hall/jieri_hall1.png"

	HALL__JIERI_HALL4_PNG = "hall/jieri_hall4.png"
	HALL__JIERI_HALL2_PNG = "hall/jieri_hall2.png"
	HALL__HALLLIST_CONT_PNG ="hall/halllist_cont.png"
	HALL__HELP_BACK_PNG = "hall/help_back.png"
	HALL__FUNCTION_MARKET_PNG = "hall/function_market.png"
	HALL__FUNCTION_FRIENDS_PNG = "hall/function_friends.png"
	HALL__HALL_GIFT_BOX_PNG = "hall/hall_gift_box.png"
	HALL__FUNCTION_HONOUR_PNG = "hall/function_honour.png"
	HALL__FUNCTION_ACTIVITY_PNG = "hall/function_activity.png" 

-- sns folder
	SNS__INTERACT_LISTFS9_PNG = "sns/interact_listfs.9.png"
	SNS__INTERACT_BG9_PNG = "sns/interact_bg.9.png"

--  topN folder
	TOPN__QIU_HUI_PNG = "topN/qiu_hui.png"
	TOPN__BILLBOARD_PORTRAIT_FRAME_PNG = "topN/billboard_portrait_frame.png"	
	TOPN__NUMBERS_IMAGES = {"topN/1.png","topN/2.png","topN/3.png"}
	TOPN__HELPCORNUCOPIA_PNG = "topN/helpCornucopia.png"
	TOPN__ACHIEVEMENT_FOOTER_JPG = "topN/achievement_footer.jpg"
	TOPN__ARROW_RIGHT_WITHOUT_STICK_PNG = "topN/arrow_right_without_stick.png"

-- room folder
	ROOM__MEN_HEAD_PNG ="room/men_head.png"  --RES_PATH
	ROOM__ROOM_MANAGER_SBG9_PNG = "room/room_manager_sbg.9.png"
	ROOM__TOOL1_PNG ="room/tool1.png"
	ROOM__TOOL2_PNG ="room/tool2.png"
	ROOM__TOOL3_PNG ="room/tool3.png"
	ROOM__TOOL4_PNG ="room/tool4.png"
	ROOM__TOOL5_PNG ="room/tool5.png"
	ROOM__TOOL6_PNG ="room/tool6.png"
	ROOM__BLUE_DIA_PNG = "room/blue_dia.png"
	ROOM__TONGJIMESSAGEBG_TWO2_9_PNG = "room/tongjimessagebg_two2.9.png"
	ROOM__TONGJIMESSAGEBG_TWO9_PNG = "room/tongjimessagebg_two.9.png"
	ROOM__PORTRAIT_WAIT9_PNG = "room/portrait_wait.9.png"
	ROOM__LEVELLISTSCELLSELECTED_PNG = "room/LevelListsCellSelected.png"
	ROOM__LEVELLISTSCELLINDICATOR_PNG = "room/LevelListsCellIndicator.png"
	ROOM__USERSEAT_PNG = "room/userseat.png"
	ROOM__PORTRAIT_WAIT_PNG = "room/portrait_wait.png"
	ROOM__USER_WIN_PNG = "room/user_win.png"
	ROOM__NULLSEATDOWN_PNG = "room/nullseatdown.png"
	ROOM__FUNCTION_LV_BG9_PNG = "room/function_lv_bg.9.png"
	ROOM__CHIP_ADD_SUBMIT_PNG = "room/chip_add_submit.png"
	ROOM__CARD_TYPE_MESSAGE_PNG = "room/card_type_message.png"
	ROOM__CARD_TYPEBG_PNG = "room/card_typebg.png"
	ROOM__DISPLAY_CARD_TYPE_PNG = "room/display_card_type.png"
	ROOM__CHIPINPOP_PNG ="room/chipinpop.png"
	ROOM__CHIPINBT_PNG = "room/chipinbt.png"
	ROOM__CHIPIN_NO_PNG = "room/chipin_no.png"
	ROOM__CHIPIN_CHOOSE_PNG = "room/chipin_choose.png"
	ROOM__CHIPIN_NULL_PNG = "room/chipin_null.png"
	ROOM__CHIP_4_PNG = "room/chip_4.png"
	ROOM__CHIP_20_PNG = "room/chip_20.png"
	room__chip_80_png = "room/chip_80.png"
	ROOM__CHIP_200_PNG = "room/chip_200.png"
	ROOM__ROOM_MENU_BG9_PNG = "room/room_menu_bg.9.png"
	ROOM__ROOM_MENU_OUT_PNG ="room/room_menu_out.png"
	ROOM__ROOM_MENU_UP_PNG = "room/room_menu_up.png"
	ROOM__TABLE_PNG = "room/table.png"
	ROOM__YOUWIN_PNG = "room/youwin.png"
	ROOM__DEALER_PNG = "room/dealer.png"
	ROOM__ROOMBG_PNG = "room/roombg.png"
	ROOM__ROOM_MENU_BUT_PNG = "room/room_menu_but.png"
	ROOM__ROOMGIRL_PNG = "room/roomgirl.png"
	ROOM__SMALLFEEBTN9_PNG = "room/smallfeebtn.9.png"
	ROOM__LOADING_VIEW_PNG = "room/loading_view.png"

-- window folder
	WINDOW__HELP_BG_NEW9_PNG = "window/help_bg_new.9.png"

-- chat folder
	CHAT__CHATTEN_SIMPLE_PANE2_PNG = "chat/chatten_simple_pane2.png"
	CHAT__CHATCLICK_PNG = "chat/chatclick.png"
	CHAT__CHATITEM_BG9_PNG = "chat/chatitem_bg.9.png"
	CHAT__CHATPOP_LEFT9_PNG = "chat/chatpop_left.9.png"
	CHAT__CHATTEN_SIMPLE_PANE_PNG = "chat/chatten_simple_pane.png"
	CHAT__CHAT_CLOSE_PNG = "chat/chat_close.png"
	CHAT__CHAT_BG9_PNG = "chat/chat_bg.9.png"
	CHAT__CHATTEN_SIMLIING_FACE_PNG = "chat/chatten_simliing_face.png"
	CHAT__CHATCLICK_BUT_PNG = "chat/chatclick_but.png"
	CHAT__SEND_PNG = "chat/send.png"
	CHAT__CHAT_TANGLE_PNG = "chat/chat_tangle.png"
	CHAT__BG_OPTION_LINE_PNG = "chat/bg_option_line.png"

--  load folder
	LOADING__SPINNER_BLACK_48_PNG = "loading/spinner_black_48.png"

-- bets config
ROOM__BETSHASH = {}
	ROOM__BETSHASH[0]="room/texas_chip01.png"
	ROOM__BETSHASH[1]="room/texas_chip05.png"
	ROOM__BETSHASH[2]="room/texas_chip1k.png"
	ROOM__BETSHASH[3]="room/texas_chip5k.png"
	ROOM__BETSHASH[4]="room/texas_chip10w.png"
	ROOM__BETSHASH[5]="room/texas_chip50w.png"	

CARDS_COLOR = {"hall/function_diamonds.png", "hall/function_clubs.png", "hall/function_hearts.png","hall/function_spades.png"}; --{"方", "梅", "紅","黑"}; 

 