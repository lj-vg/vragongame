--滑動按纽
-- Author: anjun
-- Date: 2014-04-24 20:33:02
--　
local SlipButton = class("SlipButton", function()
    return display.newNode() 
end)
SlipButton.CLICK = "click"

function SlipButton:ctor()
	makeUIControl_(self)
 	self:initView()
 	-- self:initEvent()
end 

function SlipButton:initView()
	local CHECKBOX_BUTTON_IMAGES = {
		off = COMMON__SETTING_OFF_JPG,
		on = COMMON__SETTING_ON_JPG,
	}

    self._ck = cc.ui.UICheckBoxButton.new(CHECKBOX_BUTTON_IMAGES) 
          :onButtonClicked(function(event) 
            self:dispatchEvent({name=SlipButton.CLICK,data= event.target:isButtonSelected() })
        end)
        :onButtonStateChanged(function(event)
            self:move() 
        end)
        :align(display.BOTTOM_LEFT,0,0)
        :addTo(self) 
	-- self._slipSp = display.newSprite(COMMON__SLIP_BTN_PNG) 
	-- :align(display.BOTTOM_LEFT,0,0)
	-- :addTo(self)  
end



function SlipButton:checkBoxButton()
	return self._ck
end


function SlipButton:move() 
	-- local x = self._slipSp:getPositionX()
	-- local w = self._slipSp:getContentSize().width 
	-- local w2 = self._ck.sprite_:getContentSize().width
	-- local fixW = w2 - w 
	-- if ( self._ck:isButtonSelected()) then
	-- 	self._slipSp:setPositionX(x+fixW)
	-- else
	-- 	self._slipSp:setPositionX(x-fixW) 
	-- end
end

-- 後續跟進吧．^_^...
-- function SlipButton:initEvent()
-- 	-- self:setTouchEnabled(true)
-- 	-- self:registerScriptTouchHandler(function(event, x, y)
--  --        return self:__onTouch(event, x, y)
--  --    end)
-- end

-- --  touch事件处理
-- function SlipButton:__onTouch(event, x, y) 
--     local rect = nil 
--     if event == "began" then 
--     	self._point = ccp(x, y)
--     	print("began:",y,self._point.y)
--         return true
--     elseif event == "moved" then
--     	print("moved:",y,self._point.y)
-- print("···",self._slipSp:getContentSize().width,self._slipSp:getPositionX() )
--     	-- if (math.abs(y - self._point.y) > 0.2 ) then
--     	-- 	self:moveSlipButton(y - self._point.y)
--     	-- end
--         return true
--     elseif event == "ended" then   
    	
--         return true
--     else -- cancelled

--          return true
--     end
-- end   
-- function SlipButton:moveSlipButton( x ) 
-- 	local currposX = self._slipSp:getPositionX()
-- 	local width = self._slipSp:getContentSize().width 
-- 	print("···",self._slipSp:getContentSize().width,self._slipSp:getPositionX() )
-- 	if (currposX == 0) then
-- 		if (x < 0) then
-- 			return
-- 		end
-- 	elseif ( currposX > 0) then 
-- 		if (x >0) then
-- 			return
-- 		end
-- 	end 
-- 	self._slipSp:setPositionX(currposX+x)
-- end

function SlipButton:getContentSize()  
    return self._ck.sprite_:getContentSize();
end

return SlipButton