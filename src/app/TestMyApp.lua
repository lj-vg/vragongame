--
-- Author: anjun
-- Date: 2014-06-02 12:26:12
-- 
require("config")
require("framework.init")
require("app.components.init")
require("app.config.init")
-- require("app.utils.init")  
-- require("app.net.init") 
require("app.managers.init")
-- require("app.events.init")
-- require("app.controllers.init")
-- require("app.platformCustom.init") 
-- require("app.testUnits.data.TestData") 
local TestMyApp = class("TestMyApp", cc.mvc.AppBase) 
function TestMyApp:ctor()
    TestMyApp.super.ctor(self) 
end

function TestMyApp:tmpTest()
end
 
function TestMyApp:run() 
    cc.FileUtils:getInstance():addSearchPath("res/")
    -- self:enterScene("LoginScene")
    self:enterScene("TestScene") 
end

function TestMyApp:init()
 
end 

function TestMyApp:initEvent() 
    -- self.mainModule:addEventListener(CLIENT_EVENT_SHOW_WINDOW,handler(self,self.__showWindow) )
end

-- about Global start
function TestMyApp:getModuleByName( name,subType ) --得到數據模型根據name
    if(not controllers.ModuleControl:isModuleExists(name)) then
        local modeuleClass = 'app.models.'..name
        if (subType) then
            modeuleClass = 'app.models.'..subType.."."..name
        end
        controllers.ModuleControl:addModule(name, require(modeuleClass).new())  
    end
    return controllers.ModuleControl:getModule(name) 
end 
-- about Global end 
return TestMyApp
