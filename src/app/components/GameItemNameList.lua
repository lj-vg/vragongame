--選擇商品ItemList
-- Author: anjun
-- Date: 2014-08-7 11:40:42
-- 
local GameListItem = require("app.scenes.items.GameListItem") 
local AdvancedList =  import(".AdvancedList")
local GameItemNameList = class("GameItemNameList", AdvancedList) 
function GameItemNameList:ctor(option)  
	option.item = option.item or GameListItem 
	GameItemNameList.super.ctor(self, option)
	self._nValue=-1;
	self._bLoadGetData=true;
	self._nMinLoadGameID=1; 
	self._sAllGameLabel= option.AllGameLabel or '全部';
	self._nAddAllGame= option.AddAllGame or false;	 
	self:setSourceData()
	self:initialization() 
	if( #self._dataSource>0) then self:setDataValue( self._dataSource[1].data ) end 
end 
-- function GameItemNameList:initView()
--    GameItemNameList.super.initView( self )
-- end 
-- function GameItemNameList:initEvent() 
--     makeUIControl_(self) 
-- end   
function AdvancedList:getListValue() 
	return self._currInfo
end

function AdvancedList:setListValue(value) 
	self._currInfo = value
end
 
function GameItemNameList:setDataValue(nV) 
	self._nValue=-1; 
	for i=1,#self._dataSource do
		if (  toint(self._dataSource[i]["data"]) == nV ) then
			self._nValue = nV
			self:updateButtonLabel( self._dataSource[i]["labelname"] )
			self:setListValue( self._dataSource[i] )
			break;
		end 
	end 
end
function GameItemNameList:getDataValue()
	if (self._currInfo) then
		return toint( self._currInfo['data'] )
	end 
	return -1;
end
 
function GameItemNameList:__itemListCallBack(info)
	self:hide()
	self._currInfo = info[1] 
	if self._callBack then
		self._callBack(self._currInfo)
	end  
    self._buttonItem:setButtonLabelString( self._currInfo.labelname ) 
end 

function GameItemNameList:setSourceData() 
	local tData= app.mainModule._GameListInfo:getData(0,true);
	self._dataSource = clone( tData ) 
	if(self._nAddAllGame) then
		local ta = {}
		ta['labelname']=self._sAllGameLabel;
		ta['data']=-1;  
		table.insert(self._dataSource,1,ta)
	end  
end

return GameItemNameList		  