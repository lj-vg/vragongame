--测试进度条
-- Author: anjun
-- Date: 2014-02-25 13:47:28
--
 

local function runTestProgressTest()
    local newScene = CCScene:create()
	local layer = CCLayer:create() 
    s = display

    local to1 = CCProgressTo:create(2, 100)
    local left = CCProgressTimer:create(CCSprite:create("button_confirm_on.png")) 
    local currCount = 0
    local updateGlobalIndex = -1

    -- 精灵沿着径向执行进度动画  
        -- left:setType(kCCProgressTimerTypeRadial)
        -- left:setPosition(CCPointMake(200, s.height / 2))
        -- -- left:runAction(CCRepeatForever:create(to1))
        --      -- left:setPercentage(60)
        -- layer:addChild(left)  

        local function __callBack( ...)
            -- body
            print("__callBack",...)
            currCount = currCount + 1;
            left:setPercentage(currCount)
            print("getPercentage:",left:getPercentage())

            if ( currCount > 100) then
                extendUI.scheduler.unscheduleGlobal(updateGlobalIndex)  
            end
        end 
        -- updateGlobalIndex = extendUI.scheduler.scheduleUpdateGlobal(__callBack)
        updateGlobalIndex = extendUI.scheduler.scheduleGlobal(__callBack,1)
        

    -- 精灵沿着水平方向执行动画
        left:setType(kCCProgressTimerTypeBar) 
        -- left:setType(kCCProgressTimerTypeRadial) 
        -- Setup for a bar starting from the left since the midpoint is 0 for the x
        left:setMidpoint(CCPointMake(0, 0))
        -- Setup for a horizontal bar since the bar change rate is 0 for y meaning no vertical change
        left:setBarChangeRate(CCPointMake(1, 0))
        left:setPosition(CCPointMake(200, s.height / 2))

        -- left:runAction(CCRepeatForever:create(to1))
        -- left:runAction(to1)
        -- left:setPercentage(60)

        layer:addChild(left)


        -- CCSprite * sprite = CCSprite::createWithSpriteFrameName("fly.png");
        -- CCProgressTimer * right = CCProgressTimer::create(sprite);
        -- this->addChild(right, kLayerFly);
        -- right->setPosition(400, 400);
        -- right->setType(kCCProgressTimerTypeBar);
        -- right->setPercentage(0);
        -- right->setBarChangeRate(ccp(1,0));
        -- right->setMidpoint(ccp(1,0)) ;
        -- right->setReverseProgress(true);

        -- CCProgressTo * act = CCProgressTo::create(3.0, 100);
        -- right->runAction(act);

    --精灵沿着垂直方向执行动画 
        -- left:setType(kCCProgressTimerTypeBar)
        -- -- Setup for a bar starting from the bottom since the midpoint is 0 for the y
        -- left:setMidpoint(CCPointMake(0,0))
        -- -- Setup for a vertical bar since the bar change rate is 0 for x meaning no horizontal change
        -- left:setBarChangeRate(CCPointMake(0, 1))
        -- left:setPosition(CCPointMake(200, s.height / 2))
        -- left:runAction(CCRepeatForever:create(to1))
        -- layer:addChild(left)  

    -- 精灵沿着中点改变时的那个径向执行进度动画
    -- Our image on the left should be a radial progress indicator, clockwise　
        -- left:setType(kCCProgressTimerTypeRadial)
        -- left:setMidpoint(CCPointMake(0.5, 0.75))
        -- left:setPosition(CCPointMake(200, s.height / 2))
        -- left:runAction(CCRepeatForever:create(CCProgressTo:create(2, 100)))
        -- layer:addChild(left)     

    -- 精灵执行进度条动画时，精灵所在的那个条不断变化
        -- left:setType(kCCProgressTimerTypeBar) 
        -- -- Setup for a bar starting from the bottom since the midpoint is 0 for the y
        -- left:setMidpoint(CCPointMake(0.5, 0.5))
        -- -- Setup for a vertical bar since the bar change rate is 0 for x meaning no horizontal change
        -- left:setBarChangeRate(CCPointMake(1, 0))
        -- left:setPosition(CCPointMake(200, s.height / 2))
        -- left:runAction(CCRepeatForever:create(CCProgressTo:create(2, 100)))
        -- layer:addChild(left) 

    -- 精灵执行进度条动画时，颜色变化，并且有淡出效果
        -- local array = CCArray:create()
        -- array:addObject(CCTintTo:create(1, 255, 0, 0))
        -- array:addObject(CCTintTo:create(1, 0, 255, 0))
        -- array:addObject(CCTintTo:create(1, 0, 0, 255))
        -- local tint = CCSequence:create(array)
        -- local fade = CCSequence:createWithTwoActions(
        --     CCFadeTo:create(1.0, 0),
        --     CCFadeTo:create(1.0, 255))

        -- left:setType(kCCProgressTimerTypeBar)
        -- -- Setup for a bar starting from the bottom since the midpoint is 0 for the y
        -- left:setMidpoint(CCPointMake(0.5, 0.5))
        -- -- Setup for a vertical bar since the bar change rate is 0 for x meaning no horizontal change
        -- left:setBarChangeRate(CCPointMake(1, 0))
        -- left:setPosition(CCPointMake(200, s.height / 2))
        -- left:runAction(CCRepeatForever:create(CCProgressTo:create(6, 100)))
        -- left:runAction(CCRepeatForever:create(CCSequence:create(array)))
        -- layer:addChild(left)
        -- left:addChild(CCLabelTTF:create("Tint", "Marker Felt", 20.0)) 

    -- 并不仅仅是精灵执行动画，精灵所在的整个frame执行动画效果，包括精灵所在的那个frame旁边的填充      
        -- left:setType(kCCProgressTimerTypeBar)
        -- -- Setup for a bar starting from the bottom since the midpoint is 0 for the y
        -- left:setMidpoint(CCPointMake(0.5, 0.5))
        -- -- Setup for a vertical bar since the bar change rate is 0 for x meaning no horizontal change
        -- left:setBarChangeRate(CCPointMake(1, 0))
        -- left:setPosition(CCPointMake(200, s.height / 2))
        -- -- left:runAction(CCRepeatForever:create(CCProgressTo:create(6, 100)))
        -- -- left:runAction( CCProgressTo:create(6, 100))
        -- layer:addChild(left) 

	newScene:addChild(layer)
    return newScene
end 

function TestProgressMain() 
	flag = not flag
	local scene = CCScene:create() 
    scene:addChild(runTestProgressTest()) 
	return scene
end 