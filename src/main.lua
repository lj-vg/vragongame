function log( logStr )
  local Date = os.date("*t") 
  logStr = Date.hour..":"..Date.min..":"..Date.sec.. " "..logStr
  local function readFile(path)
      local file = io.open(path, "rb")
      if file then
          local content = file:read("*all")
          io.close(file)
          return content
      end
      return nil
  end

  local path = device.writablePath
  local curFile = path.."log.txt" 
  local data
  if io.exists(curFile) then
    data = readFile(curFile) 
  end
  if (data == nil) then
    io.writefile(curFile, logStr)
  else
    data = data .."\r\n"..logStr
    io.writefile(curFile, data) 
  end  
end


function __G__TRACKBACK__(errorMessage)
    print("----------------------------------------")
    print("LUA ERROR: " .. tostring(errorMessage) .. "\n")
    print(debug.traceback("", 2))
    print("----------------------------------------")
end

-- init File Path
function checkDirOK( path )
    require "lfs"
    local oldpath = lfs.currentdir()
    -- CCLuaLog("old path------> "..oldpath)

     if lfs.chdir(path) then
        lfs.chdir(oldpath)
        -- CCLuaLog("path check OK------> "..path)
        return true
     end

     if lfs.mkdir(path) then
        -- CCLuaLog("path create OK------> "..path)
        return true
     end
end

function makeSysDir()
    local filePath
    if device.platform == "android" then
       filePath = "/mnt/sdcard/vragon/"
    elseif  (device.platform == "windows") then 
        filePath = device.writablePath.."vragon\\"
    else
        filePath = device.writablePath .."vragon/" 
    end
    if not checkDirOK(filePath) then 
        print(".....init File Pathfail") 
        filePath = nil
    end
    return filePath
end

-- require("app.MyApp").new():run()
require("app.TestMyApp").new():run()
