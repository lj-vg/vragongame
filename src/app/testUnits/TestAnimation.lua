--测试动画
-- Author: anjun
-- Date: 2014-02-28 11:46:01
-- 

local function createWheelFromSwf(sp)  
	--原生
 --    local index = 0;
 --    local animFrames = CCArray:create(); 
 --    repeat	   
	-- 	local pngName  = string.format( "expression_m6.swf/%04d" , index); 
	-- 	local frame =  CCSpriteFrameCache:sharedSpriteFrameCache():spriteFrameByName(pngName);
	-- 	print(frame,pngName)
	-- 	if(frame == nil) then
	-- 		break;
	-- 	end
	-- 	--frame:setOffset( ccp(-259,-184) ); 
	-- 	animFrames:addObject(frame); 
	-- 	-- deathWheelImgList[#deathWheelImgList+1] = frame;   
	-- 	index = index + 1;
	-- until (false); 

	-- local animation = CCAnimation:createWithSpriteFrames(animFrames, 0.5 / 8);
	-- -- animation:setLoops(1); 

	-- -- sp:runAction(CCAnimate:create(animation)) 
	-- sp:playAnimationOnce(animation,true,function()
	--         print("playAnimationOnce completed") 
	--     	end,1) 

	-- CCSpriteFrameCache:sharedSpriteFrameCache():removeSpriteFramesFromFile( GAME_TEXTURE_DATA_EXPRESSION_WORM16 ); 

	--  quick 方法
	-- local frames = display.newFrames("expression_m6.swf/%04d", 0, 59) -- Run0001.png 到 Run0008.png
	-- local frames = extendUI.Common.newFrames("expression_m6.swf/%04d", 0,59)
	local frames = extendUI.Common.newFrames2("expression_m6.swf/%04d", 15) 
	local animation = display.newAnimation(frames, 0.5 / 8) -- 0.5 秒播放 8 帧 
  	-- animation:setLoops(-1); 
	sp:runAction(CCAnimate:create(animation)) 	

end 

local function _callBack( )
	print("_callBack...")
end 

local function createHimiLayer() --手工制作
    local layerH = CCLayer:create()
 
    local _font = CCLabelTTF:create("Himi_(cocos2dx-Lua)教程","Arial",33)
    _font:setPosition(230,280)
    layerH:addChild(_font)
 
    --创建自定义类型精灵
    local hsprite = HSprite:createHSprite("button_confirm_off.png")
    hsprite:setPosition(100,100)
    hsprite:setScale(1.5)
     local tmd= hsprite:getResponseDataLua()
     print("...",tmd)
    -- hsprite:setRotation(45)

    dump( HSprite:createHTTPRequestLua(_callBack,"visit()",1111),"ddd" )

    layerH:addChild(hsprite)
    return layerH 

end

local function createToluaPlus( ... )  --toLUＡ制作
	--创建自定义类型精灵
    local hsprite = MySprite:createMS("button_confirm_off.png")
    hsprite:setPosition(100,100)
    hsprite:setScale(1.5)
    return hsprite
end 

local function runTestAnimation()
    local newScene = CCScene:create()
	local layer = CCLayer:create() 
	local frames = extendUI.Common.newFrames2("expression_m6.swf/%04d", 0)
	local currframe = frames[1]
	local sprite = display.newSprite(currframe)
	-- sprite =  display.newSprite("function_sbutton_out.png") --如果这样就会出现不见了。
    sprite:setPosition(CCPointMake(100, 100))
	layer:addChild(sprite)
	newScene:addChild(layer) 
	-- local sprite = CCSprite:create("button_confirm_on.png")

    -- left:runAction(CCRepeatForever:create(to1)) 

	-- sprite:setFlipX(true) -- 启用水平翻转
	-- print(sprite:isFlipX()) -- 返回是否启用了水平翻转 	 
	-- sprite:setFlipY(true) -- 启用垂直翻转
	-- print(sprite:isFlipY()) -- 返回是否启用了垂直翻转
	-- local f = ccBlendFunc()
	-- f.src = GL_SRC_ALPHA
	-- f.dst = GL_ONE_MINUS_SRC_ALPHA 
	-- sprite:setBlendFunc(f)


	--transition.playAnimationOnce  播放之后sprite为啥不见了？
	-- 在显示对象上播放一次动画，并返回 CCAction 动作对象。 
	-- 格式： 
	-- action = transition.playAnimationOnce(显示对象,
	--                                       动画对象,
	--                                       [播放完成后删除显示对象],
	--                                       [播放完成后要执行的函数],
	--                                       [播放前等待的时间])
		-- 用法示例： 
		-- local frames = display.newFrames("HeroDead%04d.png", 1, 4)
		-- local animation = display.newAnimation(frames, 0.5 / 4) -- 0.5s play 20 frames
		-- transition.playAnimationOnce(sprite, animation,true,function()
		-- 	        print("playAnimationOnce completed",sprite:isVisible())
		-- 	        -- sprite:setVisible(true) 
		-- 	    	end,1)  

		-- 还可以用 CCSprite 对象的 playAnimationOnce() 方法来直接播放动画： 
		-- local frames = display.newFrames("HeroDead%04d.png", 1, 4)
		-- local animation = display.newAnimation(frames, 0.5 / 4) -- 0.5s play 20 frames 
		-- sprite:playAnimationOnce(animation,true,function()
		--         print("playAnimationOnce completed",sprite:getOpacity()) 
		--     	end,1)   

	-- transition.playAnimationForever

		-- 在显示对象上循环播放动画，并返回 CCAction 动作对象。 
		-- 格式：
		-- action = transition.playAnimationForever(显示对象, 动画对象, [播放前等待的时间])
		-- 用法示例：
		-- local frames = display.newFrames("HeroDead%04d.png", 1, 4)
		-- local animation = display.newAnimation(frames, 0.5 / 4) -- 0.5s play 20 frames
		-- sprite:playAnimationForever(animation) 	 

	local function testAnimationFrames() 
	    -- local button = display.newSprite("#expression_m6.png.swf/0000")--display.right - 100, display.bottom + 270)
	    -- layer:addChild(button) 
		-- 创建一个跑步的动画
		local frames = display.newFrames("expression_m6.swf/%04d", 0, 18) -- Run0001.png 到 Run0008.png
		local animation = display.newAnimation(frames, 0.5 / 8) -- 0.5 秒播放 8 帧

		display.setAnimationCache("Run", animation) -- 用 Run 这个名字将动画缓存起来  
		-- 创建 sprite
		-- 有时候我们可以创建由一组图片组成的动画，然后直接设置 sprite 显示这个动画的第几帧，这和 Flash 中 MovieClip.setFrameIndex() 功能一样。
		local currframe = frames[15]
		local sprite2 = display.newSprite(currframe,190,-115)
		print( currframe:getOffset().x,currframe:getOffset().y);
		-- print(sprite2:getContentSize().height,sprite2:getContentSize().width)
		layer:addChild(sprite2)
		sprite2:setAnchorPoint(CCPoint(0,0)) 
		-- 在需要的时候，设置显示内容为 Run 动画的第 5 帧
		sprite2:setDisplayFrameWithAnimationName("Run", 15) -- 动画是 C++ 对象，所以索引从 0 开始 

		--test animation mv
		-- local animation = CCAnimation:createWithSpriteFrames(animFrames, 0.1);
		-- animation:setLoops(-1); 
		-- sprite:runAction(CCAnimate:create(animation))   	 
 
	end

    local leftMenu = cc.ui.UIPushButton.new("common/slip_btn.png")
    :onButtonPressed(function(event) 
    end)
    :onButtonRelease(function(event) 
    end)
    :onButtonClicked(function(event)  

   -- createWheelFromSwf(sprite) 
   -- testAnimationFrames()
 
		-- local animation = display.newAnimation(frames, 0.5 / 12) -- 0.5s play 20 frames 
		-- -- sprite:setScale(2)
		-- sprite:playAnimationOnce(animation,false,function()
		--         print("playAnimationOnce completed",sprite:getOpacity()) 
		--     	end,1)   　

	-- local frames = extendUI.Common.newFrames2("expression_m6.swf/%04d", 0) 
	local animation = display.newAnimation(frames, 0.5 / 8) -- 0.5 秒播放 8 帧  
	-- sprite:runAction(CCAnimate:create(animation)) 　　
    -- local action = CCAnimate:create(animation)
    -- sprite:runAction(CCSequence:createWithTwoActions(action, action:reverse()))

	local testAction = sprite:playAnimationOnce(animation,true,function()
	        print("playAnimationOnce completed") 
	        	sprite:removeSelf() -- 从父对象中删除自己  
	        	sprite = nil  

	        	local currframe = frames[25]
				local sprite2 = display.newSprite(currframe)
				print( currframe:getOffset().x,currframe:getOffset().y); 
				layer:addChild(sprite2)
				sprite2:setPosition(CCPointMake(100, 100))

				--test播放完之后，可以改变牌型
				-- 牌
				local pai = display.newSprite("function_sbutton_out.png") 
				pai:setPosition(ccp(200,100))
				sprite2:addChild( pai )

	    	end,0)  	

    end)
    :align(display.CENTER, 0, display.cy)
    :addTo(layer)	  
 
	-- test tolua
	newScene:addChild(createToluaPlus())
	-- newScene:addChild(createHimiLayer())
    return newScene
end  




function TestAnimationMain()  
    -- CCSpriteFrameCache:sharedSpriteFrameCache():addSpriteFramesWithFile(GAME_TEXTURE_DATA_EXPRESSION_WORM16)
	display.addSpriteFramesWithFile(GAME_TEXTURE_DATA_EXPRESSION_WORM16, GAME_TEXTURE_IMAGE_EXPRESSION_WORM16)

	local scene = CCScene:create()  

	local function __callback( ... )
		-- body
		-- scene:addChild(runTestAnimation()) 
		 scene:addChild( createHimiLayer() )
	end  
	extendUI.scheduler.performWithDelayGlobal(__callback,0)


	return scene
end 