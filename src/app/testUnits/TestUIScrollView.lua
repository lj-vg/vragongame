--
-- Author: anjun
-- Date: 2014-09-14 11:10:30
--

local TestUIScrollView = class("TestUIScrollView", function()
    return display.newScene("TestUIScrollView")
end)

function TestUIScrollView:ctor() 
    self:createScrollView1()
    self:createScrollView2()
end

function TestUIScrollView:createScrollView1()
    cc.ui.UILabel.new({text = "原始图", size = 24, color = display.COLOR_WHITE})
        :align(display.CENTER, 140, 890)
        :addTo(self)
	local sp1 = display.newScale9Sprite("TEST/sunset.png")
	sp1:setContentSize(380, 285)
	sp1:pos(240, 700)
	sp1:addTo(self)

    cc.ui.UILabel.new({text = "可滚动的图", size = 24, color = display.COLOR_WHITE})
        :align(display.CENTER, 140, 520)
        :addTo(self)
    local sp2 = display.newScale9Sprite("TEST/sunset.png")
    sp2:setContentSize(300, 200)
    sp2:pos(240, 480)
    sp2:addTo(self)

    -- local emptyNode = cc.Node:create()
    -- emptyNode:addChild(sp2)

    -- local bound = sp2:getBoundingBox()
    -- bound.width = 300
    -- bound.height = 200

    -- cc.ui.UIScrollView.new({viewRect = bound})
    --     :addScrollNode(emptyNode)
    --     -- :setDirection(cc.ui.UIScrollView.DIRECTION_HORIZONTAL)
    --     :onScroll(handler(self, self.scrollListener))
    --     :addTo(self)

	-- sp2:setDirection(0) --支持横向，纵向滑动，默认
	-- sp2:setDirection(1) --只支持纵向滑动
    -- sp2:setDirection(2) --只支持横向滑动

    -- sp2:regScrollListener(handler(self, self.scrollListener))

    cc.ui.UILabel.new({text = "可拖动的图", size = 24, color = display.COLOR_WHITE})
        :align(display.CENTER, 140, 520)
        :addTo(self)
    local sp3 = display.newScale9Sprite("TEST/sunset.png")
    sp3:setContentSize(300, 200)
    sp3:pos(240, 480)
    sp3:addTo(self)


    cc(sp3):addComponent("components.ui.DraggableProtocol")
        :exportMethods()
        :setDraggableEnable(true)
end

function TestUIScrollView:scrollListener(event)
	-- print("TestUIScrollView - scrollListener:" .. event.name)
end

function TestUIScrollView:createScrollView2()

end

return TestUIScrollView
