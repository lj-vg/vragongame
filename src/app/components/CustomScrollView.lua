--定制 ScrollView 组件
-- 可以收缩的显示更多内容
-- Author: anjun
-- Date: 2014-07-11 11:15:29
--
local CustomScrollView = class("CustomScrollView", function()
    return display.newNode() 
end)
function CustomScrollView:ctor(viewSize,itemGape)
	self._viewSize = viewSize or CCSize(280,220)
	self._itemGape = itemGape or 5
    self._widgets = {}
	makeUIControl_(self)
 	self:initView()
 	self:initEvent()
end 

function CustomScrollView:initView() 
    self._scrollView = extendUI.ScrollView.new(self._viewSize)  
    self._scrollView:setBounceable(true)
    self._scrollView:setDirection(kCCScrollViewDirectionVertical)  
    self._scrollView:setTouchEnabled(true)
    self._layerContainer = display.newColorLayer(ccc4(10, 20, 30, 10)) 
    self._layerContainer:setPosition(ccp(1, 0)) 
	self._widgetContainer = display.newSprite()  
	    :align(display.BOTTOM_LEFT, 0, 0)   
	    :addTo(self._layerContainer)
	self:addChild(self._scrollView) 
	self._scrollView:setContainer(self._layerContainer) 
 	self._layerContainer:setContentSize( self._viewSize )                  -- 设置容器
--debug 
-- local rectU = extendUI.Common.drawRect( CCRect(self._scrollView:getPositionX(),self._scrollView:getPositionY(),
--                                             self._scrollView:getViewSize().width,self._scrollView:getViewSize().height) )
-- self:addChild( rectU ) 	
end

function CustomScrollView:addChildWidget(child,tag )
	self._widgetContainer:addChild(child,0,tag)
    -- self._widgets[#self._widgets +1] = child
    self._widgets[tag] = child
	self:fixPos()
	return self
end

function CustomScrollView:getChildWidgetByTag(tag )
    return self._widgetContainer:getChildByTag(tag)
end

function CustomScrollView:getChildWidgets()
    return self._widgets
end

function CustomScrollView:initEvent()
    self._scrollView:addEventListener(self._scrollView.ISCANTOUCH, handler(self,self.__checkTouchRect))
end 

function CustomScrollView:setItemGape(value) 
	if ( self._itemGape == value) then return self end
	self._itemGape = value; 
	self:update()
 	return self
end

function CustomScrollView:update()
	self:fixPos()
end

function CustomScrollView:fixPos() 
    -- local count = self._widgetContainer:getChildrenCount() 
    -- local x = 0
    -- local y = 0  
    -- local oldSize = self._layerContainer:getContentSize()
    -- for i=1,count do 
    --     local scrollItem = self._widgetContainer:getChildByTag(i)
    --     if (scrollItem) then
    --         scrollItem:align(display.BOTTOM_LEFT, x, y)
    --         y = y + scrollItem:getContentSize().height + self._itemGape 
    --     end   
    -- end
    local count = #self._widgets
    local x = 0
    local y = 0  
    local oldSize = self._layerContainer:getContentSize()
    for i=1,count do 
        local scrollItem = self._widgets[i]
        if (scrollItem) then
            scrollItem:align(display.BOTTOM_LEFT, x, y)
            y = y + scrollItem:getContentSize().height + self._itemGape 
        end   
    end    

--最后一行不要间隔
 y = y - self._itemGape  

    self._layerContainer:setContentSize(CCSize(self._viewSize.width,y))
    local offsetY = oldSize.height-y
    local h1 = self._layerContainer:getPositionY() + offsetY
    if h1 > 0 then h1 = 0 end
    self._layerContainer:setPositionY(h1)
	-- fix offsetY 
	if (self._viewSize.height-y >0) then
	    self._layerContainer:setPositionY(self._viewSize.height-y)
	end
end 

--判断用户触摸是否在区域内
function CustomScrollView:__checkTouchRect( event ) 
    local flag = self:isCanTouch() 
    local count = #self._widgets 
    for i=1,count do 
        local scrollItem = self._widgets[i]
        if (scrollItem) then 
            scrollItem:lock( not flag)
        end   
    end        
    -- if (flag) then
    --     self:removeMask()
    -- else
    --     self:drawMask()
    -- end
end 

-- 画遮罩 (第一次判断有bug)
function CustomScrollView:drawMask() 
    self:removeMask()
    if(not self.mask_) then 
        self.mask_ = display.newSprite() 
    end
    self.mask_:setCascadeBoundingBox(CCRectMake(0, 0, self._layerContainer:getContentSize().width, self._layerContainer:getContentSize().height))
    self.mask_:setTouchEnabled(true) 
    self.mask_:addTouchEventListener(function(event)
        if event == "began" then
            return true
        elseif event == "ended" then 

        end
    end)
    self._widgetContainer:addChild(self.mask_,1);      

end 

function CustomScrollView:removeMask()
    if( self.mask_ ) then  
        self.mask_:removeSelf()    
    end 
    self.mask_ = nil; 
end

function CustomScrollView:isScrolling()
	return self._scrollView:isScrolling()
end

function CustomScrollView:isCanTouch()
    return self._scrollView:isCanTouch() 
end

function CustomScrollView:setTouchEnabled(flag)
   self._scrollView:setTouchEnabled(flag)
end 

return CustomScrollView
