--进度条（简单的，暂时没有考虑２端圆角）
-- Author: anjun
-- Date: 2014-03-25 14:53:31
-- 
local ProgressBar = class('ProgressBar',function ()
	  return display.newNode() 
end) 

function ProgressBar:ctor(images)
	self._total = 100
	self._currCount = 100
    self:initView(images)
    self:initEvent() 
end 

function ProgressBar:initView(images)
	local bg = (images and  images.bg) or PROGRESS__PROGRESS_NEW_DOWN_PNG
	local contentBg = (images and images.contentBg ) or PROGRESS__PROGRESS_YELLOW_MIDDLE_PNG

 	self._bg = display.newSprite(bg)
 					:addTo(self)
    self._content = CCProgressTimer:create(CCSprite:create(contentBg))    
	-- 精灵沿着水平方向执行动画
	self._content:setType(kCCProgressTimerTypeBar) 
	-- Setup for a bar starting from the left since the midpoint is 0 for the x
	self._content:setMidpoint(CCPointMake(0, 0))
	-- Setup for a horizontal bar since the bar change rate is 0 for y meaning no vertical change
	self._content:setBarChangeRate(CCPointMake(1, 0))
	self._content:setPosition(CCPointMake(0,0))
	self:addChild(self._content)
	self._content:setPercentage(self._currCount) 
end 

function ProgressBar:initEvent()

end  

function ProgressBar:update( percent )
	self._currCount = percent 
	self._content:setPercentage(self._currCount) 
	return self
end

function ProgressBar:getContentSize() 
	return self._bg:getContentSize()
end

return ProgressBar