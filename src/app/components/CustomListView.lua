--定制 ListView 组件
-- Author: anjun
-- Date: 2014-07-03 18:19:25
--支持多选或者单选　
-- Item参考ServerItem
local ListView = import(".ListView")
local CustomListView = class("CustomListView", ListView)
function CustomListView:ctor(options)
	self._itemSize = options.itemSize
	self.Item = options.Item
	self._info = options.info
    self.currSelectInfo = {}
    self.currSelectInfo[#self.currSelectInfo+1] = self._info[1]  
    self._isMultipleSelect = options.isMultipleSelect or false
	CustomListView.super.ctor(self,options)
end

function CustomListView:isSelectInAtInfo( info ) 
    local result = false
    for i=1,#self.currSelectInfo do
        if ( self.currSelectInfo[i] == info) then
            result = true
            break
        end
    end
    return result
end

function CustomListView:isSelectInAtIndex( index )
    local result = false
    for i=1,#self.currSelectInfo do
        if ( i == index) then
            result = true
            break
        end
    end
    return result
end

function CustomListView:removeAllSelect()
    self.currSelectInfo = {} 
end

function CustomListView:allSelect() 
    self.currSelectInfo = {} 
    for i=1,#self._info do
        self.currSelectInfo[#self.currSelectInfo+1] = self._info[i]
    end 
    -- self.currSelectInfo = clone(self._info) --因為table嵌table的原因?
end

function CustomListView:removeSelectAtInfo( info )
    local index = 1
    for i,v in ipairs(self.currSelectInfo) do
        print(i,v)
        if ( v == info ) then
            index= i
            break
        end
    end
    table.remove(self.currSelectInfo,index)
end

function CustomListView:setCurrSelectInfo( selectInfo )
    self.currSelectInfo =  {}
    self.currSelectInfo[#self.currSelectInfo+1] = selectInfo
    self:updateListSelectLight()
end

function CustomListView:__listhandle( fn, table, a1, a2 )
    local r 
    local item = nil
    if fn == "cellSize" then
        r = self._itemSize
    elseif (fn == "scroll") then 
    elseif fn == "cellAtIndex" then 
        if not a2 then
            a2 = CCTableViewCell:new()
            item = self.Item.new( self._itemSize )  
            a2:addChild(item,0,123) 
            item:setAnchorPoint(ccp(0, 0))
        end 
        
        item = a2:getChildByTag(123)
        if nil ~= item then
            if (self:isSelectInAtInfo( self._info[a1+1] ) ) then
                item:selectLight(true)
            else
                item:selectLight(false)
            end       
        	item:update( self._info[a1+1] )  
        end
        r = a2 
    elseif fn == "numberOfCells" then 
        r =#self._info 
    -- Cell events:
    elseif fn == "cellTouched" then         -- A cell was touched, a1 is cell that be touched. This is not necessary.
        item = a1:getChildByTag(123);
        -- table.remove(self.currSelectInfo,1)
        if ( not self._isMultipleSelect) then
             self:removeAllSelect()
            self.currSelectInfo[#self.currSelectInfo+1] = item:info()
        else
            if ( not self:isSelectInAtInfo( item:info() ) ) then 
                self.currSelectInfo[#self.currSelectInfo+1] = item:info()
            else
                self:removeSelectAtInfo( item:info() ) 
            end            
        end 
        if ( item ) then	         
			item:highlight(false)  
        end
        self:updateListSelectLight()
        self:actionCallBack() 
    elseif fn == "cellTouchBegan" then      -- A cell is touching, a1 is cell, a2 is CCTouch
    elseif fn == "cellTouchEnded" then      -- A cell was touched, a1 is cell, a2 is CCTouch
    elseif fn == "cellHighlight" then       -- A cell is highlighting, coco2d-x 2.1.3 or above
  		item = a1:getChildByTag(123); 
		if ( item ) then
 			item:highlight(true)
		end  
    elseif fn == "cellUnhighlight" then     -- A cell had been unhighlighted, coco2d-x 2.1.3 or above
    	item = a1:getChildByTag(123); 
		if ( item ) then
 			item:highlight(false)
		end  
    elseif fn == "cellWillRecycle" then     -- A cell will be recycled, coco2d-x 2.1.3 or above
    end
    return r
end

function CustomListView:setCallBack( callFun)
	self._callFun =  callFun
end

function CustomListView:actionCallBack()
    if (self._callFun) then
    	self._callFun( self.currSelectInfo )
    end
end

function CustomListView:updateListSelectLight() 
    local item 
    local cell
    local listView = self:getListView()
    local currIndex = 0
    for i=1,#self._info do  
        cell = listView:cellAtIndex(i-1) 
        if (cell) then
            item = cell:getChildByTag(123) 
            item:selectLight(false)  
            item:highlight(false)
        end
        for j=1,#self.currSelectInfo do 
            if ( self.currSelectInfo[j] == self._info[i] ) then 
                if ( cell ) then
                    item = cell:getChildByTag(123) 
                    item:selectLight(true)  
                end
            end 
        end
    end
end

function CustomListView:show(isMask,isLock) 
	CustomListView.super.show(self,isMask,isLock)
	self:updateListSelectLight()    
end

function CustomListView:initView(options)
	CustomListView.super.initView(self,options)
end

return CustomListView
