--测试动画
-- Author: anjun
-- Date: 2014-09-27 11:27:19
--

local TestMV = class("TestMV", function()
    return display.newScene("TestMV")
end)   

function TestMV:ctor() 
	local function __callback( ... )
		local frames = Common.newFrames2("expression6.swf/%04d", 0)
		local currframe = frames[1]
		local sprite = display.newSprite(currframe)
		-- sprite =  display.newSprite("function_sbutton_out.png") --如果这样就会出现不见了。
		sprite:setPosition(cc.p(100, 100))		
		sprite:addTo(self)

		local frames = display.newFrames("expression6.swf/%04d", 0, 18) -- Run0001.png 到 Run0008.png
		local animation = display.newAnimation(frames, 0.5 / 8) -- 0.5 秒播放 8 帧

		display.setAnimationCache("Run", animation) -- 用 Run 这个名字将动画缓存起来  
		-- 创建 sprite
		-- 有时候我们可以创建由一组图片组成的动画，然后直接设置 sprite 显示这个动画的第几帧，这和 Flash 中 MovieClip.setFrameIndex() 功能一样。
		-- local currframe = frames[15]
		-- local sprite2 = display.newSprite(currframe,190,-115)
		-- print( currframe:getOffset().x,currframe:getOffset().y);
		-- print(sprite2:getContentSize().height,sprite2:getContentSize().width)
		-- self:addChild(sprite2)
		-- sprite2:setAnchorPoint(cc.p(0,0)) 
		-- 在需要的时候，设置显示内容为 Run 动画的第 5 帧
		-- sprite2:setDisplayFrameWithAnimationName("Run", 15) -- 动画是 C++ 对象，所以索引从 0 开始 


		--test animation mv
		-- local animation = CCAnimation:createWithSpriteFrames(animFrames, 0.1);
		-- animation:setLoops(-1); 
		-- sprite:runAction(cc.CCAnimate:create(animation))   	 	

		-- sprite:playAnimationOnce(animation)
		sprite:playAnimationForever(animation)
	end
	display.addSpriteFrames("TEST/expression6.plist","TEST/expression6.png",__callback)
	-- scheduler.performWithDelayGlobal(__callback,1)
end

return TestMV

 