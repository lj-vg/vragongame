--
-- Author: msdnet
-- Date: 2014-01-21 20:06:17
-- 
local BaseVO = class('BaseVO')
function BaseVO:ctor() 

end

function BaseVO:fill( obj )
  if(obj == nil) then return end
  for key, value in pairs(obj) do  
     if self[key] ~= nil then
     	self[key] = obj[key]  
      end  
  end 
end 

return BaseVO