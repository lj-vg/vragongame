--选择Icon按纽<>
--Author : anjun
--Date   : 2014/6/17
--此文件由[BabeLua]插件自动生成

local UIButton =  require("framework.cc.ui.UIButton")
local UIPushButton =  cc.ui.UIPushButton

local IconButton = import(".IconButton")
local SelectIconButton = class("SelectIconButton", IconButton)  
SelectIconButton.OFF          = "off" 
SelectIconButton.ON           = "on" 
function SelectIconButton:ctor(images, options,iconImages) 
    local events = {
        {name = "disable", from = {"off", "pressed"}, to = "disabled"},
        {name = "disable", from = {"on", "pressed"}, to = "disabled"},
        {name = "enable",  from = {"disabled"}, to = "off"},
        {name = "enable",  from = {"disabled"}, to = "on"},
        {name = "press",   from = "off",  to = "pressed"},
        {name = "press",   from = "on",  to = "pressed"},
        {name = "release", from ={"pressed","off"}, to = "off"},
        -- {name = "release", from ={"pressed","on"}, to = "on"},
        {name = "select",   from = {"off"}, to = "on"},
        {name = "unselect", from = {"on","pressed"}, to = "off"},  
    }
    SelectIconButton.super.super.super.ctor(self,events,"off",options) 
-- UIPushButton
    if type(images) ~= "table" then images = {on = images} end 
    self:setButtonImage(SelectIconButton.ON, images["on"], true)
    self:setButtonImage(SelectIconButton.OFF, images["off"], true)
    self:setButtonImage(UIPushButton.PRESSED, images["pressed"], true)
    self:setButtonImage(UIPushButton.DISABLED, images["disabled"], true) 
--IconButton
    self.iconImages_ = {}
    self.currentIconImage_ = nil
    self.iconSprite_ = nil
    self.iconOffset_ = {0, 0}
    self.autoOffset_ = 0;    --自动偏移量.
    self.isFirst_ = true;   -- 是否第一次. 
    self.iconDirection_ = extendUI.Common.LEFT 
    self:initEvent(iconImages)
end

function SelectIconButton:isButtonSelected() 
    return self.fsm_:canDoEvent("unselect")
end

function SelectIconButton:setButtonSelected(selected) 
    if self:isButtonSelected() ~= selected then 
        if selected then
            self.fsm_:doEventForce("select")
        else
            self.fsm_:doEventForce("unselect")
        end
        self:dispatchEvent({name = UIButton.STATE_CHANGED_EVENT, state = self.fsm_:getState()})
    end
    return self
end

--override 
function SelectIconButton:initEvent(iconImages)
    self:addScriptEventListener(cc.Event.ENTER_SCENE, function()      
        if type(iconImages) ~= "table" then iconImages = {normal = iconImages} end
        if ( self.isFirst_ ) then
            self:setButtonIconImage(SelectIconButton.ON, iconImages["normal"], true) 
             self:setButtonIconImage(SelectIconButton.OFF, iconImages["normal"], true) 
            self:autoButtonOffset()
            self:updateButton()   
            self.isFirst_ = false         
        end 
    end)    
end

-- override
function SelectIconButton:setButtonImage(state, image, ignoreEmpty)
    assert(state == UIPushButton.PRESSED
        or state == UIPushButton.DISABLED
        or state == SelectIconButton.OFF
        or state == SelectIconButton.ON ,
        string.format("UIPushButton:setButtonImage() - invalid state %s", tostring(state)))
    UIPushButton.super.setButtonImage(self, state, image, ignoreEmpty)

    if state == UIPushButton.ON then
        if not self.images_[UIPushButton.PRESSED] then
            self.images_[UIPushButton.PRESSED] = image
        end
        if not self.images_[UIPushButton.DISABLED] then
            self.images_[UIPushButton.DISABLED] = image
        end
        if not self.images_[SelectIconButton.OFF] then
            self.images_[SelectIconButton.OFF] = image
        end        
    end
    return self
end

-- override
function SelectIconButton:onTouch_(event, x, y)
    if event == "began" then
        -- anjun
        self._isSelected = self:isButtonSelected()
        -- anun end
        if not self:checkTouchInSprite_(x, y) then return false end
        self.fsm_:doEvent("press")
        self:dispatchEvent({name = UIButton.PRESSED_EVENT, x = x, y = y, touchInTarget = true})
        return true
    end

    local touchInTarget = self:checkTouchInSprite_(x, y)
    if event == "moved" then
        if touchInTarget and self.fsm_:canDoEvent("press") then
            self.fsm_:doEvent("press")
            self:dispatchEvent({name = UIButton.PRESSED_EVENT, x = x, y = y, touchInTarget = true})
        elseif not touchInTarget and self.fsm_:canDoEvent("release") then
            self.fsm_:doEvent("release")
            self:dispatchEvent({name = UIButton.RELEASE_EVENT, x = x, y = y, touchInTarget = false})
        end
    else 
        if self.fsm_:canDoEvent("release") then 
            self.fsm_:doEvent("release")
            self:dispatchEvent({name = UIButton.RELEASE_EVENT, x = x, y = y, touchInTarget = touchInTarget})
        end
        if event == "ended" and touchInTarget then
            self:setButtonSelected(self.fsm_:canDoEvent("select"))
            self:dispatchEvent({name = UIButton.CLICKED_EVENT, x = x, y = y, touchInTarget = true})
        else
            if ( self._isSelected ) then  --如果之前是選擇的狀態,touchinTarget為false,要復原
                self:setButtonSelected(self.fsm_:canDoEvent("select"))
            end
            
        end
    end
end

-- 取所有状态图片最大的宽与高
-- <br>还有个隐患:on; off 先后的问题，后续...
function SelectIconButton:getMaxContentSize( )
    local image,size,tmpSprite
    size = CCSize(0,0)
    for _, image in pairs(self.images_) do  
        if image and string.len(image) ~= 0 then 
            tmpSprite = display.newSprite(image) 
            if (tmpSprite:getContentSize().width > size.width) then
                size.width = tmpSprite:getContentSize().width
            end
            if (tmpSprite:getContentSize().height > size.height ) then
                size.height =  tmpSprite:getContentSize().height
            end
        end
    end 
    tmpSprite = nil 
    return size
end  

-- function SelectIconButton:getLayoutSize()
--     -- getmetatable(self).getLayoutSize(self)  
--     local w,h = 120,120
--      print("funckdsf adsfa asd你:getLayoutSize()",w,h)
--     return w,h
-- end

-- function SelectIconButton:getContentSize()
--     local size =self:getMaxContentSize()
--     print("funckdsf adsfa asd你:getMaxContentSize()")
--     return size
-- end

return SelectIconButton;
--endregion
