--Test CocosGUI
-- Author: anjun
-- Date: 2014-06-02 12:29:10
-- 
-- local TestCocosGUI = class("TestCocosGUI",UIScene) 
local TestCocosGUI = class("TestCocosGUI", function()
    return display.newNode()
    -- return display.newLayer()
end)

function TestCocosGUI:ctor()
    -- TestCocosGUI.super.ctor(self)
    self._uiLayer = TouchGroup:create()
    self:addChild(self._uiLayer)

    self._widget = GUIReader:shareReader():widgetFromJsonFile("DemoLogin/DemoLogin.json")
    self._uiLayer:addWidget(self._widget)
    print("self._uiLayer:",self._uiLayer:getAnchorPoint().x,self._uiLayer:getAnchorPoint().y)

    -- self._uiLayer:clear()

    local lbl = self._uiLayer:getWidgetByName("Label_58")
    -- -- lbl:setString("funckme")
    -- dump(lbl)
    -- -- lbl = tolua.cast(lbl, "Label")
    lbl:setText("ts222")
    local testLbl = ui.newTTFLabel({text="tsetLbl"}) 
    -- self._uiLayer:removeWidget(lbl)

    -- local testLbl = Label:create() 
    -- testLbl:setText("tsetLbl你好驶到？？？")
    self:addChild(testLbl)
    testLbl:setAnchorPoint(ccp(0, 0))
    testLbl:setPositionX(100)
    testLbl:setPositionY(100)
    -- self._uiLayer:addWidget(testLbl) 
    -- self._sceneTitle = self._uiLayer:getWidgetByName("UItest")

    -- local back_label = self._uiLayer:getWidgetByName("back")
    -- back_label:setVisible(false)
end 

return TestCocosGUI
