-- web連接組件

local WebClient=class("WebClient")

function WebClient:ctor()
   self.gateway = 'http://192.168.1.168/dzx3/game/taxes/amfphp2/amfphp/index.php' ;
   self.code = '' ;
   -- 请求列表
   self._requestList = {}
end
function WebClient:setCode(codevalue)
	self.code = codevalue ;
end
function WebClient:setGateway(url)
	self.gateway = url ;
end
function WebClient:callFailTip( msg ) --请求失败 
  managers.LoadingManager:hideLoading()
  managers.AlertManager:showCenterMsgTip(msg)  
end

  --parameters 用 table 格式 如果為空 用nil
  -- parameters = {"wq","ww","ss"}                   --传递多个参数
  -- parameters ={ {"abcd",1,"33",{11,22,"ddd"}} }   --传递数组(1)
  -- parameters= nil                                 --无参数
  -- parameters= { {} }                              --传空數組
function WebClient:call(serviceName,methodName,parameters,callback,isTips)   
  isTips = ( (isTips~=false) and true ) or false 
  local currHttpID = serviceName.."."..methodName
  if isTips then managers.LoadingManager:showLoading('加載中，請稍後...') end
  local function __callBack( resVO )  --回调 
    if ( not self:cancelCall(currHttpID) ) then return end  --已经取消则不回调了 
    if (callback ~= nil ) then   --and resVO ~= nil 
      callback(resVO)  
    end 
  end

  if ( not app.phpManager:checkNetWork() ) then
    self:callFailTip( "網絡狀態不佳，請檢查後重試" ) 
    return
  end

  local parameters = parameters or nil ;      
  local _headinfo = {} ; 
  local _jsondata = {headers=_headinfo,serviceName=serviceName,methodName=methodName,parameters=parameters}  ;
  local _jsondata =  json.encode(_jsondata) ;
   function defaultCallback(event)  
            
      if isTips then managers.LoadingManager:hideLoading() end
        local ok = (event.name == "completed")
		    local request = event.request		  
		    if not ok then 
             __callBack(nil)
            self:callFailTip(request:getErrorCode()..request:getErrorMessage())
		        print("請求錯誤",request:getErrorCode(), request:getErrorMessage())           
		        return
		    end		 
		    local _rstcode = request:getResponseStatusCode()		    
		    if _rstcode ~= 200 then 
		        -- 请求结束，但没有返回 200 响应代码	
             __callBack(nil)
		         print("请求结束， 返回  响应代码",_rstcode)	 
             self:callFailTip("响应代码".._rstcode)      
		        return
		    end		 
		    -- 请求成功，显示服务端返回的内容
		    local response = request:getResponseString()    
        local resVO = nil
        if (response) then
          resVO = json.decode(response)
        end 
        __callBack(resVO)
   end 
   local _requestUrl = self.gateway ;
    _requestUrl = _requestUrl .. '?contentType=application/json' ;
    _requestUrl = _requestUrl .. '&code=' .. self.code ;
   local _httprequest = network.createHTTPRequest(defaultCallback,_requestUrl,'POST');   
   _httprequest:setPOSTData(_jsondata) ;
   _httprequest:start();
   -- anjun   
  self._requestList[currHttpID] = _httprequest  
   -- anjun   
end 

function WebClient:cancelCall(url) 
  local result = false
  local httprequest = self._requestList[url] 
  if (httprequest) then
    httprequest:cancel()
    result = true
  end
  httprequest = nil
  self._requestList[url] = nil
  return result
end

return WebClient