--文本
-- Author: anjun
-- Date: 2014-06-23 14:16:30
--暫時用官方的，後續跟進
local TextField = class("TextField", function(options)
    local editBox = ui.newEditBox({
      image = options.image,
      size = options.size, 
      listener = options.listener
  	})
    return editBox
end)

return TextField