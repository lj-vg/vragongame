--图标，文字按纽
-- Author: anjun
-- Date: 2014-01-22 17:27:21
-- 
local UIPushButton =  cc.ui.UIPushButton
local IconButton = class("IconButton", UIPushButton) 
IconButton.ICON_IMAGE_SPACEING = 5
function IconButton:ctor(images, options,iconImages)
    IconButton.super.ctor(self, images, options) 
    self.iconImages_ = {}
    self.currentIconImage_ = nil
    self.iconSprite_ = nil
    self.iconOffset_ = {0, 0}
    self.autoOffset_ = 0;    --自动偏移量.
    self.isFirst_ = true;   -- 是否第一次. funck

    self.iconDirection_ = extendUI.Common.LEFT 
    self:initEvent(iconImages)
end

function IconButton:initEvent(iconImages)
    self:addScriptEventListener(cc.Event.ENTER_SCENE, function()      
        if type(iconImages) ~= "table" then iconImages = {normal = iconImages} end
        if ( self.isFirst_ ) then
            self:setButtonIconImage(UIPushButton.NORMAL, iconImages["normal"], true)
            -- self:setButtonIconImage(UIPushButton.PRESSED, iconImages["pressed"], true)
            -- self:setButtonIconImage(UIPushButton.DISABLED, iconImages["disabled"], true) 
            self:autoButtonOffset()
            self:updateButton()   
            self.isFirst_ = false         
        end 
    end)    
end

-- 设置图标方向(相对文字)，左，右，上，下
function IconButton:setButtonIconDirection( value ) 
    self.iconDirection_ = value
    self:updateButton()
    return self
end
--得到图标偏移(相对文字)
function IconButton:getButtonIconOffset()
    return self.iconOffset_[1], self.iconOffset_[2]
end
-- 设置图标偏移(相对文字)
function IconButton:setButtonIconOffset(ox, oy)
    self.iconOffset_ = {ox, oy}
    self:updateButtonIconImage_()
    return self
end 
 
-- 自动调整button偏移
function IconButton:autoButtonOffset() 
    self.autoOffset_ = self:getAutoButtonOffset(self.iconDirection_)
    local offset = self.autoOffset_
    if (self.iconDirection_ == extendUI.Common.LEFT ) then 
        self.labelOffset_[1] = self.labelOffset_[1] + offset 
    elseif (self.iconDirection_ == extendUI.Common.RIGHT)  then
        self.labelOffset_[1] = self.labelOffset_[1] - offset  
    elseif (self.iconDirection_ == extendUI.Common.UP)  then
        self.labelOffset_[2] = self.labelOffset_[2] + offset
    elseif (self.iconDirection_ == extendUI.Common.DOWN) then 
        self.labelOffset_[2] = self.labelOffset_[2] - offset
    end   
end
-- 得到自动调整button偏移量
function IconButton:getAutoButtonOffset( direction )
    local spriteSize = self.sprite_:getContentSize() 

    local buttonLable = self:getButtonLabel()
    local buttonLableSize = (buttonLable and buttonLable:getContentSize() ) or CCSize(0,0)
    local ButtonIconSize = (self.iconSprite_ and self.iconSprite_:getContentSize() ) or CCSize(0,0);
    local offset = 0

    if (direction == extendUI.Common.LEFT ) then 
        offset = (spriteSize.width - buttonLableSize.width)/2 - (spriteSize.width - buttonLableSize.width - ButtonIconSize.width)/3
    elseif (direction == extendUI.Common.RIGHT)  then
        offset = (spriteSize.width - buttonLableSize.width)/2 - (spriteSize.width - buttonLableSize.width - ButtonIconSize.width)/3
    elseif (direction == extendUI.Common.UP)  then
        offset = (spriteSize.height - buttonLableSize.height - ButtonIconSize.height)/3
    elseif (direction == extendUI.Common.DOWN) then 
        offset = (spriteSize.height - buttonLableSize.height - ButtonIconSize.height)/3
    end  
    return offset
end

function IconButton:setButtonIconImage(state, image, ignoreEmpty)
    if ignoreEmpty and image == nil then return end
    self.iconImages_[state] = image
    if state == self.fsm_:getState() then
        self:updateButtonIconImage_()
    end
    return self
end
-- update
function IconButton:updateButton() 
    self:updateButtonImage_()
    self:updateButtonLable_()
    self:updateButtonIconImage_()    
end 

function IconButton:updateButtonIconImage_()
    local state = self.fsm_:getState()
    local image = self.iconImages_[state]

    if not image then
        for _, s in pairs(self:getDefaultState_()) do
            image = self.iconImages_[s]
            if image then break end
        end
    end 

    if image then
        if self.currentIconImage_ ~= image then
            if self.iconSprite_ then
                self.iconSprite_:removeFromParentAndCleanup(true)
                self.iconSprite_ = nil 
            end
            self.currentIconImage_ = image 
            self.iconSprite_ = display.newSprite(image)
            self:addChild(self.iconSprite_,-2) 
        end  

        local ap = self:getAnchorPoint()
        local spriteSize = self.sprite_:getContentSize() 
        local buttonLable = self:getButtonLabel()
        local buttonSize = (buttonLable and buttonLable:getContentSize() ) or CCSize(0,0)
        local buttonLablePos = (buttonLable and buttonLable:getPositionInCCPoint() ) or CCPoint(0,0)
        local refrenSize = (buttonLable and buttonLable:getContentSize() ) or spriteSize

        local ox, oy = self.iconOffset_[1], self.iconOffset_[2]
        local anchorPoint = self.labelAlign_  
        if (self.iconDirection_ == extendUI.Common.LEFT ) then 
            ox = ox + buttonLablePos.x- buttonSize.width/2 - self.iconSprite_:getContentSize().width
            oy = oy + spriteSize.height * (0.5 - ap.y)
        elseif (self.iconDirection_ == extendUI.Common.RIGHT)  then
            ox = ox + buttonLablePos.x+ buttonSize.width/2 + self.iconSprite_:getContentSize().width
            oy = oy + spriteSize.height * (0.5 - ap.y)
        elseif (self.iconDirection_ == extendUI.Common.UP)  then
            ox = ox + spriteSize.width * (0.5 - ap.x)
            oy = oy + buttonLablePos.y + buttonSize.height/2 + self.iconSprite_:getContentSize().height
        elseif (self.iconDirection_ == extendUI.Common.DOWN) then 
            ox = ox + spriteSize.width * (0.5 - ap.x)
            oy = oy + buttonLablePos.y - buttonSize.height/2 - self.iconSprite_:getContentSize().height
        elseif ( self.iconDirection_ == extendUI.Common.CENTER) then 
            ox = ox + spriteSize.width * (0.5 - ap.x)  
            oy = oy + spriteSize.height * (0.5 - ap.y)
        end  
        self.iconSprite_:align(anchorPoint, ox, oy) 

    else
        -- echoError("IconButton:updateButtonIconImage_() - not set image for state %s", state)
    end
end
-- 追加图标(比如hot,new...之类)
function IconButton:addIcon( fileName,x,y )  
    x = x or 0
    y = y or 0
    local icon = display.newSprite(fileName) 
                :addTo(self)
                :align(display.CENTER, x, y)
    return self,icon
end

--override 
function IconButton:updateButtonLable_()  
    if not self.labels_ then return end
    local state = self.fsm_:getState()
    local label = self.labels_[state]

    if not label then
        for _, s in pairs(self:getDefaultState_()) do
            label = self.labels_[s]
            if label then break end
        end
    end

    -- anjun add
    local anchorValue_y = 0;
    if (self.iconDirection_ == extendUI.Common.UP)  then
        anchorValue_y = 0
        self.labelAlign_ = display.BOTTOM_CENTER
    elseif (self.iconDirection_ == extendUI.Common.DOWN) then 
        anchorValue_y = 1
        self.labelAlign_ = display.CENTER_TOP
    else
        anchorValue_y = 0.5
        self.labelAlign_ = display.CENTER
    end
    -- anjun end

    local ox, oy = self.labelOffset_[1], self.labelOffset_[2]
    if self.sprite_ then
        local ap = self:getAnchorPoint()
        local spriteSize = self.sprite_:getContentSize()
        ox = ox + spriteSize.width * (0.5 - ap.x) 
        oy = oy + spriteSize.height * (anchorValue_y - ap.y)
    end 
    for _, l in pairs(self.labels_) do
        l:setVisible(l == label)
        l:align(self.labelAlign_, ox, oy)
    end 
end

function IconButton:updateButtonImage_()
    local state = self.fsm_:getState()
    local image = self.images_[state] 
    if not image then
        for _, s in pairs(self:getDefaultState_()) do
            image = self.images_[s]
            if image then break end
        end
    end
    if image then
        if self.currentImage_ ~= image then
            if self.sprite_ then 
                self.sprite_:removeFromParentAndCleanup(true)
                self.sprite_ = nil
            end
            self.currentImage_ = image

            if self.scale9_ then
                self.sprite_ = display.newScale9Sprite(image)
                if not self.scale9Size_ then
                    local size = self.sprite_:getContentSize()
                    self.scale9Size_ = {size.width, size.height}
                else
                    self.sprite_:setContentSize(CCSize(self.scale9Size_[1], self.scale9Size_[2]))
                end
            else
                self.sprite_ = display.newSprite(image)
            end 
            self:addChild(self.sprite_, IconButton.super.IMAGE_ZORDER)
        end

        self.sprite_:setAnchorPoint(self:getAnchorPoint())
        self.sprite_:setPosition(0, 0)
    else 
        self.sprite_ = display.newSprite()   
        if self.scale9_ then
            self.sprite_:setContentSize(CCSize(self.scale9Size_[1], self.scale9Size_[2]))
            local size = self.sprite_:getContentSize()
            self.scale9Size_ = {size.width, size.height}
            self.sprite_:setCascadeBoundingBox(CCRectMake(0, 0, self.scale9Size_[1], self.scale9Size_[2]))  
        else 
            if (self.iconSprite_) then
                local size = CCSize(0,0)
                local lableSize = self:getButtonLabel():getContentSize()
                local iconSpriteSize = self.iconSprite_:getContentSize() 
                size.width = (lableSize.width > iconSpriteSize.width and lableSize.width ) or iconSpriteSize.width
                size.height = (lableSize.height > iconSpriteSize.height and lableSize.height ) or iconSpriteSize.height
                self.sprite_:setContentSize(size) 
                self.sprite_:setCascadeBoundingBox(CCRectMake(0, 0, size.width, size.height))  
            end             
        end 
                      
    end
end        

function IconButton:align(align, x, y)
    IconButton.super.align(self, align, x, y)
    self:updateButtonIconImage_()
    return self
end

function IconButton:getContentSize()
    return  self.sprite_:getContentSize();
end

return IconButton;