--重写官方CCTableView
-- 方便扩展
-- Author: anjun
-- Date: 2014-02-16 18:33:56
-- 
local TableView = class("TableView",function( size )
	size = size or CCSize(0,0)
    return CCTableView:create(size)
end)

function TableView:ctor()
	--这个参数设定是显示列表 ?
	-- type = 1 　
	--当列表需要变化时（比如点击时改变宽高）
	-- 分别设置指定的那个子项的宽高　實現在對應的方法 
	-- 具體用法見TestExtensionII
	self.type = 0

	-- 用戶打開的哪個格子
	self.isOpen = 0
	-- 上次用戶打開的哪個格子　　　 
	self.lastOpen = 0
	-- 鎖狀態默認為關
	-- 解決touch覆蓋問題
	self.isLock = false
end

-- 記錄偏移，解決奇葩問題用的．詳見 TestExtensionII
function TableView:getCurrOffset() 
	return self:getContentOffset()
end 
-- 
function TableView:updateCurrOffset( offset ) 
	self:setContentOffset( offset );  
end
-- 
function TableView:resetOpen()
	self.isOpen = 0
	self.lastOpen = 0
end 

-- 
function TableView:lock()  
	self.isLock = true 
	self:setTouchEnabled(false); 
end
function TableView:unLock()  
	self.isLock = false
	self:setTouchEnabled(true); 
end
-- 以上都是觖決實際問題中，暫時不要管不好的設計，先實現功能，後研究吧．．．．

return TableView