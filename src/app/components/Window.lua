--窗口基类 
-- Author: anjun
-- Date: 2014-01-26 10:31:12
--

-- 后续跟进7ＲＯＡＤ的组件重构吧。(2014-09-06 2:20)
local Window = class("Window", function()
    return display.newNode() 
end)

Window.CLOSE = "WINDOW_CLOSE_EVENT"

function Window:ctor(options)
    makeUIControl_(self)  
    self.touchInSpriteOnly_ = options and options.touchInSprite  --是否开启触摸
    self.titleBar_ = nil
    self.background_ = nil
    self.contentSprite_ = nil
    self.closeButton_ = nil 
    self.isHasCloseButton_ = false 
	self.titleLbl_ = nil
	-- 
	self.closeBtnPosdirection_ ="right"	 --关闭按纽（返回）的方向
	self.customPosTitleBar_ = nil        --标题栏位置定制
	self.customPosTitleTxt_ = nil        -- 标题文本位置定制
	self.isCloseEffect_ = false            --是否开启关闭特效

    self.scale9_ = options and options.scale9
    self.scale9Size_ = nil   
    self._isInitial = false               --是否初始化  
    self._titleContentGap = 5               --标题面板距内容面板间距
    self:setNodeEventEnabled(true)
end

function Window:initView() 
	if ( not self.background_) then
		self.background_ = display.newRect(cc.rect(0, 0, 0, 0))
	end
	if ( not self.titleBar_) then
		self.titleBar_ = display.newRect(cc.rect(0, 0, 0, 0))
	end
	if ( not self.contentSprite_) then
		self.contentSprite_ = display.newRect(cc.rect(0, 0, 0, 0))
	end
	self:addChildren();
	self:invalidate(); 
end

function Window:initEvent()  
	if (self.touchInSpriteOnly_ and self.background_) then 
		self.background_:setTouchEnabled(true)
		self.background_:addNodeEventListener(cc.NODE_TOUCH_EVENT,handler(self, self.__windowTouch))	 
	end
	
end

function Window:__windowTouch(event) 
end

function Window:addChildren( ) 
	if (self.background_) then 
		self:addChild(self.background_,0)  
	end
	if (self.contentSprite_) then
		self:addChild(self.contentSprite_,1)
	end
	if (self.titleBar_) then 
		self:addChild(self.titleBar_,2)
	end 
	if (self.closeButton_) then 
		self:setIsHasClose(true)
	end 
	self:render();
end

function Window:addContentChild( child,zorder )   
	if (self.contentSprite_) then
		zorder = zorder or self.contentSprite_:getLocalZOrder()
		self.contentSprite_:addChild(child,zorder)
	end 
	return self
end

function Window:addBackgroundChild( child ,zorder) 
	zorder = zorder or self.background_:getLocalZOrder()
	self.background_:addChild(child,zorder) 
	return self
end

function Window:addTopBarChild( child,zorder ) 
	if (self.titleBar_) then
		zorder = zorder or self.titleBar_:getLocalZOrder()
		self.titleBar_:addChild(child,zorder)
		end
	self:invalidate()
	return self
end  

function Window:invalidate( )
	self:render()
end

function Window:render()
	self:fixPos()
end

-- 位置的要后续地一直跟进才行
function Window:fixPos()
	-- body
	local posX,posY =0,0   
	local w = self:getContentSize().width
	local h = self:getContentSize().height

	if (self.closeButton_ ~= nil ) then 
		local buttonSize = cc.size(0,0) 
        if not self.closeButton_.scale9Size_ then
            buttonSize = self.closeButton_:getContentSize() 
        else 
        	buttonSize = cc.size(self.closeButton_.scale9Size_[1],self.closeButton_.scale9Size_[2]) 
        end	 
		if (self.closeBtnPosdirection_ == "left") then --后续改进
			posX = buttonSize.width
			posY = h
		else 
			posX = w
			posY = h 			
		end		
		-- anjun end   
		self.closeButton_:setPositionX(posX)
		self.closeButton_:setPositionY(posY)
	end 	

	if (self.titleBar_ ~= nil) then  
		if (self.customPosTitleBar_ ~= nil ) then 
			posX = self.customPosTitleBar_.x
			posY = self.customPosTitleBar_.y
		else
			posX = (w - self.titleBar_:getContentSize().width)/2
			posY = h-self.titleBar_:getContentSize().height;			
		end
		self.titleBar_:setPositionX(posX)
		self.titleBar_:setPositionY(posY)		
	end  

	if (self.contentSprite_ ~= nil ) then		
		posX = (w - self.contentSprite_:getContentSize().width)/2
		if (self.titleBar_ ~= nil ) then 	--注意cocos2dx的座标系
			posY = self.titleBar_:getPositionY() - self.contentSprite_:getContentSize().height -self._titleContentGap
		else 
			posY = (h-self.contentSprite_:getContentSize().height) - self._titleContentGap
		end 
		self.contentSprite_:setPositionX(posX)
		self.contentSprite_:setPositionY(posY)
	end

	if (self.titleLbl_ ~= nil ) then 
		self.titleLbl_:setAnchorPoint(cc.p(0.5,0.5))  --居中  
		if (self.customPosTitleTxt_ ~= nil ) then 
			posX = self.customPosTitleTxt_.x
			posY = self.titleBar_:getContentSize().height/2
		else
			posX = self.titleBar_:getContentSize().width/2  --(self.titleBar_:getContentSize().width - self.titleLbl_:getContentSize().width)/2 
			posY = self.titleBar_:getContentSize().height/2
		end 
		self.titleLbl_:setPositionX(posX)
		self.titleLbl_:setPositionY(posY)
	end

end 

function Window:setIsHasClose(value)  	-- 这里是不是有？？？？
	self.isHasCloseButton_ = value	
	if ( not self.closeButton_  )  then return self end

	if ( self.isHasCloseButton_ ) then 		
		if ( not self.closeButton_:getParent() ) then
			if (self._isInitial) then
				self:addChild( self.closeButton_,3)
				self.closeButton_:addNodeEventListener(cc.NODE_TOUCH_EVENT,handler(self, self.__CloseTouch)) 
			end 
		end
	elseif (self.closeButton_) then 
		self.closeButton_:removeFromParent()
		self.closeButton_ = nil
	end
	self:invalidate()
    return self
end
function Window:getIsHasClose()
    return self.isHasCloseButton_
end

-- function Window:setIsTitleBar( value )  --后续跟进
-- 	if ( not self.titleBar_  )  then return self end

-- 	if ( value ) then 		
--  		self.titleBar_:removeFromParentAndCleanup(true)
-- 		self.titleBar_ = nil
-- 	else
-- 		if (self.titleBar_) then

-- 		end
-- 	end
-- 	self:invalidate()
--     return self	 
-- end

function Window:setTitleBar( var )
 	if ( self.titleBar_ ) then
		if ( self.titleLbl_) then 
			self.titleLbl_:removeFromParent()
			self.titleLbl_ = nil 
		end	 		
		self.titleBar_:removeFromParent()
		self.titleBar_ = nil 		
 	end
 	self.titleBar_ = var
 	if (self._isInitial) then
	 	self:addChild( self.titleBar_,2)
	 	self:invalidate()		
 	end 
 	return self
 end 

 function Window:setCloseButton( var ) 
  	if ( self.closeButton_ ) then 
		self.closeButton_:removeFromParent()
		self.closeButton_ = nil 		
 	end
 	self.closeButton_ = var	
 	self:setIsHasClose (true);
 	return self
 end

 function Window:setBackground( var ) 
  	if ( self.background_ ) then
		self.background_:removeFromParent()
		self.background_ = nil 		
 	end
 	self.background_ = var
 	if (self._isInitial) then 
	  	self:addChild(self.background_, 0) 
	 	self:invalidate()
 	end
 	return self
 end

 function Window:setContentSprite( var ) 
  	if ( self.contentSprite_ ) then
		self.contentSprite_:removeFromParent()
		self.contentSprite_ = nil 		
 	end
 	self.contentSprite_ = var
 	if (self._isInitial) then
	  	self:addChild(self.contentSprite_, 1) 
	 	self:invalidate()
 	end
 	return self
 end 

function Window:setTitleTxtStr( var ) 
	if ( not self._isInitial) then
		return
	end						
	if ( not self.titleLbl_) then 		  		
		self.titleLbl_ = cc.ui.UILabel.new({ text = var,
											size = 24,
											}) 
		self:addTopBarChild( self.titleLbl_ )
	end
	self.titleLbl_:setString(var)
	self:invalidate()
	return self
end 

function Window:setTitleTxtSize( size )
	self.titleLbl_:setFontSize(size)
	self:invalidate()
	return self	
end

function Window:setTitleTxtColor( var ) 
	if (self.titleLbl_) then
		self.titleLbl_:setColor(var)
		self:invalidate()
	end
	return self
end

function Window:setClosetBtnPosDirection( var ) 
	self.closeBtnPosdirection_ = var;
	self:invalidate()
	return self
end

function Window:setCustomPosTitleBar( var ) 
	self.customPosTitleBar_ = var;
	self:invalidate()
	return self
end

function Window:setCustomPosTitleTxt( var ) 
	self.customPosTitleTxt_ = var;
	self:invalidate()
	return self
end
  

function Window:getWinContentSize() 
	return self.contentSprite_:getContentSize()
end

function Window:getContentSize() 
	return self.background_:getContentSize()
end

function Window:getTitleBarSize() 
	return self.titleBar_:getContentSize()
end

function Window:__CloseTouch(event)  
	if ( event.name == "began" ) then
		self.closeButton_:setOpacity(128)
	elseif event.name == "ended" then 
		self.closeButton_:setOpacity(255)
		self:dispatchEvent({name = Window.CLOSE}) 
	end 	 
    return true
end
-- 画遮罩
function Window:drawMask(w, h)  
	if(not self.mask_) then 
		self.mask_ = display.newSprite()
	end
    self.mask_:setCascadeBoundingBox(cc.rect(0, 0, w, h))
    self.mask_:setTouchEnabled(true)
    self.mask_:addNodeEventListener(cc.NODE_TOUCH_EVENT,function(event)
        if event.name == "began" then
            return true
        elseif event.name == "ended" then
            print("Window Mask layer touch ended......")
        end
    end)  
end 
 -- 移除遮罩
function Window:removeMask()
	if( self.mask_) then  
		self.mask_:removeFromParent()
		self.mask_ = nil; 
	end
end

--关于销毁的后续应该要仔细考虑,可能会牵边到窗体的状态....
function Window:dispose()
	self:removeMask()
	self:stopAllActions()  
	self:removeAllEventListeners() 	
	self:removeAllChildren()
	self:removeFromParent()	 
end  

-- 关闭
function Window:closeWindow( ) 
	if (self.isCloseEffect_) then 
	    local function __callBack( ) 
	        self:dispose()         
	    end   
	    Common.hideWindowEffect(self,{onComplete=__callBack}) 
	else
		self:dispose()  
	end 
end

function Window:onEnterTransitionFinish()  
	if (self._isInitial) then --funck 不清楚为啥这个事件会进入多次？？？
		return 
	end
	self._isInitial = true
	self:initView()  
	self:initEvent() 
	--吞掉下面的事件，否则，你懂的
	self:drawMask(self:getContentSize().width, self:getContentSize().height);
	self:addChild(self.mask_,-1);
end
 
function Window:onEnter()
 
end

-- 删除所有监听，释放内存
function Window:onExit()  
    self:stopAllActions()     
    self:removeAllEventListeners()   
end 

return Window
