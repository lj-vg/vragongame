--层级管理器
-- Author: anjun
-- Date: 2014-01-28 16:47:28
--
local LayerManager = class("LayerManager")
LayerManager.LayerManager_instance = nil 
function LayerManager.getInstance()  
    if(not LayerManager_instance) then  
        LayerManager_instance = LayerManager.new()
    end
    return LayerManager_instance
end  
-- game Layer config
DEBUG_LAYER = 1
UI_LAYER 	= 2
TOP_LAYER	= 3

function LayerManager:ctor()    
end 

function LayerManager:initiation() --可能要多次,因为。。。
	self.currStage = cc.Director:getInstance():getRunningScene()
	assert(self.currStage ~= nil, string.format("LayerManager:initiation - currStage not exists", self.currStage)) 
	self.currStage:addNodeEventListener(cc.NODE_EVENT,function(event)  
	     if event.name=="cleanup" then     
	     	self:dispose()
	     	print("self.currStage cleanup")
	     end
	 end)  	 
	local stageSize = self.currStage:getContentSize() 
	self._debugLayer = display.newNode()
	self._debugLayer:setContentSize( stageSize )
	self.currStage:addChild(self._debugLayer,DEBUG_LAYER);   
	self._uiLayer = display.newNode()
	self._uiLayer:setContentSize( stageSize ) 
	self.currStage:addChild(self._uiLayer,UI_LAYER); 
	self._topLayer = display.newNode()
	self._topLayer:setContentSize( stageSize )
	self.currStage:addChild(self._topLayer,TOP_LAYER); 
end

function LayerManager:dispose()    --同上
	self.currStage =nil
	self._debugLayer = nil
	self._uiLayer = nil
	self._topLayer = nil
end
 
function LayerManager:addChildToLayer(option)  
	local child = option.child 
	assert(child ~= nil, string.format("LayerManager:addChildToLayer() - child not exists", child))
	local layerType = option.layerType or UI_LAYER 
	local isCeneter = option.isCeneter
	local isMask = option.isMask  
	local anchor_point = option.anchor_point or display.BOTTOM_LEFT
 	local currParent = self:getLayerByName( layerType ) 
 	local x = option.x or 0
 	local y = option.y or 0

	if (currParent) then	
		if(isMask) then
			if ( self.mask_ ) then  -- del mask 
				self.mask_:removeFromParent();
				self.mask_ = nil; 
			end
			self:drawMask(currParent:getContentSize().width, currParent:getContentSize().height);
			currParent:addChild(self.mask_,currParent:getLocalZOrder()); 
		end 
		currParent:addChild(child,currParent:getLocalZOrder());   
		if(isCeneter) then  
			x = (currParent:getContentSize().width-child:getContentSize().width)/2
			y = (currParent:getContentSize().height -child:getContentSize().height)/2	 
		end
		child:align(anchor_point, x, y)
		-- event
		if (isMask) then
			child:addNodeEventListener(cc.NODE_EVENT,function(event)  
			     if event.name=="cleanup" then    
					if ( self.mask_ ) then  -- del mask 
						self.mask_:removeFromParent();
						self.mask_ = nil; 
					end	  
			     end
			 end)  
		end  
	end	
end 

-- 通过名称获取图层
function LayerManager:getLayerByName(layerType) 
	if ( not self.currStage ) then
		self:initiation()  
	end  
	local currlayer = cc.Director:getInstance():getRunningScene()
	if layerType == DEBUG_LAYER then
		currlayer = self._debugLayer
	elseif layerType == UI_LAYER then 
		currlayer = self._uiLayer
	elseif layerType == TOP_LAYER then 
		currlayer = self._topLayer
	end
	return currlayer
end

-- 画遮罩
function LayerManager:drawMask(w, h) 
	if(not self.mask_) then 
		self.mask_ = display.newSprite() 
	end
    self.mask_:setCascadeBoundingBox(cc.rect(0, 0, w, h))
    self.mask_:setTouchEnabled(true)
    self.mask_:addNodeEventListener(cc.NODE_TOUCH_EVENT,function(event)
        if event.name== "began" then
            return true
        elseif event.name == "ended" then 
        	print("LayerManager Mask......")
        end
    end)   
end
 
 -- 从图层中移除视图
function LayerManager:removeChildFromLayer(child, a_isRemoveMask)   
	local isRemove = false; 
	if (not cc.Director:getInstance():getRunningScene() ) then 
		return isRemove
	end
	if ( (not self.currStage) or self.currStage ~= cc.Director:getInstance():getRunningScene() ) then
		if (child) then  
			child = nil;
		end
		if( self.mask_ and a_isRemoveMask) then  
			self.mask_:removeFromParent();
			self.mask_ = nil; 
		end 
		return isRemove
	end

	if ( child and child.getParent ) then    
		child:removeFromParent();
	    isRemove = true  

	end  
	child = nil;
	if( self.mask_ and a_isRemoveMask) then  
		self.mask_:removeFromParent();
		self.mask_ = nil;  
	end	
	return isRemove; 
end

-- 删除mask层
function LayerManager:removeMaskFromLayer()  
	if( self.mask_ ) then  
		self.mask_:removeFromParent()		 
		self.mask_ = nil;  
	end 
end

return LayerManager  

