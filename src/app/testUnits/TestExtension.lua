--测试官方的 CCTableView 和 CCScrollView
-- Author: anjun
-- Date: 2014-02-13 21:55:25
--

-- tableView start 
local TableViewTestLayer = class("TableViewTestLayer")
TableViewTestLayer.__index = TableViewTestLayer

function TableViewTestLayer.extend(target)
    local t = tolua.getpeer(target)
    if not t then
        t = {}
        tolua.setpeer(target, t)
    end
    setmetatable(t, TableViewTestLayer)
    return target
end

function TableViewTestLayer.scrollViewDidScroll(view)
    -- print("scrollViewDidScroll")
    local offSet = view:getContentOffset() 
    -- scrollView2:setContentOffset(CCPoint(offSet.x, offSet.y));
    -- print("offSet : %f %f",offSet.x,offSet.y);
end

function TableViewTestLayer.scrollViewDidZoom(view)
    -- print("scrollViewDidZoom")
end
--  处理触摸事件，可以计算点击的是哪一个子项  
function TableViewTestLayer.tableCellTouched(table,cell)
    -- print("cell touched at index: " .. cell:getIdx())
    -- CCLog("posy: %f",table->getContentOffset().y);
    local offsetY = table:getContentOffset().y;
    local offsetX = table:getContentOffset().x;
    table:setContentOffset(CCPointMake(offsetX-5,offsetY),true);

    -- local posX = cell:getPositionX();
    -- local posY = cell:getPositionY();
    -- cell:setPositionX(posX - 5);
end
--  每一项的高度和宽度 
function TableViewTestLayer.cellSizeForTable(table,idx)
    return 60,60
end
-- 生成列表每一项的内容  
function TableViewTestLayer.tableCellAtIndex(table, idx)
    -- print("TableViewTestLayer.tableCellAtIndex:"..idx)
    local strValue = string.format("%d",idx)
    local cell = table:dequeueCell()
    local label = nil
    if nil == cell then
        cell = CCTableViewCell:new()
        local sprite = CCSprite:create("TEST/Images/Icon.png")
        sprite:setAnchorPoint(CCPointMake(0,0))
        sprite:setPosition(CCPointMake(0, 0))
        cell:addChild(sprite)

        --  I
        local sprite1 = CCSprite:create("TEST/Images/Icon.png")
        sprite1:setAnchorPoint(CCPointMake(0,0))
        sprite1:setPosition(CCPointMake(60, 0))
        cell:addChild(sprite1)
        local txt = CCLabelTTF:create("姓名I:", "1", 20.0)
        txt:setPosition(CCPointMake(0,0))
        txt:setAnchorPoint(CCPointMake(0,0)) 
        sprite1:addChild(txt)

        -- --  II
        -- sprite1 = CCSprite:create("TEST/Images/Icon.png")
        -- sprite1:setAnchorPoint(CCPointMake(0,0))
        -- sprite1:setPosition(CCPointMake(120, 0))
        -- cell:addChild(sprite1)
        -- txt = CCLabelTTF:create("II", "1", 20.0)
        -- txt:setPosition(CCPointMake(0,0))
        -- txt:setAnchorPoint(CCPointMake(0,0)) 
        -- sprite1:addChild(txt)

        -- --  III
        -- sprite1 = CCSprite:create("TEST/Images/Icon.png")
        -- sprite1:setAnchorPoint(CCPointMake(0,0))
        -- sprite1:setPosition(CCPointMake(180, 0))
        -- cell:addChild(sprite1)
        -- txt = CCLabelTTF:create("III", "1", 20.0)
        -- txt:setPosition(CCPointMake(0,0))
        -- txt:setAnchorPoint(CCPointMake(0,0)) 
        -- sprite1:addChild(txt)

        -- --  IV
        -- sprite1 = CCSprite:create("TEST/Images/Icon.png")
        -- sprite1:setAnchorPoint(CCPointMake(0,0))
        -- sprite1:setPosition(CCPointMake(240, 0))
        -- cell:addChild(sprite1)
        -- txt = CCLabelTTF:create("IV", "1", 20.0)
        -- txt:setPosition(CCPointMake(0,0))
        -- txt:setAnchorPoint(CCPointMake(0,0)) 
        -- sprite1:addChild(txt)

        -- --  V
        -- sprite1 = CCSprite:create("TEST/Images/Icon.png")
        -- sprite1:setAnchorPoint(CCPointMake(0,0))
        -- sprite1:setPosition(CCPointMake(300, 0))
        -- cell:addChild(sprite1)
        -- txt = CCLabelTTF:create("V", "1", 20.0)
        -- txt:setPosition(CCPointMake(0,0))
        -- txt:setAnchorPoint(CCPointMake(0,0)) 
        -- sprite1:addChild(txt)

        -- --  VI
        -- sprite1 = CCSprite:create("TEST/Images/Icon.png")
        -- sprite1:setAnchorPoint(CCPointMake(0,0))
        -- sprite1:setPosition(CCPointMake(360, 0))
        -- cell:addChild(sprite1)
        -- txt = CCLabelTTF:create("VI", "1", 20.0)
        -- txt:setPosition(CCPointMake(0,0))
        -- txt:setAnchorPoint(CCPointMake(0,0)) 
        -- sprite1:addChild(txt)

        label = CCLabelTTF:create(strValue, "Helvetica", 20.0)
        label:setPosition(CCPointMake(0,0))
        label:setAnchorPoint(CCPointMake(0,0))
        label:setTag(123)
        cell:addChild(label)
    else
        label = tolua.cast(cell:getChildByTag(123),"CCLabelTTF")
        if nil ~= label then
            label:setString(strValue)
        end
    end

    return cell
end
-- 一共多少项
function TableViewTestLayer.numberOfCellsInTableView(table)
   return 11120
end

    local startX = x                    --开始X点
    local startY = y                     --开始Y点
function TableViewTestLayer.onTouch(event, x, y) 
    print("TableViewTestLayer:onTouch:",event)
    if event == "began" then
        startX = x
        startY = y
        return true 
    elseif event == "moved" then 
    -- anjun start
        local startPos = CCPoint(startX,startY)
        local currPos = CCPoint(x,y)
        print("ScrollView:onTouchMoved:",extendUI.Common.getDirection(startPos,currPos) )
    --  anjun end       
        return true 
    elseif event == "ended" then 
    else -- cancelled 
    end
end

function TableViewTestLayer:init()

    local winSize = CCDirector:sharedDirector():getWinSize()
    local touchRect =  display.newLayer()
    touchRect:setContentSize( CCSize(30,60))

    --下面的CCSizeMake是很关键的，他设定列表显示的范围，
    -- 如果列表上面显示不全，总有一部分被遮挡，那就调整高度吧，
    -- 同理，长度也一样 
    -- local tableView = CCTableView:create(CCSizeMake(30,60)) 
    local tableView = extendUI.TableView.new( CCSizeMake(30,60) )
    --左右滑动
    -- tableView:setDirection(kCCScrollViewDirectionHorizontal) 
    -- --上下滑动
    tableView:setDirection(kCCScrollViewDirectionVertical)
    -- 
    -- tableView:setDirection(kCCScrollViewDirectionBoth)    
    -- tableView:setDirection(kCCScrollViewDirectionNone)    
    -- 设置列表显示的初始位置 
    tableView:setPosition(CCPointMake(100, 100))
    -- 列表设置为从小到大显示，及idx从0开始
    tableView:setVerticalFillOrder(kCCTableViewFillTopDown)
    self:addChild(tableView)
    -- touchRect:setPosition(CCPointMake(100, 100))
    -- self:addChild(touchRect)
    tableView:getContainer():setTouchEnabled(true)
    --registerScriptHandler functions must be before the reloadData function
    tableView:registerScriptHandler(TableViewTestLayer.scrollViewDidScroll,CCTableView.kTableViewScroll)
    tableView:registerScriptHandler(TableViewTestLayer.scrollViewDidZoom,CCTableView.kTableViewZoom)
    tableView:registerScriptHandler(TableViewTestLayer.tableCellTouched,CCTableView.kTableCellTouched)
    tableView:registerScriptHandler(TableViewTestLayer.cellSizeForTable,CCTableView.kTableCellSizeForIndex)
    tableView:registerScriptHandler(TableViewTestLayer.tableCellAtIndex,CCTableView.kTableCellSizeAtIndex)
    tableView:registerScriptHandler(TableViewTestLayer.numberOfCellsInTableView,CCTableView.kNumberOfCellsInTableView)
    --重新加载数据，当遮挡后，再显示，是必须的。  
    tableView:reloadData()

    tableView:getContainer():registerScriptTouchHandler(function(event, x, y)
        return TableViewTestLayer.onTouch(event, x, y)
    end)



    local scrollView1 = CCScrollView:create() 
    local screenSize = CCDirector:sharedDirector():getWinSize()
  
  
    local function scrollView1DidScroll()
        -- print("scrollView1DidScroll")
    end

    local function scrollView1DidZoom()
        -- print("scrollView1DidZoom")
    end
    -- 　现在我们开始创建CCScrollView对象了。
    -- 当然我们最好还是再创建一个Layer用来作为
    -- CCScrollView的Container。
    if nil ~= scrollView1 then
         -- 显示显示的区域

         -- 设置scrollView的大小,为显示的view的尺寸 
        scrollView1:setViewSize(CCSizeMake(screenSize.width / 2,screenSize.height))
        -- scrollView1:setViewSize(CCSizeMake(960/2,640/2)) 

        scrollView1:setPosition(CCPointMake(screenSize.width / 2,0))
        scrollView1:setScale(1.0)
        -- 对于场景或层等大型节点，它们的IgnoreAnchorPointForPosition属性为 true ，
        -- 此时引擎会认为 AnchorPoint 永远为(0,0)；
        scrollView1:ignoreAnchorPointForPosition(true)
        local flowersprite1 =  CCSprite:create("TEST/ccb/flower.jpg")
        if nil ~= flowersprite1 then

            -- 显示滑动的区域大小 scrollview的实际大小
            -- 设置容器
            scrollView1:setContainer(flowersprite1)
            -- 设置容器大小
            -- scrollView1:setContentSize(CCSizeMake(1024, 1024));  
            -- 是开启弹性效果，关闭的话就不用使用这个控件
            -- scrollView1:setBounceable(false);
            -- local flag = scrollView1:isBounceable();
            -- print("flag: %d",flag);

            scrollView1:updateInset()
        end

        -- 滑动方向控制
        scrollView1:setDirection(kCCScrollViewDirectionBoth)
        scrollView1:setClippingToBounds(true)
        scrollView1:setBounceable(true)
        scrollView1:registerScriptHandler(scrollView1DidScroll,CCScrollView.kScrollViewScroll)
        scrollView1:registerScriptHandler(scrollView1DidZoom,CCScrollView.kScrollViewZoom)
    end
    self:addChild(scrollView1) 

    -- tableView = CCTableView:create(CCSizeMake(60, 350))
    -- tableView:setDirection(kCCScrollViewDirectionVertical)
    -- tableView:setPosition(CCPointMake(winSize.width - 150, winSize.height / 2 - 150))
    -- tableView:setVerticalFillOrder(kCCTableViewFillTopDown)
    -- self:addChild(tableView)
    -- tableView:registerScriptHandler(TableViewTestLayer.scrollViewDidScroll,CCTableView.kTableViewScroll)
    -- tableView:registerScriptHandler(TableViewTestLayer.scrollViewDidZoom,CCTableView.kTableViewZoom)
    -- tableView:registerScriptHandler(TableViewTestLayer.tableCellTouched,CCTableView.kTableCellTouched)
    -- tableView:registerScriptHandler(TableViewTestLayer.cellSizeForTable,CCTableView.kTableCellSizeForIndex)
    -- tableView:registerScriptHandler(TableViewTestLayer.tableCellAtIndex,CCTableView.kTableCellSizeAtIndex)
    -- tableView:registerScriptHandler(TableViewTestLayer.numberOfCellsInTableView,CCTableView.kNumberOfCellsInTableView)
    -- tableView:reloadData()

    -- Back Menu
    local pToMainMenu = CCMenu:create()
    CreateExtensionsBasicLayerMenu(pToMainMenu)
    pToMainMenu:setPosition(CCPointMake(0, 0))
    self:addChild(pToMainMenu,10)

    return true
end

function TableViewTestLayer.create()
    -- local layer = TableViewTestLayer.extend(CCLayer:create())
    -- if nil ~= layer then
    --     layer:init()
    -- end

    -- return layer

    local layer = TableViewTestLayer.extend(display.newNode())
    if nil ~= layer then
        layer:init()
    end

    return layer
end

local function runTableViewTest()
    local newScene = CCScene:create()
    local newLayer = TableViewTestLayer.create()    
    newScene:addChild(newLayer)
    return newScene
end
-- tableView end

function CreateExtensionsBasicLayerMenu(pMenu)
	if nil == pMenu then
		return
	end
	local function toMainLayer()
       local pScene = ExtensionsTestMain()
       if pScene ~= nil then
           CCDirector:sharedDirector():replaceScene(pScene)
       end
    end
    --Create BackMneu
    CCMenuItemFont:setFontName("Arial")
    CCMenuItemFont:setFontSize(24)
   	local pMenuItemFont = CCMenuItemFont:create("Back")
   	pMenuItemFont:setAnchorPoint(CCPoint(0,0))
    -- pMenuItemFont:setPosition(ccp(VisibleRect:rightBottom().x - 50, VisibleRect:rightBottom().y + 25))
    pMenuItemFont:registerScriptTapHandler(toMainLayer)
    pMenu:addChild(pMenuItemFont)
end

--  scrollView start
local function runScrollViewTest()
    local newScene = CCScene:create()
    local newLayer = CCLayer:create()

    -- Back Menu
    local pToMainMenu = CCMenu:create()
    CreateExtensionsBasicLayerMenu(pToMainMenu)
    pToMainMenu:setPosition(ccp(0, 0))
    newLayer:addChild(pToMainMenu,10)

    local layerColor = CCLayerColor:create(ccc4(128,64,0,255))
    newLayer:addChild(layerColor)

    local scrollView1 = CCScrollView:create()

    local scrollView2 = CCScrollView:create()

    local screenSize = CCDirector:sharedDirector():getWinSize()
    local xOffSet
    local yOffSet

    local function scrollView1DidScroll()
        print("scrollView1DidScroll",scrollView2)
         -- static int flag = 0;
         --    CCLog("Scroll %d",flag++);
            
            local offSet = scrollView1:getContentOffset() 
            scrollView2:setContentOffset(CCPoint(offSet.x, offSet.y));
            print("offSet : %f %f",offSet.x,offSet.y);
            -- if (offSet.x < xOffSet or offSet.y < yOffSet) then                
            --     print("scrollView 已经出现黑边问题了！")
                
            --     if (offSet.x < xOffSet ) then
            --         print("scrollView X轴 出现黑边问题了！");
            --         scrollView1:setContentOffset(CCPoint(xOffSet, offSet.y));
            --     else
            --         print("scrollView Y轴 已经出现黑边问题了！");
            --         scrollView1:setContentOffset(CCPoint(offSet.x, yOffSet));
            --     end
            -- end
            
            -- if (offSet.x > 0 or offSet.y > 0) then
            --     print("scrollView 已经出现黑边问题了！");
                
            --     if (offSet.x > 0 ) then
            --         print("scrollView X轴 出现黑边问题了！");
            --         scrollView1:setContentOffset(CCPoint(0, offSet.y));
            --     else
            --         print("scrollView Y轴 已经出现黑边问题了！");
            --         scrollView1:setContentOffset(CCPoint(offSet.x, 0));
            --     end                
            -- end

    end

    local function scrollView1DidZoom()
        print("scrollView1DidZoom")
    end
    -- 　现在我们开始创建CCScrollView对象了。
    -- 当然我们最好还是再创建一个Layer用来作为
    -- CCScrollView的Container。
    if nil ~= scrollView1 then
         -- 显示显示的区域

         -- 设置scrollView的大小,为显示的view的尺寸 
        scrollView1:setViewSize(CCSizeMake(screenSize.width / 2,screenSize.height))
        -- scrollView1:setViewSize(CCSizeMake(960/2,640/2)) 

        scrollView1:setPosition(CCPointMake(0,0))
        scrollView1:setScale(1.0)
        -- 对于场景或层等大型节点，它们的IgnoreAnchorPointForPosition属性为 true ，
        -- 此时引擎会认为 AnchorPoint 永远为(0,0)；
        scrollView1:ignoreAnchorPointForPosition(true)
        local flowersprite1 =  CCSprite:create("TEST/ccb/flower.jpg")
        if nil ~= flowersprite1 then

            -- 显示滑动的区域大小 scrollview的实际大小
            -- 设置容器
            scrollView1:setContainer(flowersprite1)
            -- 设置容器大小
            -- scrollView1:setContentSize(CCSizeMake(1024, 1024));  
            -- 是开启弹性效果，关闭的话就不用使用这个控件
            -- scrollView1:setBounceable(false);
            -- local flag = scrollView1:isBounceable();
            -- print("flag: %d",flag);

            scrollView1:updateInset()
        end
        -- 黑边防御坐标
        local scrollSize = scrollView1:getContentSize();
        -- local scrollSize = scrollView1:getViewSize();
        print("scrollSize: %f %f",scrollSize.width,scrollSize.height);
        xOffSet = screenSize.width - scrollSize.width;
        yOffSet = screenSize.height - scrollSize.height;
        print("offset x, y", xOffSet,yOffSet)


        -- 滑动方向控制
        scrollView1:setDirection(kCCScrollViewDirectionBoth)
        scrollView1:setClippingToBounds(true)
        scrollView1:setBounceable(true)
        scrollView1:registerScriptHandler(scrollView1DidScroll,CCScrollView.kScrollViewScroll)
        scrollView1:registerScriptHandler(scrollView1DidZoom,CCScrollView.kScrollViewZoom)
    end
    newLayer:addChild(scrollView1) 



    -- local scrollView2 = CCScrollView:create()
    local function scrollView2DidScroll()
        print("scrollView2DidScroll")
    end
    local function scrollView2DidZoom()
        print("scrollView2DidZoom")
    end
    if nil ~= scrollView2 then
        scrollView2:setViewSize(CCSizeMake(screenSize.width / 2,screenSize.height))
        scrollView2:setPosition(CCPointMake(screenSize.width / 2,0))
        scrollView2:setScale(1.0)
        scrollView2:ignoreAnchorPointForPosition(true)
        local flowersprite2 =  CCSprite:create("TEST/ccb/flower.jpg")
        if nil ~= flowersprite2 then
            scrollView2:setContainer(flowersprite2)
            scrollView2:updateInset()
        end
        scrollView2:setDirection(kCCScrollViewDirectionBoth)
        scrollView2:setClippingToBounds(true)
        scrollView2:setBounceable(true)
        scrollView2:setDelegate()
        scrollView2:registerScriptHandler(scrollView2DidScroll,CCScrollView.kScrollViewScroll)
        scrollView2:registerScriptHandler(scrollView2DidZoom,CCScrollView.kScrollViewZoom)
    end
    newLayer:addChild(scrollView2) 

    newScene:addChild(newLayer)
    return newScene
end 

-------------------------------------
--  Extensions Test
-------------------------------------
local flag = false
function ExtensionsTestMain() 
	flag = not flag
	local scene = CCScene:create()
	if (flag) then
		-- scene:addChild(runScrollViewTest())
        scene:addChild(runTableViewTest())
        
	else 
		scene:addChild(runTableViewTest()) 
	end 
	return scene
end