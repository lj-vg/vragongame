------通用提示面板 
-- Author: anjun
-- Date: 2014-02-21 14:27:42
--
local NormalAlertPanle = class("NormalAlertPanle", extendUI.AlertWindow)
function NormalAlertPanle:ctor(param) 
	 self.param_ = param or {}
    NormalAlertPanle.super.ctor(self,param)
end

function NormalAlertPanle:initView()  
	NormalAlertPanle.super.initView(self)
    local w = self.param_.w or display.width;
    local h = self.param_.h or display.height;  
    local background  = ( cc.ui.UIImage.new(COMMON__LIEBIAO_TAOTAI_LIANG_PNG, {scale9 = true})
                                             :setLayoutSize(w,h) )      
    self:setBackground(background)    

    local contentSprite = cc.ui.UIImage.new(COMMON__COMMON_KUANG_9PNG, {scale9 = true})
                    :setLayoutSize(w,h)      
    self:setContentSprite(contentSprite)

    local titleBar   = ( cc.ui.UIImage.new(COMMON__COMMON_BANNER_PNG, {scale9 = true})
                                        :setLayoutSize(self.param_.w,FinanceUtils.TopBarHeight) ) 
    self:setTitleBar(titleBar)      


    local okButton    = ( cc.ui.UIPushButton.new(COMMON_BUTTON_YELLOW, {scale9 = true}) 
                                            :setButtonLabel(ui.newTTFLabel({
                                                text = "確定",
                                                size = 20, 
                                            })) ) 
    self:setOkButton(okButton)   
    local cancelButton    = ( cc.ui.UIPushButton.new(COMMON_BUTTON_YELLOW, {scale9 = true}) 
                                            :setButtonLabel(ui.newTTFLabel({
                                                text = "取消",
                                                size = 20, 
                                            })) ) 
    self:setCancelButton(cancelButton) 
    self:setButtonToBottom(80)
end

function NormalAlertPanle:initEvent()
	NormalAlertPanle.super.initEvent(self) 
    app:addEventListener(ENABLE_SCROLL_TABLE_VIEW, handler(self, self.__enableScrollTableView))
end

function NormalAlertPanle:__enableScrollTableView( event ) 
    -- if ( self.enableScrollTableView ) then
    --     self:enableScrollTableView(event.data)
    -- end
end

function NormalAlertPanle:onExit()
    self:removeEvent()
end

function NormalAlertPanle:removeEvent()
app:removeEventListener(ENABLE_SCROLL_TABLE_VIEW, handler(self, self.__enableScrollTableView))
end

function NormalAlertPanle:dispose()
    app:enableScrollTableView(true)
	NormalAlertPanle.super.dispose( self);
end

function NormalAlertPanle:show() 
	managers.LayerManager:addChildToLayer(UI_LAYER,self,true,true )  
    app:enableScrollTableView(false)
end

function NormalAlertPanle:enableScrollTableView( flag )  
    if (self._scrollView) then 
        self._scrollView:setTouchEnabled(flag)
    end
    if (self._customDataGrid) then
        self._customDataGrid:enableControl(flag)
    end
end

function NormalAlertPanle:hide() 
end

return NormalAlertPanle;