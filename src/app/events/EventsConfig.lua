--
-- Author: msdnet
-- Date: 2014-03-20 15:56:58
--
-- EVENT_ANALYSIS_SOCKET = "analysis_socket" ;  --socket消息
-- EVENT_CLIENT_MESSAGE = "client_message"   ;  --client消息

CLIENT_EVENT_LOGIN_SUCCESS = "client_event_login_success"  --登陆成功
CLIENT_EVENT_LOGIN_FAIL = "client_event_login_fail"  	--登陆失败
CLIENT_EVENT_LOGIN_LOGOUT = "client_event_login_logout"  --注销登陆
C_GAME_USER_RE_LOGIN = "C_GAME_USER_RE_LOGING";   --重登陸

CLIENT_EVENT_SHOW_WINDOW = "client_event_show_window"       --顯示子窗口 
-- ENABLE_SCROLL_TABLE_VIEW = "enable_Scroll_Table_View"   --滾動條開關(cocos2dx哪套)   原因见NormalWindow的enableScrollTableView
-- CLIENT_EVENT_UPDATE_DATAGRID = "client_event_update_datagrid" --更新網格數據窗口 局部
-- 根据.socketID_主命令_子命令,来生成消息名字
function getEventTypeByCMD( socketId,mainCmdID,subCmdID ) 
	return tostring(socketId).."_"..tostring(mainCmdID).."_"..tostring(subCmdID)
end