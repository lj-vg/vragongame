--文字 ,图标 按纽 　
-- Author: anjun
-- Date: 2014-03-06 18:17:35
-- 后续跟进，暂不用
local IconButton = import(".IconButton")
local IconTextButton = class("IconTextButton", IconButton)  
function IconTextButton:ctor(images, options,iconImages) 
    IconTextButton.super.ctor(self, images, options,iconImages)   
    self.iconDirection_ = extendUI.Common.RIGHT
    extendUI.Common.makeNormalBtn(self)  -- this is bad design so... 
end
   

-- 取所有状态图片最大的宽与高
-- <br>还有个隐患:on; off 先后的问题，后续...
function IconTextButton:getMaxContentSize( )
    local image,size,tmpSprite
    size = CCSize(0,0)
    for _, image in pairs(self.images_) do  
        if image and string.len(image) ~= 0 then 
            tmpSprite = display.newSprite(image) 
            if (tmpSprite:getContentSize().width > size.width) then
                size.width = tmpSprite:getContentSize().width
            end
            if (tmpSprite:getContentSize().height > size.height ) then
                size.height =  tmpSprite:getContentSize().height
            end
        end
    end 
    tmpSprite = nil 
    return size
end  

return IconTextButton;

