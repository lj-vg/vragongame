------通用提示窗口 
-- Author: anjun
-- Date: 2014-02-21 14:27:42
--
local NormalAlertWindow = class("NormalAlertWindow", extendUI.AlertWindow)
function NormalAlertWindow:ctor(param) 
    self._param = param or {}
    NormalAlertWindow.super.ctor(self,param) 
end

function NormalAlertWindow:initView()  
	NormalAlertWindow.super.initView(self)
    local w = self._param.width or display.width;
    local h = self._param.height or display.height-FinanceUtils.ToolBarHeight
    local toph = FinanceUtils.TopBarHeight
    local windowsName = self._param.windowsName or "提示"
    local returnName = self._param.returnName or "返回"
 
    local background  = ( cc.ui.UIImage.new(COMMON__LIEBIAO_TAOTAI_LIANG_PNG, {scale9 = true})
                                             :setLayoutSize(w,h) )      
    self:setBackground(background)    
    local titleBar   = ( cc.ui.UIImage.new(COMMON__COMMON_BANNER_PNG, {scale9 = true})
                                        :setLayoutSize(w,toph) ) 
    self:setTitleBar(titleBar)  
    self:setTitleTxtStr(windowsName) 
    local contentSprite = cc.ui.UIImage.new(COMMON__COMMON_KUANG_9PNG, {scale9 = true})
                    :setLayoutSize(w-20,h-toph-10)      
    self:setContentSprite(contentSprite) 

    local closeButton = extendUI.IconButton.new(COMMON__TCL_UPAY_KEYBOARD_BTN_SELECTED_9PNG,{scale9 = true})
            :setButtonSize(80, 48)      
            :setButtonLabel("normal", ui.newTTFLabel({
                text = returnName,
                size = 30
            }))             
            :onButtonPressed(function(event)
                event.target:setOpacity(128)
            end)
            :onButtonRelease(function(event)
                event.target:setOpacity(255)
            end)             
    self:setCloseButton(closeButton)
    self:setClosetBtnPosDirection("left")

    local okButton    = ( cc.ui.UIPushButton.new(COMMON_BUTTON_YELLOW, {scale9 = true}) 
                                            :setButtonLabel(ui.newTTFLabel({
                                                text = "確定",
                                                size = 20, 
                                            })) ) 
    self:setOkButton(okButton)   
    local cancelButton    = ( cc.ui.UIPushButton.new(COMMON_BUTTON_YELLOW, {scale9 = true}) 
                                            :setButtonLabel(ui.newTTFLabel({
                                                text = "取消",
                                                size = 20, 
                                            })) ) 
    self:setCancelButton(cancelButton) 
end

function NormalAlertWindow:setWindowsName( name ) 
    self:setTitleTxtStr(name) 
end

function NormalAlertWindow:setReturnButtonName(name) 
    self.closeButton_:setButtonLabelString( name )
end

function NormalAlertWindow:initEvent()
	NormalAlertWindow.super.initEvent(self) 
    app:addEventListener(ENABLE_SCROLL_TABLE_VIEW, handler(self, self.__enableScrollTableView))
end

function NormalAlertWindow:__enableScrollTableView( event ) 
    if ( self.enableScrollTableView ) then
        self:enableScrollTableView(event.data)
    end
end

function NormalAlertWindow:onExit()
    self:removeEvent()
end

function NormalAlertWindow:removeEvent()
app:removeEventListener(ENABLE_SCROLL_TABLE_VIEW, handler(self, self.__enableScrollTableView))
end

function NormalAlertWindow:enableScrollTableView( flag )  
    if (self._scrollView) then 
        self._scrollView:setTouchEnabled(flag)
    end
    if (self._customDataGrid) then
        self._customDataGrid:enableControl(flag)
    end
end

function NormalAlertWindow:dispose()
	self:__comeBack()
	NormalAlertWindow.super.dispose( self);
end

-- 返回上一级
function NormalAlertWindow:__comeBack()
    local windowname,windowParam = app.mainModule:getLastWindow() 
    app:showTransitionSceneWindow(windowname,windowParam)  
end

return NormalAlertWindow;