--
-- Author: anjun
-- Date: 2014-01-16 15:07:21
--
require("framework.init")

-- #!/usr/bin/env lua 
require("app.framework.cc.utils.serialize")

local _unserialize_check, _test_value

-- function _test_value(data)
--   local types, r1, r2, dtype, check
--   types = {
--     ['n'] = 'Null',
--     ['b'] = 'Boolean',
--     ['i'] = 'Integer',
--     ['d'] = 'Float',
--     ['s'] = 'String',
--     ['a'] = 'Array',
--   }
--   r1 = serialize(data)
--   r2, dtype = unserialize(r1)
--   if r1 == serialize(r2) then
--     check = 'OK'
--   else
--     check = 'Fail!'
--   end
--   print(types[dtype], data, r1, serialize(r2), 'check:',  check)
-- end

-- _test_value(nil)

-- _test_value(true)

-- _test_value(false)

-- _test_value(1)

-- _test_value(-1)

-- _test_value(1.1)

-- _test_value(-1.1)

-- print('String 1 as Int ')
-- _test_value('1')

-- _test_value('text')

-- print('Array with booleans ')
-- _test_value(  unserialize(serialize({
--   [false] = true,
--   [true] = false,
-- }))    )

function eval(str)
    if type(str) == "string" then
        return loadstring("return " .. str)()
    elseif type(str) == "number" then
        return loadstring("return " .. tostring(str))()
    else
        error("is not a string")
    end
end

--验证eval函数
-- print(eval("111,333"))
print(eval("111+333*2"))


-- lua中table如何安全移除元素
-- http://childhood.logdown.com/posts/200499/lua-table-how-to-safely-remove-an-element

-- 1,
local test = { 2, 3, 4, 8, 9, 100, 20, 13, 15, 7, 11}
for i, v in ipairs( test ) do
    if v % 2 == 0 then
        table.remove(test, i)
    end
end

for i, v in ipairs( test ) do
    print(i .. "====" .. v)
end
-- 2,
local test = { 'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p' }
local remove = { a = true, b = true, c = true, e = true, f = true, p = true }

local function dump(table)
    for k, v in pairs( table ) do
        print(k)
        print(v)
        print("*********")
    end
end

for i = #test, 1, -1 do
    if remove[test[i]] then
        table.remove(test, i)
    end
end

dump(test)

-- print(eval("134+34*1224>1"))

-- print('Array with booleans ')
-- _test_value({
--   [1] = 'string',
--   [2] = 1,
--   a = 'str_key1',
--   ['b'] = 'str_key2',
-- })



-- 注释语句
 
--[[  
注释段落语句
  ]]--
 
--引用其他lua文件,不需要加上（.lua）后缀
--require "xx"
 
--变量不需要定义，可以直接赋值
count = 100  --成员变量
local count =100  --局部变量
 
--方法定义
function hello(...)
    --打印
    print("Hello Lua!");
    print(string.format(...))
end
 
-- 每一行代码不需要使用分隔符，当然也可以加上
-- 访问没有初始化的变量，lua默认返回nil
 
-- 调用函数形式
-- hello("你懂的")
if (1 ==1) then
  return;
end
--打印变量的类型
isOK =false
print(type(isOK))
 
-- 基本变量类型
a =nil --Lua 中值为nil 相当于删除
b =10
c =10.4
d =false
--定义字符串，单引号，双引号都可以的
e ="i am"
d ='himi'
 
--两个字符串的连接可以如下形式
stringA ="Hi"
stringB ="mi"
print(stringA..stringB)
 
--另外Lua也支持转移字符，如下
print(stringA.."\n"..stringB);
 
--修改字符串的部分gsub，可以如下形式:(将stringA字符串中的Hi修改为WT)
stringA=string.gsub(stringA,"Hi","WT")
print(stringA);
 
--将字符换成数字tonumber(不转也会自动转)
--将数字换成字符tostring(不转也会自动转)
stringC = "100"
stringC = tonumber(stringC)
stringC = stringC +20
stringC = tostring(stringC)
print(stringC)
 
--取一个字符串的长度使用 #
print(#stringC)
 
--创建 表
tableA ={}
m = "x"
tableA[m] =100
m2 ='y'
tableA[m2] =200
print(tableA["x"].."\n"..tableA.y)
--另外表还可以如下形式（从1开始）
tableB ={"4","5","6","7","8"}
print(tableB[1])
 
--算术操作符
c1 = 10+2
c2 = 10-2
c3 = 10*2
c4 = 10/2
c5 = 10^2
c6 = 10%2
c7 = -10+2
print(c1.."_"..c2.."_"..c3.."_"..c4.."_"..c5.."_"..c6.."_"..c7)
 
--控制操作
--if then elseif then else end
abc =10
if  abc ==10 then
    print("v1")
elseif abc == 9 then
    print("v2")
else
    print("v3")
end
 
--for
--从4（第一个参数）涨到10（第二个参数），每次增长以2（第三个参数）为单位
for i=4,10,2 do
    print("for1:"..i+1)
end
--也可以不制定最后一个参数，默认1的增长速度
for i=4,10 do
    print("for2:"..i+1)
end
 
tableFor = {"himi1","himi2","himi3","himi4","himi5"}
for k,v in pairs(tableFor) do
    print("for3:key:"..k.."value:"..v)
end
 
--while
w1 = 20
while true do
    w1=w1+1
    if w1 ==25 then
        break
    end
end
print("whlile:"..w1)
 
--repeat
    aa =20
    repeat aa = aa+2
        print("repeat:"..aa)
    until aa>28
 
--关系操作符
--需要注意的是不等于符号 ~=  而不是!=
ax =10
bx =20
 
if ax >bx then
    print("GX1")
elseif ax<bx then
    print("GX2")
elseif ax>=bx then
    print("GX3")
elseif ax<=bx then
    print("GX4")
elseif ax==bx then
    print("GX5")
elseif ax~=bx then
    print("GX6")
else
    print("GX7")
end




























-- local ClassA = class("ClassA")
-- ClassA.field1 = "this is field1"

-- echo("BEGIN TESTMY")
--  local _a1 = {20, 1, key1 = "hello", key2 = "world", lang = "lua"}
--   print("the table _a1:")
--   for _,v in pairs(_a1) do
--       print(v)
--   end
  
--   local _a2 = { 
--       key1 = "hello new",
--       key2 = "world new"
--   }
  
--   print("\nthe old table _a2:")
--   for _,v in pairs(_a2) do
--       print(v)
--   end
  
--   print("\na2的metatable:",getmetatable(_a2))
--   print("language:",_a2["lang"])

--   -- setmetatable(_a2, {__index = _a1})  
--   setmetatable(_a2, _a1)  
   
--   print("\nthe new table _a2:")
--   for _,v in pairs(_a2) do
--       print(v)
--   end  
--   print("\nlanguage:", _a2["lang"])

--     --数组是从1开始的哈哈
--   print("_a2数组第一个元素:", _a2[1])
--   print("\n_a2的metatable:",getmetatable(_a2))

-- print("END TESTMY")
-- t = {}
-- print(getmetatable(t)) --nil

-- mt = {name = "quick"} 
-- setmetatable(t, {__index = mt} )
-- echo(t.name)
-- print(getmetatable(t))
-- assert(getmetatable(t) == mt)

-- --====================Person======================
-- local Person = {}
-- Person.attack = 5

-- function Person:new(o)
--     o = o or {}
--     setmetatable(o, self)
--     self.__index = self
--     return o
-- end

-- function Person:setAttack(attack)
--     self.attack = attack
-- end

-- function Person:getAttack()
--     return self.attack
-- end


-- --====================Hero======================
-- local Hero = Person:new()
-- Hero.name = ""
-- Hero.skill = ""


-- --====================hero1,hero2======================
-- local hero1 = Hero:new()
-- hero1.name = "金刚狼"
-- hero1.skill = "甩开爪子切牛排"

-- local hero2 = Hero:new({name = "超人"})
-- hero2.skill = "内裤外穿走T台"


-- ----====================================================
-- function printKeys(name, t)
--     print("======================" .. name)
--     for k, v in pairs(t) do
--         print(k)
--     end
-- end


-- printKeys("Person", Person)
-- printKeys("Person.__index", Person.__index)

-- printKeys("Hero", Hero)
-- printKeys("Hero.__index", Hero.__index)
-- printKeys("getmetatable(Hero).__index", getmetatable(Hero).__index)

-- printKeys("hero1", hero1)

-- printKeys("hero2", hero2)



return ClassA