--通用窗口
-- Author: anjun
-- Date: 2014-08-08 23:32:26
--改進版

-- (包含滚动组件)　
local Window = import(".Window")
local CommonWindow = class("CommonWindow", Window)
function CommonWindow:ctor( param )
	self._param = param or {}
	CommonWindow.super.ctor(self,param)  
end

function CommonWindow:initView()    
    CommonWindow.super.initView(self)  

    local w = self._param.width or display.width;
    local h = self._param.height or display.height-FinanceUtils.ToolBarHeight
    local toph = FinanceUtils.TopBarHeight
    local windowsName = self._param.windowsName or "提示"
    local returnName = self._param.returnName or "返回"
 
    local background  = ( cc.ui.UIImage.new(COMMON__LIEBIAO_TAOTAI_LIANG_PNG, {scale9 = true})
                                             :setLayoutSize(w,h) )      
    self:setBackground(background)    
    local titleBar   = ( cc.ui.UIImage.new(COMMON__COMMON_BANNER_PNG, {scale9 = true})
                                        :setLayoutSize(w,toph) ) 
    self:setTitleBar(titleBar)  
    self:setTitleTxtStr(windowsName) 
    local contentSprite = cc.ui.UIImage.new(COMMON__COMMON_KUANG_9PNG, {scale9 = true})
                    :setLayoutSize(w-20,h-toph-10)      
    self:setContentSprite(contentSprite) 

    local closeButton = extendUI.IconButton.new(COMMON__TCL_UPAY_KEYBOARD_BTN_SELECTED_9PNG,{scale9 = true})
            :setButtonSize(80, 48)      
            :setButtonLabel("normal", ui.newTTFLabel({
                text = returnName,
                size = 30
            }))             
            :onButtonPressed(function(event)
                event.target:setOpacity(128)
            end)
            :onButtonRelease(function(event)
                event.target:setOpacity(255)
            end)             
    self:setCloseButton(closeButton)
    -- closeButton:setContentSize(CCSize(80,48))
    self:setClosetBtnPosDirection("left")

    -- 滚动组件及其它列表组件
    self:createScrollPanel()
end

function CommonWindow:setWindowsName( name ) 
	self:setTitleTxtStr(name) 
end

function CommonWindow:setReturnButtonName(name) 
	self.closeButton_:setButtonLabelString( name )
end

-- 創建滾動組件 OR overrideclass重载
function CommonWindow:createScrollPanel( scrollViewSize )  
    scrollViewSize = scrollViewSize or self:getWinContentSize()
    self._scrollView = extendUI.ScrollView.new( scrollViewSize ) 
    self:addContentChild(self._scrollView) 
    self._scrollView:setBounceable(true)
    self._scrollView:setDirection(kCCScrollViewDirectionVertical) 
    self._scrollView:setTouchEnabled(true)
end

function CommonWindow:fixPos(  )
    CommonWindow.super.fixPos(self)
    local posX,posY =0,0   
    local w = self:getContentSize().width
    local h = self:getContentSize().height
    if (self.closeButton_ ~= nil ) then 
        local buttonSize = CCSize(0,0) 
        if not self.closeButton_.scale9Size_ then
            buttonSize = self.closeButton_.sprite_:getContentSize() 
        else 
            buttonSize = CCSize(self.closeButton_.scale9Size_[1],self.closeButton_.scale9Size_[2]) 
        end  
        if (self.closeBtnPosdirection_ == "left") then --后续改进
            posX = buttonSize.width
            posY = h-buttonSize.height/2;   
        else
            posX = w - buttonSize.width
            posY = h-buttonSize.height/2;                   
        end     
        -- anjun end 
        if ( self.titleBar_ ~= nil ) then
            posY =  ( (h - (self:getTitleBarSize().height-10)/2) );                          
        end 
        self.closeButton_:setPositionX(posX)
        self.closeButton_:setPositionY(posY)
    end
    if (self.contentSprite_ ~= nil ) then       
        posX = (w - self.contentSprite_:getContentSize().width)/2
        if (self.titleBar_ ~= nil ) then    --注意cocos2dx的座标系
            posY = self.titleBar_:getPositionY() - self.contentSprite_:getContentSize().height 
        else 
            posY = (h-self.contentSprite_:getContentSize().height) - 5
        end 
        self.contentSprite_:setPositionX(posX)
        self.contentSprite_:setPositionY(posY)
    end
end

function CommonWindow:__alertClose(event)   --这有问题 why??? 终于弄懂原因.see windows 的
    self:__comeBack()
    self.closeButton_:removeTouchEventListener()
    self.closeButton_:removeFromParentAndCleanup(true)
    self.closeButton_ = nil   
    self:dispose()
end

-- 返回上一级
function CommonWindow:__comeBack()
	local windowname,windowParam = app.mainModule:getLastWindow() 
	app:showTransitionSceneWindow(windowname,windowParam)  
end

-- why see enableScrollTableView 这里合适么？
function CommonWindow:__enableScrollTableView( event ) 
    if ( self.enableScrollTableView ) then
        self:enableScrollTableView(event.data)
    end
end

function CommonWindow:initEvent()
    CommonWindow.super.initEvent(self)  
    self:addEventListener(extendUI.Window.CLOSE, handler(self, self.__alertClose) );  
app:addEventListener(ENABLE_SCROLL_TABLE_VIEW, handler(self, self.__enableScrollTableView))
end 

function CommonWindow:onExit()
    self:removeEvent()
end

function CommonWindow:removeEvent()
    app:removeAllEventListenersForEvent(ENABLE_SCROLL_TABLE_VIEW)  
end

function CommonWindow:onEnter() 
    if device.platform ~= "android" then return end 
    platformCustom.AndroidCutom.comeBackAction(self, handler(self, self.__androidComeBack) ) 
end

-- 按android返回鍵的回調
function CommonWindow:__androidComeBack()
    -- overridclass 實現
    self:__alertClose()
end

function CommonWindow:setVisible(flag) 
    getmetatable(self).setVisible(self,flag)    
end

--滾動條開關(cocos2dx哪套)
-- 如果不手動控制,會有吞噬触摸
    -- CCScrollView(cocos2dx)與quick的事件衝突
    -- 希望2.2.3版本能解決吧.後續升級(2.2.3目前不穩定)

-- quick2.2.1 touch事件
-- http://quick.cocoachina.com/?p=1504
-- 1. 任意 Node 都可以响应触摸事件，并且按照该 Node 及其所有子 Node 占用的屏幕空间来判断触摸响应区域；
-- 2. 按照 Node 的渲染次序来响应触摸，也就是说最上层的 Node 最先响应触摸，用来制作对话框就很方便了。
-- 3. 在 Lua 框架里提供了多种按钮类型。

-- quick 2.2.3事件
-- https://github.com/dualface/cocos-docs/blob/master/manual/framework/quick/how-to/upgrade-to-2_2_3/zh.md
function CommonWindow:enableScrollTableView( flag )  
    print("enableScrollTableView",flag,self._customDataGrid)
    if (self._scrollView) then 
        self._scrollView:setTouchEnabled(flag)
    end
    if (self._customDataGrid) then
        self._customDataGrid:enableControl(flag)
    end
end

return CommonWindow