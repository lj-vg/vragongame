--测试场景
-- Author: anjun
-- Date: 2014-06-02 12:26:57
-- 
local TestButtons = import("..testUnits.TestButtons")  
    -- local TestRichLabel = import("..testUnits.TestRichLabel")   
local TestWindow = import("..testUnits.TestWindow")   
local TestUIPageView = import("..testUnits.TestUIPageView")   
local TestUIListView = import("..testUnits.TestUIListView") 
local TestUIScrollView = import("..testUnits.TestUIScrollView")   
local TestTouchEvent = import("..testUnits.TestTouchEvent")
local TestMV = import("..testUnits.TestMV") 


local TestScene = class("TestScene", function()
    return display.newScene("TestScene")
end)

function TestScene:ctor()
end
-- -- 获取一个格式化后的浮点数
-- local function str_formatToNumber(number, num)
--     local s = "%." .. num .. "f"
--     return tonumber(string.format(s, number))
-- end

-- -- 全角 半角
-- function TestScene:accountTextLen(str, tsize)
--     local list = self:tab_cutText(str)
--     local aLen = 0
--     for k,v in pairs(list) do
--         local a = string.len(v)
--         -- 懒得写解析方法了
--          local label = ui.newTTFLabel({text = v, size = tsize})
--         a = tsize/(label:getContentSize().width)
--         local b = str_formatToNumber(ChineseSize/a, 4)
--         aLen = aLen + b
--         label:release()
--     end 
--     return aLen
-- end 

-- -- 拆分出单个字符
-- function TestScene:tab_cutText(str)
--     local list = {}
--     local len = string.len(str)
--     local i = 1 
--     while i <= len do
--         local c = string.byte(str, i)
--         local shift = 1
--         if c > 0 and c <= 127 then
--             shift = 1
--         elseif (c >= 192 and c <= 223) then
--             shift = 2
--         elseif (c >= 224 and c <= 239) then
--             shift = 3
--         elseif (c >= 240 and c <= 247) then
--             shift = 4
--         end
--         local char = string.sub(str, i, i+shift-1)
--         i = i + shift
--         table.insert(list, char)
--     end
--     return list, len
-- end

function TestScene:onEnter()    
    -- cc.Director:getInstance():replaceScene(TestButtons.new() )   
    -- cc.Director:getInstance():replaceScene(TestWindow.nsew() )  
-- cc.Director:getInstance():replaceScene(TestRichLabel.new() )  
    -- cc.Director:getInstance():replaceScene(TestUIPageView.new() )  
    -- cc.Director:getInstance():replaceScene(TestUIListView.new() )  
    -- cc.Director:getInstance():replaceScene(TestUIScrollView.new() )  
    -- cc.Director:getInstance():replaceScene(TestTouchEvent.new() )
    cc.Director:getInstance():replaceScene(TestMV.new() )   
 --1408102714
    -- local t = os.date("*t", 1408102714)
    -- print('date',t.year,t.month,t.day) 
end

function TestScene:onExit()
	
end

return TestScene