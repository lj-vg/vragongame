--测试Audio
-- Author: anjun
-- Date: 2014-02-28 17:47:04
 

local function runTestAudio()
    local newScene = CCScene:create()
	local layer = CCLayer:create() 

	-- local sprite = CCSprite:create("button_confirm_on.png")
	local sprite =  display.newSprite("button_confirm_on.png") --这样扩展了很多方便属性
    sprite:setPosition(CCPointMake(100, display.height / 2))
    -- left:runAction(CCRepeatForever:create(to1))
	layer:addChild(sprite)
	newScene:addChild(layer)


    -- 场景中的按钮
    local leftMenu = cc.ui.UIPushButton.new("common/slip_btn.png")
        :onButtonPressed(function(event)
            print("left menu pressed")
        end)
        :onButtonRelease(function(event)
            print("left menu release")
        end)
        :onButtonClicked(function(event)
			audio.playSound("TEST/effect1.wav")
        end)
        :align(display.CENTER, display.cx - 100, display.cy)
        :addTo(layer)

-- audio.playMusic, audio.playBackgroundMusic  见 http://wiki.quick-x.com/doku.php?id=zh_cn:api:framework.audio
-- 播放音乐。 
		-- 格式： 
		-- audio.playMusic(音乐文件名, [是否循环播放])
		-- 如果音乐文件尚未载入，则会首先载入音乐文件，然后开始播放。默认会无限循环播放音乐。

		-- 注意，即便音乐音量为 0，playMusic() 仍然会进行播放操作。如果希望停止音乐来降低 CPU 占用，应该使用 stopMusic() 接口完全停止音乐播放。

		-- ~~	
 
	local audioIndex = audio.playSound("TEST/background.mp3",true)

	-- audio.playBackgroundMusic("TEST/effect1.wav",true)

	local updateGlobalIndex = 0
    local function __callBack( ...) 
    	 extendUI.scheduler.unscheduleGlobal(updateGlobalIndex)  
    	 -- audio.stopMusic()
    	 -- audio.stopAllSounds()
    	 audio.stopSound(audioIndex)

    	 print(···)
    end 
    -- updateGlobalIndex = extendUI.scheduler.scheduleUpdateGlobal(__callBack)
    updateGlobalIndex = extendUI.scheduler.scheduleGlobal(__callBack,4)

    return newScene
end 

function TestAudioMain()  
	local scene = CCScene:create() 
    scene:addChild(runTestAudio()) 
	return scene
end 