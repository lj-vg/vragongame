--测试SceneTransition
-- Author: anjun
-- Date: 2014-02-28 14:22:27 

local function runTestSceneTransition()
    local newScene = CCScene:create()
	local layer = CCLayer:create()  
 
 	     -- 背景
    display.newColorLayer(ccc4(255, 255, 255, 255)):addTo(newScene) 
    for y = display.bottom, display.top, 40 do
        local line = display.newPolygon({{display.left, y}, {display.right, y}}):addTo(newScene)
        line:setLineColor(ccc4f(0.9, 0.9, 0.9, 1.0))
    end

    for x = display.left, display.right, 40 do
        local line = display.newPolygon({{x, display.top}, {x, display.bottom}}):addTo(newScene)
        line:setLineColor(ccc4f(0.9, 0.9, 0.9, 1.0))
    end

    local line = display.newPolygon({{display.left, display.cy}, {display.right, display.cy}}):addTo(newScene)
    line:setLineColor(ccc4f(1.0, 0.75, 0.75, 1.0))

    local line = display.newPolygon({{display.cx, display.top}, {display.cx, display.bottom}}):addTo(newScene)
    line:setLineColor(ccc4f(1.0, 0.75, 0.75, 1.0))  


    local setButton = cc.ui.UIPushButton.new( "common/tcl_upay_keyboard_btn_selected.9.png",{scale9 = true},"login/login_set.png")
        :setButtonSize(200, 30)   
        :setButtonLabel("normal", ui.newTTFLabel({
        text = "SceneTransition",
        size = 18
        }))           
        :onButtonPressed(function(event)
            event.target:setScale(0.9) 
        end)
        :onButtonRelease(function(event)
            event.target:setScale(1.0)
        end)      
        :onButtonClicked(function ( event )
        	-- body
        	print(event)
		    -- 创建一个新场景
		    local nextScene = display.newScene("LScene")
		    nextScene:addChild( TestActionMain() )

		    -- 包装过渡效果
		    local transitionNextScene = display.wrapSceneWithTransition(nextScene, "flipAngular", 0.5,kCCTransitionOrientationLeftOver)
		    -- 切换到新场景 
		    -- CCDirector:sharedDirector():replaceScene(transitionNextScene)  
		    -- app.enterScene("LoginScene", args, transitionType, time, more)	
		    -- app:enterScene("LoginScene", nil, "fade", 0.6, display.COLOR_WHITE)  		     

	    -- display.replaceScene 
		-- 切换到新场景。 
			-- 格式： 
			-- display.replaceScene(场景对象, [过渡效果名], 过渡时间, [过渡效果附加参数])
			-- 用法示例： 
			-- 使用红色做过渡色
			display.replaceScene(nextScene, "fade", 0.5, ccc3(255, 0, 0))
		    -- 切换到新场景
			-- display.replaceScene(nextScene,"fade",0.5,display.COLOR_BLACK) 
		-- scheduler.performWithDelayGlobal 
		-- 计划一个全局延时回调，并返回该计划的句柄。 
			-- 格式： 
			-- handle = scheduler.performWithDelayGlobal(回调函数, 延迟时间)
			-- scheduler.performWithDelayGlobal() 会在等待指定时间后执行一次回调函数，然后自动取消该计划。
			local function __callBack( )
				-- display.resume()   --暂停当前场景。
				display.pause()    --恢复当前暂停的场景。
				print("__callBack")
			end
			extendUI.scheduler.performWithDelayGlobal(__callBack, 1)
			
			
			
			-- 可用的过渡效果有： 
			-- crossFade 淡出当前场景的同时淡入下一个场景
			-- fade 淡出当前场景到指定颜色，默认颜色为 ccc3(0, 0, 0)，可用 wrapSceneWithTransition() 的最后一个参数指定颜色
			-- fadeBL 从左下角开始淡出场景
			-- fadeDown 从底部开始淡出场景
			-- fadeTR 从右上角开始淡出场景
			-- fadeUp 从顶部开始淡出场景
			-- flipAngular 当前场景倾斜后翻转成下一个场景，默认从左边开始翻转，可以指定为：
			-- kCCTransitionOrientationLeftOver 从左边开始
			-- kCCTransitionOrientationRightOver 从右边开始
			-- kCCTransitionOrientationUpOver 从顶部开始
			-- kCCTransitionOrientationDownOver 从底部开始
			-- flipX 水平翻转，默认从左往右翻转，可用的附加参数同上
			-- flipY 垂直翻转，默认从上往下翻转，可用的附加参数同上
			-- zoomFlipAngular 倾斜翻转的同时放大，可用的附加参数同上
			-- zoomFlipX 水平翻转的同时放大，可用的附加参数同上
			-- zoomFlipY 垂直翻转的同时放大，可用的附加参数同上
			-- jumpZoom 跳跃放大切换场景
			-- moveInB 新场景从底部进入，现有场景同时从顶部退出
			-- moveInL 新场景从左侧进入，现有场景同时从右侧退出
			-- moveInR 新场景从右侧进入，现有场景同时从左侧退出
			-- moveInT 新场景从顶部进入，现有场景同时从底部退出
			-- pageTurn 翻页效果，如果指定附加参数为 true，则表示从左侧往右翻页
			-- rotoZoom 旋转放大切换场景
			-- shrinkGrow 收缩交叉切换场景
			-- slideInB 新场景从底部进入，直接覆盖现有场景
			-- slideInL 新场景从左侧进入，直接覆盖现有场景
			-- slideInR 新场景从右侧进入，直接覆盖现有场景
			-- slideInT 新场景从顶部进入，直接覆盖现有场景
			-- splitCols 分成多列切换入新场景
			-- splitRows 分成多行切换入新场景，类似百叶窗
			-- turnOffTiles 当前场景分成多个块，逐渐替换为新场景		 
        end)
        :pos(display.left + 140, display.top - 60)
        :addTo(layer) 
         
	newScene:addChild(layer)
    return newScene
end 



function TestSceneTransitionMain() 
	flag = not flag
	local scene = CCScene:create() 
    scene:addChild(runTestSceneTransition()) 
	return scene
end 