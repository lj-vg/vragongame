--
-- Author: anjun
-- Date: 2014-01-16 15:39:29
--
require("framework.init")
local ClassC = class("ClassC", function ()
	-- 调用C++接口创建一个原生对象(userdata), 并且给该对象绑定一个peer（table), 这里我们以创建一个CCNode为例
	local node = CCNode:create()
	-- local peer = {} 
	-- tolua.setpeer(node,peer) 
	return node
end)
return ClassC