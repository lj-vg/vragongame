--定制的CustomTableView
-- Author: anjun
-- Date: 2014-06-24 14:57:13
--可上下 左右移动哦...good
local CustomTableView = class("CustomTableView",function(handleFun, viewSize,displayBg )
	viewSize = viewSize or CCSize(0,0)
	displayBg = displayBg or display.newSprite()
	local customTableHandle = CustomEventHandler:create( handleFun )
	local customTableView = CustomTableView:createWithHandler(customTableHandle,viewSize,displayBg )
    return customTableView
end)

function CustomTableView:ctor(handleFun, viewSize,displayBg) 
    -- self:initziation() 
end

--如果用这里的控制是比较符合封装，但是没办法控制吞噬cell上面的button触摸
-- so...
function CustomTableView:initziation() 
    self:initView()
    self:initEvent()    
end
function CustomTableView:initView() 
end

function CustomTableView:initEvent()
    self:getContainer():setTouchEnabled(true)
    self:getContainer():registerScriptTouchHandler(function(event, x, y)
        return self:onTouch(event, x, y)
    end,false,0,false)     
end

-- override  ??为报错后续跟进
-- function CustomTableView:setTouchEnabled(flag) 
--     getmetatable(self).setTouchEnabled(self, flag)
--     getmetatable(self).setPosition(self, x, y) 
--     -- self:getContainer():setTouchEnabled(flag)
-- end

-- 组件是否可触摸
function CustomTableView:enableControl(flag)
    self:setTouchEnabled(flag)
    self:getContainer():setTouchEnabled(flag)
end

-- 偏移Ｘ坐标()
function CustomTableView:moveOffsetX( offsetPosX,animationed) 
    if self:restoreY() then
        return 
    end
    animationed = animationed or false
    local offset = self:getContentOffset(); 
    local size = self:getContainer():getContentSize();

    offset.x = offset.x+offsetPosX;
    local maxX =  0 
    local minX =  -(size.width - self:getViewSize().width)
    if offset.x  > maxX then
        offset.x = maxX
    elseif offset.x  < minX then
        offset.x = minX
    end
    -- print("minX,maxX",minX,maxX,size.width,self:getViewSize().width)
    self:setContentOffset(CCPointMake(offset.x,offset.y),animationed); 
end

function CustomTableView:restoreY() --歸位Y座標
    local result = false
    local offset = self:getContentOffset(); 
    local size = self:getContainer():getContentSize(); 
    local maxY =  0 
    local minY =  self:getViewSize().height - size.height     
    if (minY >0) then --內容小於顯示的size 
        if ( offset.y ~= minY) then
            offset.y = minY
            result = true
        end 
    else
        if (offset.y < minY) then
            offset.y = minY
            result = true
        elseif offset.y > maxY then
            offset.y = maxY
            result = true
        end       
    end 
    -- print("歸位2",result)
    if (result) then  
        self:setContentOffset(CCPointMake(offset.x,offset.y))
    end
    
    return result
end

function CustomTableView:onTouch(event, x, y)
	local startPos = 0
    local endPos = 0 
    if event == "began" then  
		self._startX = x
		self._startY = y
		self.lastdirection_ = 0
		self.direction_ = 0
        return true 
    elseif event == "moved" then  
	    startPos = CCPoint(self._startX,self._startY)
	    endPos  = CCPoint(x,y)    
	    self.direction_,offsetX= extendUI.Common.getDirection(startPos,endPos)   
        if (self.lastdirection_ ~= self.direction_) then
            if (self.lastdirection_ ~= 0) then  --用户转向了
                if (math.abs(self.direction_) ~= math.abs(self.lastdirection_)) then --用户转向只能是上，下或者左,右;其他不行
                     self._startX = x
                     self._startY = y        
                     self.lastdirection_ = 0  
                     return true    
                end
            else
                self.lastdirection_ = self.direction_
            end
        end

		if (self.direction_ == extendUI.Common.LEFT or self.direction_ == extendUI.Common.RIGHT) then
		  self:setTouchEnabled(false);
		if ( math.abs(offsetX) > 0.04) then 
		    self:moveOffsetX(offsetX) 
		end 
		else 
            self:setTouchEnabled(true); 
		end
        return true  
    elseif event == "ended"then
    	startPos = CCPoint(self._startX,self._startY)
    	endPos  = CCPoint(x,y)    
    	self.direction_,offsetX = extendUI.Common.getDirection(startPos,endPos)
    	if (self.direction_ == extendUI.Common.LEFT or self.direction_ == extendUI.Common.RIGHT) then
    	    self:moveOffsetX(offsetX,true)  
    	end
        self:setTouchEnabled(true);
        return true
    else -- cancelled  
        self:setTouchEnabled(true);
    	return true
    end
end 

return CustomTableView