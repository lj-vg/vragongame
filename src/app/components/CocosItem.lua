--Cocos CocosItem基类 真是纠结．．．．
-- Author: anjun
-- Date: 2014-06-04 17:13:21
-- 暂是没用到
local CocosItem = class("CocosItem", function(contentSize)
    -- local node = display.newSprite()
    -- -- local node = display.newLayer()
    -- if contentSize then node:setContentSize(contentSize) end
    -- node:setNodeEventEnabled(true) 
    -- cc(node)
    --     :addComponent("components.behavior.EventProtocol")
    --     :exportMethods()
    local node = Layout:create()
    if contentSize then node:setSize(contentSize) end
    return node
end)

function CocosItem:ctor() 	
    -- makeUIControl_(self)
    -- self:setNodeEventEnabled(true)
    self:initiation()
end

function CocosItem:initiation()
    self._info = nil
    self:initView()
    self:initEvent()    
end

function CocosItem:initView()

end 

function CocosItem:initEvent()

end

function CocosItem:onTouch(event)

end

function CocosItem:onTap(x, y)

end

function CocosItem:fixPos() 
end

function CocosItem:update(info)
	self._info = info
end 

function CocosItem:onExit() 
    self:removeAllEventListeners()
end

function CocosItem:width()
    return self:getContentSize().width
end

function CocosItem:height()
    return self:getContentSize().height
end

function CocosItem:onExit()
    self:removeAllEventListeners()
end

function CocosItem:info()
    return self._info
end

return CocosItem