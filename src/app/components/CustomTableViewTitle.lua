--region CustomTableViewTitle.lua
--带标题栏的CustomTableView
--Author : anjun
--Date   : 2014/6/25
--此文件由[BabeLua]插件自动生成
local CustomTableViewTitle = class("CustomTableViewTitle", function()
    -- return display.newNode()  --用这个Ｔouch事件会用问题
    return display.newLayer()
end)

CustomTableViewTitle.STARTSCROLLING   = "startScrolling"
CustomTableViewTitle.STOPSCROLLING  = "stopScrolling" 

function CustomTableViewTitle:ctor(callFun,size,titleSprite)
    self:setContentSize(size)
    self._handleCallFun = callFun

    self._isScrolling= false
    self._isScrllCount = 0
    self._isLastScrllCount = -1
    -- self._scrollFun = scrollFun    
    
    self._titleSprite = titleSprite 
    self._titleSprite = self._titleSprite or display.newSprite()  --容错
    self:initView(size,self._titleSprite:getContentSize())
    self:createTitle(self._titleSprite)
    makeUIControl_(self)
    self:setNodeEventEnabled(true)
    self:initEvent() 
end

function CustomTableViewTitle:startPerformedAnimatedScroll()
    local function callback() 
        if (self._isLastScrllCount ~= self._isScrllCount) then
            self._isLastScrllCount = self._isScrllCount
        else
            if (self._isLastScrllCount ~= 0) then
                self:stopedScroll()
            end;            
        end
    end
    self._performedTimeHandle = extendUI.scheduler.scheduleGlobal(callback,1/60)
end

function CustomTableViewTitle:stopedScroll()
    self:stopPerformedAnimatedScroll()
    self._isScrolling = false
    self._isScrllCount = 0
    self._isLastScrllCount = -1
    self:dispatchEvent({name=CustomTableViewTitle.STOPSCROLLING} )
end

function CustomTableViewTitle:stopPerformedAnimatedScroll()
    if ( self._performedTimeHandle ) then
        extendUI.scheduler.unscheduleGlobal( self._performedTimeHandle )
    end
    self._performedTimeHandle = nil    
end
--[[]
    是否正在滚动;
        以上就是判断这个,新版本会有更好的办法么?
        否则边滚动;里面的button也会响应事件
--]]
function CustomTableViewTitle:isScrolling() 
    return self._isScrolling
end

function CustomTableViewTitle:__handle( fn, table, a1, a2 )
    if (fn == "scroll") then
         local offSet = table:getContentOffset()   
         if ( self._titleScorllView ~= nil ) then --关联标题
             local offSetOther = self._titleScorllView:getContentOffset()
             if (offSet.x ~= offSetOther.x) then 
                 self._titleScorllView:setContentOffset(CCPoint(offSet.x, offSetOther.y));  
             end          
         end
         -- control touch 
        if ( not self._customTableView:isTouchMoved()  ) then
            if ( self._isScrllCount == 0 ) then
                self:startPerformedAnimatedScroll()
            end 
            self._isScrllCount = self._isScrllCount + 1
        end       
        self._isScrolling = true
        if ( self._isLastScrllCount == -1) then
            self:dispatchEvent({name=CustomTableViewTitle.STARTSCROLLING} )
            self._isLastScrllCount = 0
        end
        -- control end


    end
    if ( self._handleCallFun ) then
        local titleItemCount = self:getTitleItemCount()
        local r = self._handleCallFun(fn, table, a1, a2,titleItemCount )
        if (fn == "numberOfCells") then   
            self:setItemCount(r)
        end      
        return r
    end
end


function CustomTableViewTitle:initView(size,titleSize)    
    local tableViewSize = CCSize(size.width,size.height )    
    self._customTableView = extendUI.CustomTableView.new(handler(self, self.__handle), tableViewSize)
    self._customTableView:setBounceable(true) 
-- self._customTableView:setVerticalFillOrder(kCCTableViewFillTopDown)
    self._customTableView:setDirection(kCCScrollViewDirectionVertical) 
    self._customTableView:setAnchorPoint(CCPointMake(0, 0))
    self._customTableView:setPosition( CCPointMake(0, 0) )  
    self._customTableView:setClippingToBounds(true) 
    self._customTableView:setBounceable(true)
    self:addChild(self._customTableView)

    -- 触摸范围
    self._touchRect = CCRect(0,0,size.width,size.height)
end

-- 组件是否可触摸
function CustomTableViewTitle:enableControl(flag)
    self:setTouchEnabled(flag)
    self._customTableView:enableControl(flag)
end 
--标题栏
function CustomTableViewTitle:createTitle(titleSprite)
    -- right title
    local size = self:getContentSize()
    self._titleScorllView = CCScrollView:create()
    self._titleScorllView:setBounceable(true)
    self._titleScorllView:setDirection(kCCScrollViewDirectionHorizontal)    
self._titleScorllView:setTouchEnabled(false)  --表示标题栏不主动

    self._titleScorllView:setViewSize( CCSize(size.width,titleSprite:getContentSize().height) )                             --设置self._titleScorllView的大小,为显示的view的尺寸  
    self._titleScorllView:ignoreAnchorPointForPosition(true)
    self._titleScorllView:updateInset()              
    self:addChild(self._titleScorllView,1) 
    self._titleScorllView:setPosition(CCPoint(0,size.height) )
    --设置容器
    if ( titleSprite ) then  self._titleScorllView:setContainer( titleSprite )  end
    self._titleScorllView:registerScriptHandler( handler(self,self.__titleScorllViewDidScroll),CCScrollView.kScrollViewScroll)
    -- -- 黑边防御坐标
    -- local scrollSize1 = self._rightTitleScorllView:getContentSize();
    -- local scrollSize2 = self._rightTitleScorllView:getViewSize();
    -- -- print("scrollSize: %f %f",scrollSize1.width,scrollSize1.height);
    -- xOffSet = scrollSize1.width;
    -- yOffSet = scrollSize1.height;-- - scrollSize2.height;  
    --debug  
    -- extendUI.Common.debugDraw( self._titleScorllView ) 
end

-- 标题栏关联右表
function CustomTableViewTitle:__titleScorllViewDidScroll() 
    local offSet = self._titleScorllView:getContentOffset()
    if ( self ~= nil ) then
        local offSetOther = self._customTableView:getContentOffset()
        if (offSet.x ~= offSetOther.x) then             
            self._customTableView:setContentOffset(CCPoint(offSet.x, offSetOther.y));  
        end          
    end
end 

function CustomTableViewTitle:enableTitleScrollView( flag ) 
    self._titleScorllView:setTouchEnabled(flag)
end

--判断用户触摸是否在区域内
function CustomTableViewTitle:checkTouchRect( point )
    local worldPos = self:convertToWorldSpace( self:getPositionInCCPoint())
    point.x = point.x - worldPos.x + self:getPositionX()
    point.y = point.y - worldPos.y + self:getPositionY()   --触摸真是纠结．．．
    if (self._touchRect:containsPoint(point) ) then 
        -- print("CustomTableViewTitle:checkTouchRect( point )",self._customTableView:getContainer():boundingBox():containsPoint(point))
        return self._customTableView:getContainer():boundingBox():containsPoint(point)
    end    
    return false
end

--tabeView组件的开关
function CustomTableViewTitle:enableTableViewControl( flag )
    self._customTableView:enableControl(flag) 
end

function CustomTableViewTitle:__touch(event, x, y) 
    local startPos = 0
    local endPos = 0 
    if event == "began" then  
        self._startX = x
        self._startY = y
        self.lastdirection_ = 0
        self.direction_ = 0 
        return self:checkTouchRect( CCPoint(x, y) )
    elseif event == "moved" then  
        startPos = CCPoint(self._startX,self._startY)
        endPos  = CCPoint(x,y)    
        self.direction_,offsetX= extendUI.Common.getDirection(startPos,endPos)   
        if (self.lastdirection_ ~= self.direction_) then
            if (self.lastdirection_ ~= 0) then  --用户转向了
                if (math.abs(self.direction_) ~= math.abs(self.lastdirection_)) then --用户转向只能是上，下或者左,右;其他不行
                     self._startX = x
                     self._startY = y        
                     self.lastdirection_ = 0  
                     return true    
                end
            else
                self.lastdirection_ = self.direction_
            end
        end

        if (self.direction_ == extendUI.Common.LEFT or self.direction_ == extendUI.Common.RIGHT) then
          self:enableTableViewControl(false)
          -- print("math.abs(offsetX)",math.abs(offsetX))
        if ( math.abs(offsetX) > 0.04) then 
            self:moveOffsetX(offsetX)  
            self._startX = x
            self._startY = y
            startPos = CCPoint(self._startX,self._startY)
        end 
        else  
            self:enableTableViewControl(true)
        end
        return true  
    elseif event == "ended"then
        startPos = CCPoint(self._startX,self._startY)
        endPos  = CCPoint(x,y)    
        self.direction_,offsetX = extendUI.Common.getDirection(startPos,endPos)
        if (self.direction_ == extendUI.Common.LEFT or self.direction_ == extendUI.Common.RIGHT) then
            self:moveOffsetX(offsetX,true)  
        end 
        self:enableTableViewControl(true)
        return true
    else -- cancelled   
        self:enableTableViewControl(true)
        return true
    end
end 

function CustomTableViewTitle:initEvent()
    self._titleScorllView:registerScriptHandler( handler(self,self.__titleScorllViewDidScroll),CCScrollView.kScrollViewScroll)
    self:setTouchEnabled(true)
    self:registerScriptTouchHandler(function(event, x, y)
        return self:__touch(event, x, y)
    end) 
end

function CustomTableViewTitle:onExit()
    self:stopPerformedAnimatedScroll()
    -- print("尼瑪的")
end

function CustomTableViewTitle:debugGrapic() 
    --debug
    local rectU = extendUI.Common.drawRect( CCRect(0,0,self:getContentSize().width,self:getContentSize().height) )
    self:addChild( rectU )      
end

-- about 标题栏item数目，用于宽度计算
-- function CustomTableViewTitle:setTitleItemCount( itemcount ) 
--     self._titleItemCount = itemcount
-- end
-- function CustomTableViewTitle:getTitleItemCount()
--     return self._titleItemCount
-- end

-- function CustomTableViewTitle:setTitleItemCount( itemcount ) 
--     self._titleItemCount = itemcount
-- end
function CustomTableViewTitle:getTitleItemCount()
if (self._titleSprite) then    
        if (self._titleSprite.getTitleItemsCount) then
            return self._titleSprite:getTitleItemsCount() 
        else
            return 0
        end 
    end    
        return 0
end



-- about 多少子item
function CustomTableViewTitle:setItemCount( count )
    self._itemCount = count
end

function CustomTableViewTitle:getItemCount()
    return self._itemCount
end

-- CustomTableView 原来方法
-- 偏移Ｘ坐标()
function CustomTableViewTitle:moveOffsetX( offsetPosX,animationed )
    self._isScrolling = true
    self._customTableView:moveOffsetX(  offsetPosX,animationed  )
end
function CustomTableViewTitle:getContentOffset()    
    return self._customTableView:getContentOffset()
end 

function CustomTableViewTitle:setContentOffset(offSet)    
    self._customTableView:setContentOffset( offSet )
end 

function CustomTableViewTitle:reloadData() 
    self._customTableView:reloadData() 
end
function CustomTableViewTitle:cellAtIndex( index ) 
    return self._customTableView:cellAtIndex(index) 
end

return CustomTableViewTitle
--endregion
