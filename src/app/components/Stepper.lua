--步进组件
-- Author: anjun
-- Date: 2014-06-20 17:11:14
--暂时没考虑上下，只是左右
local Stepper = class("Stepper", function() 
    return display.newNode()
end)

function Stepper:ctor(minusSprite,plusSprite,textFieldSize,buttonGape)  
	self._dMinimumValue = 0
	self._dMaximumValue = 10
	self._dStepValue = 1
	self._dValue = 0

    makeUIControl_(self) 
    self:setNodeEventEnabled(true)
    self:initView(minusSprite,plusSprite,textFieldSize,buttonGape)
    self:initEvent()
    self:fixPos()
    self:setValue(0)
end

function Stepper:initView(minusSprite,plusSprite,textFieldSize,buttonGape)
	self._minusSprite = minusSprite
						:addTo(self)

	self._plusSprite = plusSprite
					   :addTo(self)
	self._buttonGape = buttonGape or 0
	
	textFieldSize = textFieldSize or CCSize(100,self._plusSprite:getContentSize().height )			   
	self._textFieldInput = extendUI.TextField.new({
      image = LOGIN__LOGIN_NOTICE_PNG,
      size = textFieldSize, 
      listener = function(event, editbox)
          if event == "began" then 
          elseif event == "ended" then  
          elseif event == "return" then 
          elseif event == "changed" then 
          else 
          end
      end
  	}) 
  self._textFieldInput:setFontColor(display.COLOR_WHITE); 
  self._textFieldInput:setReturnType(kKeyboardReturnTypeDone)  
  self:addChild(self._textFieldInput)
end 

function Stepper:initEvent()
	self._minusSprite:onButtonClicked(function(event)
        if (self._dValue < self._dMaximumValue) then
            self._dValue = self._dValue+self._dStepValue
        end
        self:setValue(self._dValue) 
	end)

	self._plusSprite:onButtonClicked(function(event)
        if (self._dValue > self._dMinimumValue) then
            self._dValue = self._dValue-self._dStepValue
        end
        self:setValue(self._dValue)
	end)
end

function Stepper:enable( flag )
	self._minusSprite:setButtonEnabled(flag)
	self._plusSprite:setButtonEnabled(flag)
	self._textFieldInput:setEnabled(flag)
	return self
end

function Stepper:fixPos() 
	local x,y = 0,0

	self._textFieldInput:align(display.BOTTOM_LEFT, x, y )

	x = x + self._textFieldInput:getContentSize().width
	self._minusSprite:align(display.BOTTOM_LEFT, x, y)

	x = x + self._minusSprite:getContentSize().width + self._buttonGape
	self._plusSprite:align(display.BOTTOM_LEFT, x, y)
end

function Stepper:getContentSize()  
	local w = self._textFieldInput:getContentSize().width + self._minusSprite:getContentSize().width +
			  self._buttonGape + self._plusSprite:getContentSize().width
local h = self._textFieldInput:getContentSize().height --取三者最高
	local contentSize = CCSize(w,h)
	return contentSize 
end

function Stepper:setMinimumValue(minimumValue)
	self._dMinimumValue = minimumValue
	return self
end

function Stepper:setMaximumValue(maximumValue)
	self._dMaximumValue = maximumValue
	return self
end

function Stepper:setValue(value)
	value = toint(value)
	self._dValue = value
	if value > self._dMaximumValue then
		self._dValue = self._dMaximumValue
	elseif value < self._dMinimumValue then
		self._dValue = self._dMinimumValue
	end
	self._textFieldInput:setText(self._dValue)	
	return self
end

function Stepper:getValue()
	self._dValue = toint(self._textFieldInput:getText())
	return self._dValue
end

function Stepper:setStepValue(stepValue)
	self._dStepValue = stepValue
	return self
end

return Stepper
