----定制數據窗口
-- (包含CustomDataGrid组件 和 通用查詢面板...)　
-- Author: anjun
-- Date: 2014-06-26 17:19:40
--
local FastDateSearchPnl = require("app.scenes.panles.FastDateSearchPnl")
local SearchPnl = require("app.scenes.panles.SearchPnl")
local DateSearchPnl = require("app.scenes.panles.DateSearchPnl")

-- local NormalWindow = import("app.components.NormalWindow")
local CommonWindow = import("app.components.CommonWindow")
local CustomDataWindow = class("CustomDataWindow", CommonWindow)
CustomDataWindow.GRIDHANDLEBACK = "GridHandleBack"

function CustomDataWindow:ctor(param)
    param = param or {}
	param.touchInSprite=true
    param.isTransition = true
    CustomDataWindow.super.ctor(self,param)  
    self._customGridData = {}   --網格數據源 
    self._info = {}             --窗體相關信息
    self._info.page = 1         --當前页
    self._info.pageSize = 30    --页大小
    self._info.totalPage = 1    --總頁數
    self._info.sumRecords = 1   --總條數
end

function CustomDataWindow:initView()  
	CustomDataWindow.super.initView(self)
    self._fastDatesearchPnl = FastDateSearchPnl.new(CCSize(self:getWinContentSize().width,48))
    self:addContentChild(self._fastDatesearchPnl)
    self._fastDatesearchPnl:align(display.BOTTOM_LEFT, 0, 0)
    self._searchButton = extendUI.IconButton.new( COMMON__TCL_UPAY_KEYBOARD_BTN_SELECTED_9PNG,{scale9 = true})
        :setButtonSize(80, 48) 
        :setButtonLabel(ui.newTTFLabel({
            text = "過濾",
            size = 28,
            color = display.COLOR_WHITE
        }))
        :onButtonPressed(function(event)

        end)
        :onButtonRelease(function(event)
            
        end)
        :onButtonClicked(function(event)
        	self:showSearchPanle()
        end)
        :setButtonIconDirection(extendUI.Common.CENTER)             
    self:addTopBarChild( self._searchButton )  
    self:fixPos(); 
end

function CustomDataWindow:createScrollPanel()

end

-- 創建高級網格組件
function CustomDataWindow:createAdvacedGrid(customDataGridviewSize,gridConfig,type)
    local winContentSize = self:getWinContentSize()
    local fastDatesearchPnlSize = (self._fastDatesearchPnl and self._fastDatesearchPnl:getContentSize()) or CCSize(0,0)
    customDataGridviewSize = customDataGridviewSize or CCSize(winContentSize.width,winContentSize.height-fastDatesearchPnlSize.height)
    local x,y=0,0

if self._firstBtn then --tmp
    if self._autoRefreshCK then
        customDataGridviewSize.height = customDataGridviewSize.height-92
        y=92
    else
        customDataGridviewSize.height = customDataGridviewSize.height-65
        y=65
    end 
end
    self._customDataGrid = extendUI.AdvancedGrid.new( {size=customDataGridviewSize} )
    self:addContentChild(self._customDataGrid)
    self._customDataGrid:initialization(gridConfig)
    self._customDataGrid:pos(x, y)
end

-- 創建數據網格組件
function CustomDataWindow:createDataGrid(lTitleItem,rTitleItem,customDataGridviewSize,type)
    local winContentSize = self:getWinContentSize()
    local fastDatesearchPnlSize = self._fastDatesearchPnl:getContentSize()
    customDataGridviewSize = customDataGridviewSize or CCSize(winContentSize.width,winContentSize.height-fastDatesearchPnlSize.height)
type = type or 2 
if ( type == 2) then 
    self._customDataGrid = extendUI.CustomDataGrid.new(customDataGridviewSize,handler(self, self.__gridHandle),lTitleItem,rTitleItem )
elseif(type == 1) then  --
    self._customDataGrid = extendUI.CustomTableViewTitle.new(handler(self, self.__gridHandle2), customDataGridviewSize, rTitleItem )
    self._customDataGrid:pos(0, -rTitleItem:getContentSize().height)  --位置因為CustomTableViewTitle的標題欄關係...
end 
    self:addContentChild( self._customDataGrid) 
    -- extendUI.Common.debugDraw( self._customDataGrid )
end

-- 數據網格回調2
function CustomDataWindow:__gridHandle2(fn, table, a1, a2,a3)
    local function _handleCell()
        local item = nil
        if not a2 then
            a2 = CCTableViewCell:new()  
            item = self.RItem.new()
            a2:addChild(item,0,123) 
            item:setAnchorPoint(CCPoint(0,0)) 
        end

        item = a2:getChildByTag(123)
        if nil ~= item then 
            if (self._customGridData[a1+1]) then 
                item:update(self._customGridData[a1+1])
            end
        else
            a2 = CCTableViewCell:new()  
            item = self.RItem.new()
            a2:addChild(item,0,123) 
            item:setAnchorPoint(CCPoint(0,0))
            if (self._customGridData[a1+1]) then 
                item:update(self._customGridData[a1+1])
            end
        end
    end

    local r
    if fn == "cellSize" then
        local h = FinanceUtils.RTitleItemSize.height   
        local rTitleItemCount = a3 or 1             -- a3 is titleItemCount 
        rTitleItemCount = rTitleItemCount + 0.5     --半个身位
        local w = FinanceUtils.RTitleItemSize.width*rTitleItemCount 
        w = (FinanceUtils.MinItemWidth>w) and FinanceUtils.MinItemWidth or w 
        r = CCSizeMake(w,h)   
    elseif fn=="scroll" then 
    elseif fn == "cellAtIndex" then 
        _handleCell()    
        r = a2
    elseif fn == "numberOfCells" then 
        r = ( #self._customGridData)
    -- 表格事件：
    elseif fn == "cellTouched" then         -- A cell was touched, a1 is cell that be touched. This is not necessary.
    elseif fn == "cellTouchBegan" then      -- A cell is touching, a1 is cell, a2 is CCTouch
    elseif fn == "cellTouchEnded" then      -- A cell was touched, a1 is cell, a2 is CCTouch
    elseif fn == "cellHighlight" then       -- A cell is highlighting, coco2d-x 2.1.3 or above
    elseif fn == "cellUnhighlight" then     -- A cell had been unhighlighted, coco2d-x 2.1.3 or above
    elseif fn == "cellWillRecycle" then     -- A cell will be recycled, coco2d-x 2.1.3 or above
    end
    self:dispatchEvent({name=CustomDataWindow.GRIDHANDLEBACK,type=fn,data={table, a1, a2,a3}})
    return r
end

-- 數據網格回調1
function CustomDataWindow:__gridHandle(type,fn, table, a1, a2,a3)
	-- overridclass 實現 或者cutomer 
    local function _handleCell()
        local item = nil
        if not a2 then
            a2 = CCTableViewCell:new()  
            if (type == "L") then   --左邊
                item = self.LItem.new() 
            elseif (type == "R" ) then  --右邊
                item = self.RItem.new() 
            end 
            a2:addChild(item,0,123) 
            item:setAnchorPoint(CCPoint(0,0)) 
        end

        item = a2:getChildByTag(123)
        if nil ~= item then 
            if (self._customGridData[a1+1]) then 
                item:update(self._customGridData[a1+1])
            end
        else
            a2 = CCTableViewCell:new()  
            if (type == "L") then   --左邊
                item = self.LItem.new()
            elseif (type == "R" ) then  --右邊
                item = self.RItem.new() 
            end 
            a2:addChild(item,0,123) 
            item:setAnchorPoint(CCPoint(0,0))
            if (self._customGridData[a1+1]) then 
                item:update(self._customGridData[a1+1])
            end
        end
    end

    local r
    if fn == "cellSize" then
        if (type == "L") then
            r = FinanceUtils.LTitleItemSize             --only height is used(初步分析是上下排列，所以...)
        elseif (type == "R" ) then                      --右边的宽度计算比较复杂
            local h = FinanceUtils.RTitleItemSize.height   
            local rTitleItemCount = a3 or 1             -- a3 is titleItemCount 
            rTitleItemCount = rTitleItemCount + 0.5     --半个身位
            local w = FinanceUtils.RTitleItemSize.width*rTitleItemCount
            r = CCSizeMake(w,h)
        end        
    elseif fn=="scroll" then 
    elseif fn == "cellAtIndex" then 
        _handleCell()    
        r = a2
    elseif fn == "numberOfCells" then 
        r = ( #self._customGridData)    -- 
    -- 表格事件：
    elseif fn == "cellTouched" then         -- A cell was touched, a1 is cell that be touched. This is not necessary.
    elseif fn == "cellTouchBegan" then      -- A cell is touching, a1 is cell, a2 is CCTouch
    elseif fn == "cellTouchEnded" then      -- A cell was touched, a1 is cell, a2 is CCTouch
    elseif fn == "cellHighlight" then       -- A cell is highlighting, coco2d-x 2.1.3 or above
    elseif fn == "cellUnhighlight" then     -- A cell had been unhighlighted, coco2d-x 2.1.3 or above
    elseif fn == "cellWillRecycle" then     -- A cell will be recycled, coco2d-x 2.1.3 or above
    end
    return r    
end

function CustomDataWindow:initEvent()
	CustomDataWindow.super.initEvent(self) 
    self._fastDatesearchPnl:setCallBack( handler(self, self.__fastDatesearchCallBack) ) 
    self:refresh()
    --app:addEventListener(CLIENT_EVENT_UPDATE_DATAGRID, handler(self, self.__refreshDataGrid))
end  

function CustomDataWindow:removeEvent()
   -- app:removeAllEventListenersForEvent(CLIENT_EVENT_UPDATE_DATAGRID)  
end
 
function CustomDataWindow:__refreshDataGrid( event ) 
    if (self._customDataGrid) then
       self._customDataGrid:refreshCells()
    end
end 

function CustomDataWindow:refresh()
    self._info.page = 1 
    self:getDBData()
end

-- 快速查詢返回
function CustomDataWindow:__fastDatesearchCallBack(info )
	-- overridclass 實現 
    self:hideSearchPanle()
end

function CustomDataWindow:fixPos()
	CustomDataWindow.super.fixPos(self)
	if ( self._fastDatesearchPnl) then	
		self._fastDatesearchPnl:align(display.BOTTOM_LEFT, 0, self:getWinContentSize().height-self._fastDatesearchPnl:getContentSize().height ) 
	end
	if ( self._searchButton ) then
		self._searchButton:align(display.RIGHT_CENTER,self:getContentSize().width -15,(self:getTitleBarSize().height/2))
	end 

    -- fixpos
    local space = 15
    local x = 0
    local y = 0
    if self._lblPageTotalPage then
        self._lblPageTotalPage:align(display.BOTTOM_LEFT,x,y)
        x = x + self._lblPageTotalPage:getContentSize().width + space
        self._firstBtn:align(display.BOTTOM_LEFT, x, y)
        x = x + 65 + space
        self._lastBtn:pos(x, y)
        self._lastBtn:align(display.BOTTOM_LEFT, x, y)
        x = x + 65 + space
        self._nextBtn:pos(x, y)
        self._nextBtn:align(display.BOTTOM_LEFT, x, y)
        x = x + 65 + space 
        self._endBtn:align(display.BOTTOM_LEFT, x, y) 
        x = x + 65 + space 
        self._rfreshBtn:align(display.BOTTOM_LEFT, x, y)        
    end 
    if self._autoRefreshCK then
        x = self._lblPageTotalPage and self._lblPageTotalPage:getPositionX() or 0
        y = self._lblPageTotalPage and self._lblPageTotalPage:getPositionY()+ 48 or 0
        self._autoRefreshCK:align(display.BOTTOM_LEFT, x, y)         
    end 

end
 
-- 顯示翻頁功能按紐組件
function CustomDataWindow:showFunctionBtnsGroup()
    self._firstBtn = cc.ui.UIPushButton.new(COMMON__TCL_UPAY_KEYBOARD_BTN_SELECTED_9PNG, {scale9 = true})
        :setButtonSize(65, 48)
        :onButtonPressed(function(event) 
          event.target:setOpacity(128)
        end)
        :onButtonRelease(function(event)
          event.target:setOpacity(255)
        end)
        :onButtonClicked(function(event) 
            self:turnPage(1)
        end)
        :setButtonLabel("normal", ui.newTTFLabel({
            text = "首頁",
            size = 18,
            color = display.COLOR_BLACK
        })) 
        :setButtonLabel("disabled", ui.newTTFLabel({
            text = "首頁",
            size = 18,
            color = display.COLOR_BLACK 
        })) 
    self:addContentChild(self._firstBtn ) 

    self._lastBtn = cc.ui.UIPushButton.new(COMMON__TCL_UPAY_KEYBOARD_BTN_SELECTED_9PNG, {scale9 = true})
        :setButtonSize(65, 48)
        :onButtonPressed(function(event) 
          event.target:setOpacity(128)
        end)
        :onButtonRelease(function(event)
          event.target:setOpacity(255)
        end)
        :onButtonClicked(function(event)
            self:turnPage(2)
        end)
        :setButtonLabel(ui.newTTFLabel({
            text = "上一頁",
            size = 15, 
            color = display.COLOR_BLACK
        })) 
    self:addContentChild( self._lastBtn ) 

    self._nextBtn = cc.ui.UIPushButton.new(COMMON__TCL_UPAY_KEYBOARD_BTN_SELECTED_9PNG, {scale9 = true})
        :setButtonSize(65, 48)
        :onButtonPressed(function(event) 
          event.target:setOpacity(128)
        end)
        :onButtonRelease(function(event)
          event.target:setOpacity(255)
        end)
        :onButtonClicked(function(event)
            self:turnPage(3)
        end)
        :setButtonLabel(ui.newTTFLabel({
            text = "下一頁",
            size = 15, 
            color = display.COLOR_BLACK
        })) 
    self:addContentChild( self._nextBtn ) 

    self._endBtn = cc.ui.UIPushButton.new(COMMON__TCL_UPAY_KEYBOARD_BTN_SELECTED_9PNG, {scale9 = true})
        :setButtonSize(65, 48)
        :onButtonPressed(function(event) 
          event.target:setOpacity(128)
        end)
        :onButtonRelease(function(event)
          event.target:setOpacity(255)
        end)
        :onButtonClicked(function(event)
            self:turnPage(4)
        end)
        :setButtonLabel(ui.newTTFLabel({
            text = "尾頁",
            size = 15, 
            color = display.COLOR_BLACK
        })) 
    self:addContentChild( self._endBtn )   

    self._rfreshBtn = cc.ui.UIPushButton.new(COMMON__TCL_UPAY_KEYBOARD_BTN_SELECTED_9PNG, {scale9 = true})
        :setButtonSize(65, 48)
        :onButtonPressed(function(event) 
          event.target:setOpacity(128)
        end)
        :onButtonRelease(function(event)
          event.target:setOpacity(255)
        end)
        :onButtonClicked(function(event) 
            self:refresh()
        end)
        :setButtonLabel(ui.newTTFLabel({
            text = "刷新",
            size = 15, 
            color = display.COLOR_BLACK
        })) 
    self:addContentChild( self._rfreshBtn )    

    self._lblPageTotalPage = ui.newTTFLabel( {text = "",size = 25, color = display.COLOR_BLACK} )
    self:addContentChild( self._lblPageTotalPage )
    self:showPageTotalPageLbl() 
    
end


function CustomDataWindow:showAutoRefreshBtn() --顯示自動刷新按鈕
    self._autoRefreshCK = cc.ui.UICheckBoxButton.new(COMMON_CHECKBOX_IMAGES)
                    :setButtonLabel(cc.ui.UILabel.new({text = "自動刷新 最後更新時間", size = 25,color = display.COLOR_BLACK}))
                    :setButtonLabelOffset(15, 0)
                    :setButtonLabelAlignment(display.LEFT_CENTER)   
    self:addContentChild( self._autoRefreshCK )   
    self._autoRefreshCK:onButtonClicked(function(event)
        print(self._autoRefreshCK:isButtonSelected())
    end)    
end

-- 翻頁
function CustomDataWindow:turnPage(type)
    local currPage = self._info.page
    if type == 1 then
        currPage = 1
    elseif type == 2 then 
        currPage = self._info.page -1
    elseif type == 3 then
         currPage = self._info.page +1
    elseif type == 4 then 
        currPage = self._info.totalPage
    end 

    if ( currPage < 1 ) then
        currPage = 1
    elseif currPage > self._info.totalPage then
        currPage = self._info.totalPage
    end 

    if (currPage == self._info.page ) then
        return
    end

    self._info.page = currPage
    self:getDBData() 
end
-- 請求數據
function CustomDataWindow:getDBData()    
end

function CustomDataWindow:showPageTotalPageLbl() 
    self._info.totalPage =(self._info.sumRecords%self._info.pageSize)==0  and self._info.sumRecords/self._info.pageSize or math.floor(self._info.sumRecords/self._info.pageSize)+1;    
    print("CustomDataWindow:showPageTotalPageLbl",self._info.totalPage,self._info.sumRecords,self._info.pageSize)
    self._info.totalPage = toint( self._info.totalPage ) 
    local str = "當前第["
    str = str..tostring(self._info.page).."/"..tostring(self._info.totalPage).."]頁" 
    self._lblPageTotalPage:setString(str)

    self:fixPos()
end

-- handle
function CustomDataWindow:showSearchPanle()
    self:enableScrollTableView(false)
    if (self._searchPnl and self._fastDatesearchPnl) then
        self._searchPnl:update( self._fastDatesearchPnl:getDateTimer() )
    end
    if (self._searchPnl) then
        self._searchPnl:setVisible(true)
    end    
end

function CustomDataWindow:hideSearchPanle()
    self:enableScrollTableView(true) 
    if (self._searchPnl) then
        self._searchPnl:setVisible(false)
    end
   
end

function CustomDataWindow:updateGridData( data )
    self._customGridData = data
    self._customDataGrid:reloadData()
end

--  override
function CustomDataWindow:__alertClose(event)
	CustomDataWindow.super.__alertClose(self,event) 
end

--  override 
function CustomDataWindow:__windowTouch(event,x,y) 
    local point = ccp(x, y)
    local rect = nil 
    if event == "began" then 
        return true
    elseif event == "moved" then  
        return true
    elseif event == "ended" then  
        point = ccp(x, y) 
        --判斷查找面板
        if (self._searchPnl and self._searchPnl:isVisible() ) then
            rect = self._searchPnl:getCascadeBoundingBox()  
            if ( not (rect:containsPoint(point)) ) then
                self:hideSearchPanle()
            end
        end    
        return true
    else -- cancelled 
         return true
    end
end

return CustomDataWindow;
