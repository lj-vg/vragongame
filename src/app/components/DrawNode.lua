-- --重写官方CCDrawNode
-- 扩展 画K线用
-- Author: anjun
-- Date: 2014-02-27 14:31:17
--  
local DrawNode = class("DrawNode",function()
    return CCDrawNode:create()
end)

function DrawNode:ctor()
	-- body 
end

function DrawNode:drawPolygon(verts, count, fillColor,borderWidth,borderColor)  
    getmetatable(self).drawPolygon(self,verts, count, fillColor,borderWidth,borderColor) 
end  

function DrawNode:newRect(width, height)
    local x, y = 0, 0
    if type(width) == "userdata" then
        local t = tolua.type(width)
        if t == "CCRect" then
            x = width.origin.x
            y = width.origin.y
            height = width.size.height
            width = width.size.width
        elseif t == "CCSize" then
            height = width.height
            width = width.width
        else
            echoError("display.newRect() - invalid parameters")
            return
        end
    end
    
	local points = CCPointArray:create(4) 
	points:add(CCPoint(-30, 30))
	points:add(CCPoint(30, 30))
	points:add(CCPoint(30, -30))
	points:add(CCPoint(-30, -30))  
	front:drawPolygon( points:fetchPoints(), 4, yellow, 1, red); 
	front:setPosition(ccp(200,300)); 

    local rect = CCNodeExtend.extend(CCRectShape:create(CCSize(width, height)))
    rect:setPosition(x, y)
    return rect
end 

return DrawNode

