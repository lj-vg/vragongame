--重写官方CCScrollView
-- 方便扩展 
-- Author: anjun
-- Date: 2014-06-16 17:34:40
--
local ScrollView = class("ScrollView",function( size )
	size = size or CCSize(0,0)
	local scrollView = CCScrollView:create() 
    scrollView:setViewSize(size)             --设置scrollView的大小,为显示的view的尺寸  
    -- scrollView:ignoreAnchorPointForPosition(false) false的时候？
    return scrollView
end)
ScrollView.STARTSCROLLING   = "startScrolling"
ScrollView.STOPSCROLLING  = "stopScrolling" 
ScrollView.ISCANTOUCH  = "isCanTouch" 

function ScrollView:ctor(size,scrollFun)
    -- 触摸范围
    self._touchRect = CCRect(0,0,size.width,size.height)	
    self._isScrolling= false
    self._isScrllCount = 0
    self._isLastScrllCount = -1
    self._scrollFun = scrollFun
    makeUIControl_(self)
	self:initEvent()
end

function ScrollView:__scrollViewDidScroll()
	if ( not self:isTouchMoved()  ) then
	    if ( self._isScrllCount == 0 ) then
	        self:startPerformedAnimatedScroll()
	    end 
	    self._isScrllCount = self._isScrllCount + 1
	end       
	self._isScrolling = true
	if ( self._isLastScrllCount == -1) then
		self:dispatchEvent({name=ScrollView.STARTSCROLLING} )
		self._isLastScrllCount = 0
	end
	if (self._scrollFun) then
		self._scrollFun()
	end
end

function ScrollView:initEvent()
    self:registerScriptHandler(handler(self,self.__scrollViewDidScroll),CCScrollView.kScrollViewScroll)
     
end

function ScrollView:setContainer( container )
	getmetatable(self).setContainer(self, container)
    self:getContainer():setTouchEnabled(true)  --控制是否在可視範圍裡面
    self:getContainer():registerScriptTouchHandler(function(event, x, y)
        return self:__touch(event, x, y)
    end,false,128,false)  
end

function ScrollView:__touch(event, x, y)   
    if event == "began" then   
        self._isCanTouch = self:checkTouchRect( CCPoint(x, y) ) 
        self:dispatchEvent({name=ScrollView.ISCANTOUCH} )
        return false
    elseif event == "moved" then  
    elseif event == "ended"then  
    else -- cancelled
    end
end 
--判断用户触摸是否在区域内
function ScrollView:checkTouchRect( point ) 
    local worldPos = self:convertToWorldSpace( self:getPositionInCCPoint())
    point.x = point.x - worldPos.x + self:getPositionX()
    point.y = point.y - worldPos.y + self:getPositionY()   --触摸真是纠结．．．
    if (self._touchRect:containsPoint(point) ) then
        return true
    end    
    return false
end 

function ScrollView:startPerformedAnimatedScroll()
    local function callback() 
        if (self._isLastScrllCount ~= self._isScrllCount) then
            self._isLastScrllCount = self._isScrllCount
        else
			if (self._isLastScrllCount ~= 0) then
				self:stopedScroll()
			end;            
        end
    end
    self._performedTimeHandle = extendUI.scheduler.scheduleGlobal(callback,1/60)
end

function ScrollView:stopedScroll()
    self:stopPerformedAnimatedScroll()
    self._isScrolling = false
    self._isScrllCount = 0
    self._isLastScrllCount = -1
	self:dispatchEvent({name=ScrollView.STOPSCROLLING} )
end

function ScrollView:stopPerformedAnimatedScroll()
    if ( self._performedTimeHandle ) then
        extendUI.scheduler.unscheduleGlobal( self._performedTimeHandle )
    end
    self._performedTimeHandle = nil    
end
--[[]
	是否正在滚动;
		以上就是判断这个,新版本会有更好的办法么?
		否则边滚动;里面的button也会响应事件
--]]
function ScrollView:isScrolling() 
	return self._isScrolling
end

--[[]
	是否可響應容器裡的事件;
    因為超過可顯示範圍也響應事件．funck
--]]
function ScrollView:isCanTouch()
	return self._isCanTouch
end

function ScrollView:onExit()
	self:stopPerformedAnimatedScroll()
end

 
return ScrollView