--android 的定制
-- Author: anjun
-- Date: 2014-04-09 12:36:51
-- 
local AndroidCutom = {}
-- 按反回鍵的處理
function AndroidCutom.comeBackAction(parent,callback )
    if device.platform ~= "android" then return end 
    -- avoid unmeant back 避免偶然的返回事件，
    -- 所以将函数的执行推后0.5S
    parent:performWithDelay(function()
        -- keypad layer, for android
        local layer = display.newLayer()
        layer:addKeypadEventListener(function(event) --添加键盘事件监听
            if event == "back" then
            	if ( callback ) then
            		callback()
            	end
            end    --back键的消息
        end)
        parent:addChild(layer)  --场景上添加Layer。
        layer:setKeypadEnabled(true)  --设置键盘监听事件可用
    end, 0.5)
end

return AndroidCutom