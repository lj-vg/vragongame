--干....
-- Author: anjun
-- Date: 2014-08-14 11:31:38
--
local UIBoxLayout = cc.ui.UIBoxLayout  
local ExtendUIBoxLayout = class("ExtendUIBoxLayout", UIBoxLayout)  

function ExtendUIBoxLayout:ctor(direction, name) 
	ExtendUIBoxLayout.super.ctor(self,direction, name)
end

function ExtendUIBoxLayout:addWidget(widget, weight,order)
    self.order_ = order or (self.order_ + 1)
    self.widgets_[widget] = {weight = weight or 1, order = self.order_}
    return self
end

return ExtendUIBoxLayout

