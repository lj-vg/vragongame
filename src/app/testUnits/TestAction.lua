--常用Action效果
-- Author: anjun
-- Date: 2014-02-28 11:16:23
-- 
local function runTestAction()
    local newScene = CCScene:create()
	local layer = CCLayer:create()   
    local sprite = CCSprite:create("button_confirm_on.png")
    sprite:setPosition(CCPointMake(100, display.height / 2))
    -- left:runAction(CCRepeatForever:create(to1))
	layer:addChild(sprite)  
	-- moveto	
			-- 等待 1.0 后开始移动对象
			-- 耗时 1.5 秒，将对象移动到屏幕中央
			-- 移动使用 backout 缓动效果
			-- 移动结束后执行函数，显示 move completed
			-- transition.execute(sprite, CCMoveTo:create(1.5, CCPoint(display.cx, display.cy)), {
			--     delay = 1.0,
			--     easing = "backout",
			--     onComplete = function()
			--         print("move completed")
			--     end,
			-- })

					-- PS transition.execute() 的参数表格支持下列参数
						-- delay: 等待多长时间后开始执行动作
						-- easing: 缓动效果的名字及可选的附加参数，效果名字不区分大小写
						-- onComplete: 动作执行完成后要调用的函数
						-- time: 执行动作需要的时间
					-- http://wiki.quick-x.com/upload/easing_demo.html 效果演示
						-- transition.execute() 支持的缓动效果： 
						-- backIn
						-- backInOut
						-- backOut
						-- bounce
						-- bounceIn
						-- bounceInOut
						-- bounceOut
						-- elastic, 附加参数默认为 0.3
						-- elasticIn, 附加参数默认为 0.3
						-- elasticInOut, 附加参数默认为 0.3
						-- elasticOut, 附加参数默认为 0.3
						-- exponentialIn, 附加参数默认为 1.0
						-- exponentialInOut, 附加参数默认为 1.0
						-- exponentialOut, 附加参数默认为 1.0
						-- In, 附加参数默认为 1.0
						-- InOut, 附加参数默认为 1.0
						-- Out, 附加参数默认为 1.0
						-- rateaction, 附加参数默认为 1.0
						-- sineIn
						-- sineInOut
						-- sineOut			


	--rotateTo 
			-- 将显示对象旋转到指定角度，并返回 CCAction 动作对象。
			-- 耗时 0.5 秒将 sprite 旋转到 180 度
			-- transition.rotateTo(sprite, {rotate = 180, time = 0.5})

	-- transition.moveTo 
			-- 将显示对象移动到指定位置，并返回 CCAction 动作对象。	
			-- 移动到屏幕中心
			-- transition.moveTo(sprite, {x = display.cx, y = display.cy, time = 1.5})
			-- 移动到屏幕左边，不改变 y
			-- transition.moveTo(sprite, {x = display.left, time = 1.5})
			-- 移动到屏幕底部，不改变 x
			-- transition.moveTo(sprite, {y = display.bottom, time = 1.5})		
			
	--transition.moveBy
			-- 将显示对象移动一定距离，并返回 CCAction 动作对象。
			-- 向右移动 100 点，向上移动 100 点
			-- transition.moveBy(sprite, {x = 100, y = 100, time = 1.5})
			-- 向左移动 100 点，不改变 y
			-- transition.moveBy(sprite, {x = -100, time = 1.5})
			-- 向下移动 100 点，不改变 x
			-- transition.moveBy(sprite, {y = -100, time = 1.5}) 

	-- transition.fadeIn
		-- 淡入显示对象，并返回 CCAction 动作对象。		
		-- action = transition.fadeIn(sprite, {time = 1.5})
		-- action = transition.fadeTo(sprite, {time = 1.5})		
			-- PS:fadeIn 操作会首先将对象的透明度设置为 0（0%，完全透明），
			-- 	然后再逐步增加为 255（100%，完全不透明）。
			-- 	如果不希望改变对象当前的透明度，应该用 fadeTo()。	

	-- transition.fadeOut 
		-- 淡出显示对象，并返回 CCAction 动作对象。  
		-- action = transition.fadeOut(sprite, {time = 1.5})		
		   -- PS fadeOut 操作会首先将对象的透明度设置为 255（100%，完全不透明），
		   -- 然后再逐步减少为 0（0%，完全透明）。
		   -- 如果不希望改变对象当前的透明度，应该用 fadeTo()。

	-- transition.fadeTo 
		-- 将显示对象的透明度改变为指定值，并返回 CCAction 动作对象。
		-- 不管显示对象当前的透明度是多少，最终设置为 128
		-- transition.fadeTo(sprite, {opacity = 100, time = 1.5}) 

	-- transition.scaleTo 
		-- 将显示对象缩放到指定比例，并返回 CCAction 动作对象。 
		-- 整体缩放为 50%
		-- transition.scaleTo(sprite, {scale = 0.5, time = 1.5})
		-- 单独水平缩放
		-- transition.scaleTo(sprite, {scaleX = 0.5, time = 1.5})
		-- -- 单独垂直缩放
		-- transition.scaleTo(sprite, {scaleY = 0.5, time = 1.5})	

	-- transition.sequence 
	-- 创建一个动作序列对象。 
			-- 格式：  
			-- action = transition.sequence({动作对象, 动作对象, [动作对象 ...]})
			-- 用法示例： 
			-- local sequence = transition.sequence({
			--     CCMoveTo:create(0.5, CCPoint(display.cx, display.cy)),
			--     -- CCFadeOut:create(0.2),
			--     -- CCDelayTime:create(0.5),
			--     -- CCFadeIn:create(0.3):setTag(13), 
			-- })
			-- sprite:runAction(sequence)			

		local jump = CCJumpBy:create(2, ccp(300,0), 50, 4)
		local function onComplete( ... )
			-- body
			print(···)
		end  
	    local action = CCSequence:createWithTwoActions(jump, jump:reverse())
	    action = CCSequence:createWithTwoActions(action, CCCallFuncN:create(onComplete)) 
		sprite:runAction(action)	

	-- transition.removeAction
	-- 停止一个正在执行的动作。

			-- 格式： 
			-- transition.removeAction(CCAction 动作对象)
			-- 用法示例： 
			-- 开始移动
			-- local action = transition.moveTo(sprite, {time = 2.0, x = 100, y = 100}) 
			-- transition.removeAction(action) -- 停止移动	

	-- transition.stopTarget 
	-- 停止一个显示对象上所有正在执行的动作。 
		-- 用法示例： 
		-- local action =  transition.moveTo(sprite, {time = 1.0, x = 100, y = 100})
		-- transition.fadeOut(sprite, {time = 2.0}) 
		-- transition.stopTarget(sprite)		
		-- 注意： 显示对象的 performWithDelay() 方法是用动作来实现延时回调操作的，
		-- 所以如果停止显示对象上的所有动作，
		-- 会清除该对象上的延时回调操作。		

	-- transition.pauseTarget 
	-- 暂停显示对象上所有正在执行的动作。 
			-- 格式： 
			-- transition.pauseTarget(sprite)   --有问题 
			-- sprite:stopActionByTag(13)

	-- transition.resumeTarget 
	-- 恢复显示对象上所有暂停的动作。 
			-- 格式： 
			-- transition.resumeTarget(sprite) --有问题 

	newScene:addChild(layer)
    return newScene
end 

function TestActionMain()  
	local scene = CCScene:create() 
    scene:addChild(runTestAction()) 
	return scene
end 