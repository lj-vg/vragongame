--带label步进组件
-- Author: anjun
-- Date: 2014-06-20 18:23:46
--
local Stepper = import(".Stepper")
local LabelStepper = class("LabelStepper", Stepper)
function LabelStepper:ctor(minusSprite,plusSprite,textFieldSize,buttonGape,lableGape)  
	self._lableGape = lableGape or 0
if (type(minusSprite) == "string") then
	minusSprite = extendUI.IconButton.new(minusSprite, {scale9 = true})
	        :setButtonSize(60, 38)
	        :onButtonPressed(function(event) 
	          event.target:setOpacity(128) 
	        end)
	        :onButtonRelease(function(event)
	          event.target:setOpacity(255) 
	        end)
	        :onButtonClicked(function(event)  
	        end)
	        :setButtonLabel(ui.newTTFLabel({
	            text = "+",
	            size = 40, 
	        }))
end
if (type(plusSprite) == "string") then
	plusSprite = extendUI.IconButton.new(plusSprite, {scale9 = true})
	        :setButtonSize(60, 38)
	        :onButtonPressed(function(event) 
	          event.target:setOpacity(128)
	        end)
	        :onButtonRelease(function(event)
	          event.target:setOpacity(255)
	        end)
	        :onButtonClicked(function(event)   
	        end)
	        :setButtonLabel(ui.newTTFLabel({
	            text = "-",
	            size = 40, 
	        }))
end
	LabelStepper.super.ctor(self,minusSprite,plusSprite,textFieldSize,buttonGape)
end

function LabelStepper:initView(minusSprite,plusSprite,textFieldSize,buttonGape)
	LabelStepper.super.initView(self,minusSprite,plusSprite,textFieldSize,buttonGape)
	self._labelTitle = cc.ui.UILabel.new({ text = "",
										size = 20,
										})
	self:addChild( self._labelTitle )	 
end

function LabelStepper:setLableGape( gape )
	self._lableGape = gape
	self:fixPos();
	return self
end

function LabelStepper:setLabelTitleTxt(titleLbl) 
	self._labelTitle:setString(titleLbl)
	self:fixPos()
	return self
end

function LabelStepper:setLabelTitleSize(fontSize) 
	self._labelTitle:setFontSize(fontSize)
	self:fixPos()
	return self
end

function LabelStepper:setLabelTitleColor(color) 
	self._labelTitle:setColor(color) 
	return self
end

function LabelStepper:fixPos()
	local x,y = 0,self:getContentSize().height/2

	self._labelTitle:align(display.CENTER_LEFT, x, y) 
	x = x + self._labelTitle:getContentSize().width + self._lableGape
	self._textFieldInput:align(display.CENTER_LEFT, x, y )

	x = x + self._textFieldInput:getContentSize().width +self._buttonGape
	self._minusSprite:align(display.CENTER_LEFT, x, y)

	x = x + self._minusSprite:getContentSize().width + self._buttonGape
	self._plusSprite:align(display.CENTER_LEFT, x, y)
end

function LabelStepper:getContentSize()  
	local w = self._labelTitle:getContentSize().width + self._textFieldInput:getContentSize().width + self._buttonGape + self._minusSprite:getContentSize().width +
			  self._buttonGape + self._plusSprite:getContentSize().width +  self._buttonGape
local h = self._textFieldInput:getContentSize().height --取三者最高
	local contentSize = CCSize(w,h)
	return contentSize 
end

return LabelStepper
