--loading管理器 
-- Author: anjun
-- Date: 2014-04-05 19:13:51
-- 
local LoadingItem = require("app.scenes.items.LoadingItem")

local LoadingManager = class("LoadingManager")
LoadingManager.LoadingManager_instance = nil
function LoadingManager.getInstance()  
    if(not LoadingManager_instance) then 
        LoadingManager_instance = LoadingManager.new() 
    end
    return LoadingManager_instance
end

--显示加载loading
function LoadingManager:showLoading( msg,color ) 
	if ( not CCDirector:sharedDirector():getRunningScene()) then return end	
	msg = tostring(msg)  
	if ( not self._loadingItem ) then
		self._loadingItem = LoadingItem.new() 
	self._loadingItem:update(msg) 
		managers.LayerManager:addChildToLayer(UI_LAYER, self._loadingItem,true,true)		
	end 
	color = color or display.COLOR_BLACK
	self._loadingItem:setLblColor(color)
	self._loadingItem:update(msg) 
end
-- 隐藏loading
function LoadingManager:hideLoading()
	local flag = false 
	if (self._loadingItem) then
		flag= managers.LayerManager:removeChildFromLayer(self._loadingItem,true)
	end  
	self._loadingItem = nil		 
end

-- 显示miniloading
function LoadingManager:showMiniLoading( sprite,lbl,x,y) 
	self:hideMiniLoading(sprite)
	x = x or sprite:getContentSize().width/2
	y = y or  sprite:getContentSize().height/2
	local sp = display.newSprite() 
	sprite:addChild(sp,100,9999)
	sp:align(display.BOTTOM_LEFT, x, y)
	if (lbl) then
	    local label = ui.newTTFLabel({
	        text = lbl,
	        size = 88,
	        x = 128,
	        y = 56,
	        -- align = ui.TEXT_ALIGN_CENTER,
	        color = display.COLOR_BLACK
	    })
	    sp:addChild(label)
	end 

    local frames = extendUI.Common.newFrames2("s%02d.png", 1)
    local animation = display.newAnimation(frames, 0.5 / 4) -- 0.5s play 20 frames
    sp:playAnimationForever(animation)    
    sp:setScaleX(0.2)
    sp:setScaleY(0.2)
end 

-- 隐藏Miniloading
function LoadingManager:hideMiniLoading(sprite) 
	local  sp  = sprite:getChildByTag(9999)
 	if (sp) then 
 		sp:stopAllActions();
 		sp:removeSelf(true)
 	end
 	sp = nil
end

return LoadingManager 