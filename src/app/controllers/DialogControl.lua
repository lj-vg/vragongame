--
-- Author: msdnet
-- Date: 2014-01-22 13:54:24
--
-- 窗體控制器
local DialogControl =class('DialogControl') 
function DialogControl:ctor()
    self.currWindow = nil  
end 

-- 顯示指定窗體(不切换场景)
function DialogControl:showWindow( id,params ) 
    assert(self[id] ~= nil,id..' ShowWindow is nil')  
    local window = self[id].new(params)
    if (window) then
        local scene = CCDirector:sharedDirector():getRunningScene() 
        scene:addChild( window )
        self.currWindow = {id =id, params =params, type = "showWindow"} 
        local x =  (params and params.x) or 0 
        local y = (params and params.y) or FinanceUtils.ToolBarHeight 
        window:pos(x, y)   
    end
    return window 
end

-- 顯示指定场景窗體(切换场景)
function DialogControl:showSceneWindow(id,params) 
    assert(self[id] ~= nil,id..' showSceneWindow is nil')    
    local window = self[id].new(params)
    if (window) then
        local scene = display.newScene("ShowScene")   
        local toolBarPnl = self:showToolBar()
        scene:addChild(toolBarPnl,1)
        
        scene:addChild( window,0 )
        self.currWindow = {id =id, params =params, type = "showSceneWindow"}
        app:remberScene("ShowScene",self.currWindow)
        display.replaceScene(scene)           
        local x =  (params and params.x) or 0 
        local y = (params and params.y) or FinanceUtils.ToolBarHeight 
        window:pos(x, y) 
    end
    return window 
end
 -- 顯示指定场景窗體(切换场景,包含動畫地)
function DialogControl:showTransitionSceneWindow(id,params, args,transitionType, time, more) 
    assert(self[id] ~= nil,id..' showTransitionSceneWindow is nil')    
    local window = self[id].new(params)
    if (window) then
        local scene = display.newScene("showTransitionSceneWindow")   
        local toolBarPnl = self:showToolBar()
        scene:addChild(toolBarPnl,1)
        
        scene:addChild( window,0 )
        self.currWindow = {id =id, params =params, type = "showTransitionSceneWindow"}
        app:remberScene("showTransitionSceneWindow",self.currWindow)
        display.replaceScene(scene)           
        local x =  (params and params.x) or 0 
        local y = (params and params.y) or FinanceUtils.ToolBarHeight 
        window:pos(x, y) 
    end
    return window     
end

-- 显示全局弹窗面板
function DialogControl:showGlobalPanel(id,info)
    assert(self[id] ~= nil,id..' Panel is nil')
    local pnl = nil
    pnl = self[id].new(info)
    pnl:show();
end  

-- 显示窗体效果
function DialogControl:showEffect( window,toPosX,toPosY,args ) 
    local sequence = transition.sequence({  
        CCFadeIn:create(0.1),
        CCFadeTo:create(0.1,128),
        CCMoveTo:create(0.1, ccp(toPosX,toPosY)), 
        CCFadeTo:create(0.1,255),
    })
    transition.execute(window, sequence, args)
end

function DialogControl:hideEffect( window,toPosX,toPosY,args )
    local sequence = transition.sequence({
        CCFadeTo:create(0.1,128),
        CCMoveTo:create(0.1, ccp(toPosX,toPosY)), 
    })
    transition.execute(window, sequence, args) 
end

return DialogControl