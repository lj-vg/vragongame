--分页组件
-- 借鉴--UICheckBoxButtonGroup(基本上抄袭^_^
--其实，可以继承的，But... )
-- Author: anjun
-- Date: 2014-03-04 00:33:43
--
local UIBoxLayout = import(".ExtendUIBoxLayout")
local UIButton =  require("framework.cc.ui.UIButton")
local UIGroup = cc.ui.UIGroup

local TabGroup = class("TabGroup",UIGroup) 
TabGroup.BUTTON_SELECT_CHANGED = "BUTTON_SELECT_CHANGED" 

function TabGroup:ctor(direction,selectedImage)
    TabGroup.super.ctor(self)
    self:setLayout(UIBoxLayout.new(direction or display.LEFT_TO_RIGHT))
    self.buttons_ = {}
    self.currentSelectedIndex_ = 0
    self.playSound = false 
end

function TabGroup:addButton(button)
    self:addChild(button)
    self.buttons_[#self.buttons_ + 1] = button
    self:getLayout():addWidget(button):apply(self)
    button:onButtonClicked(handler(self, self.onButtonStateChanged_))
    button:onButtonStateChanged(handler(self, self.onButtonStateChanged_))
    return self
end 


function TabGroup:hideButtonAtIndex( index )
    local button = self.buttons_[index]
    if button then
        button:setVisible(false)
        local layout = self:getLayout()
        layout:removeWidget(button)
        layout:apply(self) 

        -- if self.currentSelectedIndex_ == index then
        --     self:updateButtonState_(nil)
        -- elseif index < self.currentSelectedIndex_ then
        --     self:updateButtonState_(self.buttons_[self.currentSelectedIndex_ - 1])
        -- end 
    end
end
function TabGroup:setButtonVisble(index,isVisble)
    if (isVisble) then
        self:showButtonAtIndex(index)
    else
        self:hideButtonAtIndex(index)
    end
end
function TabGroup:getButtonVisble(index) 
    local button = self.buttons_[index]
    return button:isVisible()
end


function TabGroup:showButtonAtIndex( index )
    local button = self.buttons_[index]
    if button then
        button:setVisible(true) 
        self:getLayout():addWidget(button,nil,index):apply(self)
    end
end

function TabGroup:removeButtonAtIndex(index)
    assert(self.buttons_[index] ~= nil, "TabGroup:removeButtonAtIndex() - invalid index")

    local button = self.buttons_[index]
    local layout = self:getLayout()
    layout:removeWidget(button)
    layout:apply(self)
    button:removeSelf()
    table.remove(self.buttons_, index)

    if self.currentSelectedIndex_ == index then
        self:updateButtonState_(nil)
    elseif index < self.currentSelectedIndex_ then
        self:updateButtonState_(self.buttons_[self.currentSelectedIndex_ - 1])
    end

    return self
end

-- function TabGroup:addWidget(widget, weight)
--     self.order_ = self.order_ + 1
--     self.widgets_[widget] = {weight = weight or 1, order = self.order_}
--     return self
-- end

function TabGroup:getButtonAtIndex(index)
    return self.buttons_[index]
end

function TabGroup:getButtonsCount()
    return #self.buttons_
end

function TabGroup:setButtonsLayoutMargin(top, right, bottom, left)
    for _, button in ipairs(self.buttons_) do

        if (button:isVisible()) then
            button:setLayoutMargin(top, right, bottom, left)
        end 
    end
    self:getLayout():apply(self)
    return self
end


function TabGroup:addButtonSelectChangedEventListener(callback, isWeakReference)
    return self:addEventListener(TabGroup.BUTTON_SELECT_CHANGED, callback, isWeakReference)
end

function TabGroup:onButtonSelectChanged(callback, isWeakReference)
    self:addButtonSelectChangedEventListener(callback, isWeakReference)
    return self
end

function TabGroup:onButtonStateChanged_(event)   
    if event.name == UIButton.STATE_CHANGED_EVENT and event.target:isButtonSelected() == false then
        return
    end 
    self:updateButtonState_(event.target)
end

function TabGroup:updateButtonState_(clickedButton)
    local currentSelectedIndex = 0

    for index, button in ipairs(self.buttons_) do 
        if button == clickedButton then 
            currentSelectedIndex = index
            if not button:isButtonSelected() then
                button:setButtonSelected(true) 
            end
        else
                if button:isButtonSelected() then
                button:setButtonSelected(false)
            end
        end 
    end 
    if self.currentSelectedIndex_ ~= currentSelectedIndex then 
        local last = self.currentSelectedIndex_
        self.currentSelectedIndex_ = currentSelectedIndex
        self:dispatchEvent({name = TabGroup.BUTTON_SELECT_CHANGED, selected = currentSelectedIndex, last = last})
    end
end

function TabGroup:getCurrentSelectedIndex()
    return self.currentSelectedIndex_ ;
end
--
function TabGroup:setCurrentSelectedIndex( selectedIndex )
    self.currentSelectedIndex_ = selectedIndex
end
function TabGroup:getContentSize()
    local w ,h = self:getLayoutSize()
    -- print("sfsdf 戲",w,h)  --這裡有問題? 好像是有占点蛋疼....
    local size = CCSize(w,h)
    return size
end

return TabGroup 