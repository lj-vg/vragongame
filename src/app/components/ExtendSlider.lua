--扩展Slider,官方的不够用了．
-- Author: anjun
-- Date: 2014-03-13 20:21:48
-- 

local UISlider =  cc.ui.UISlider
local ExtendSlider = class("ExtendSlider", UISlider)
function ExtendSlider:ctor(direction, images, options)
	self._offsetMin = tonum(options.min or 0)
	self._offsetMax = tonum(options.max or 100) 
	self._isFixOffset = false
	ExtendSlider.super.ctor(self,direction, images, options)
    self:align(display.CENTER,0,0)  --why CENTER
end

-- 实在想不出好办法了．．＾＿＾！！！！
-- OO不够还挺好用的ＯＯ
--后续应该在这个函数处理 updateButtonPosition_

--得到SliderValue校正偏移 
function ExtendSlider:getMaxMinOffset()
    return self._offsetMin, self._offsetMax
end
-- 校正SliderValue偏移
function ExtendSlider:setMaxMinOffset(min, max)
	self._offsetMin = min
	self._offsetMax = max
	self._isFixOffset = true
    return self
end 

-- override 
function ExtendSlider:setSliderValue(value)
    -- ExtendSlider.super.setSliderValue(self,value)

    assert(value >= self.min_ and value <= self.max_, "UISlider:setSliderValue() - invalid value")
    -- anjun start 
    if (value <  self._offsetMin ) then
    	value = self._offsetMin 
    end
    if (value >  self._offsetMax) then
    	value = self._offsetMax 
    end
    -- 不是很明白，不过达到效果了．哈哈哈！！！
    local rate = (self._offsetMax - self._offsetMin)/(self.max_ - self.min_)
    local offsetValue = self._offsetMin/rate 
    -- anjun edn

    if self.value_ ~= value then
        self.value_ = value
        self:updateButtonPosition_()    
        -- anjun start
		local realValue = (self._isFixOffset and (value/rate - offsetValue)+self.min_) or self.value_ 
        self:dispatchEvent({name = UISlider.VALUE_CHANGED_EVENT, value =realValue})
        -- anjun ednt
    end
    return self

end

function ExtendSlider:setSliderSize(width, height)
    -- ExtendSlider.super.setSliderSize(self,width, height)

    if self.isScale9_ then
	    self.scale9Size_ = {width, height}
	    if self.barSprite_ then
	        self.barSprite_:setContentSize(CCSize(self.scale9Size_[1], self.scale9Size_[2]))
	    end
    else 
        local boundingSize = self.barSprite_:getContentSize()
        local sx = width / (boundingSize.width / self:getScaleX())
        local sy = height / (boundingSize.height / self:getScaleY())
        if sx > 0 and sy > 0 then
            self:setScaleX(sx)
            self:setScaleY(sy)
        end
    end    

    return self
end 

function ExtendSlider:getContentSize()
    local sx = self:getScaleX()
    local sy = self:getScaleY()
    local boundingSize = self.barSprite_:getContentSize()
    local realSize = CCSize( math.ceil(boundingSize.width*sx) ,math.ceil(boundingSize.height*sy))

    return realSize
end

function ExtendSlider:updateButtonPosition_()
	ExtendSlider.super.updateButtonPosition_(self)
end

return ExtendSlider

