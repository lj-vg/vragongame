--选择按纽<>  
-- Author: anjun
-- Date: 2014-03-05 14:07:41
-- 暂时
local UIButton =  require("framework.cc.ui.UIButton")
local SelectButton = class("SelectButton", UIButton)  

SelectButton.OFF          = "off"
SelectButton.OFF_PRESSED  = "off_pressed"
SelectButton.OFF_DISABLED = "off_disabled"
SelectButton.ON           = "on"
SelectButton.ON_PRESSED   = "on_pressed"
SelectButton.ON_DISABLED  = "on_disabled"

function SelectButton:ctor(images, options)
    SelectButton.super.ctor(self, {
        {name = "disable",  from = {"off", "off_pressed"}, to = "off_disabled"},
        {name = "disable",  from = {"on", "on_pressed"},   to = "on_disabled"},
        {name = "enable",   from = {"off_disabled"}, to = "off"},
        {name = "enable",   from = {"on_disabled"},  to = "on"},
        {name = "press",    from = "off", to = "off_pressed"},
        {name = "press",    from = "on",  to = "on_pressed"},
        {name = "release",  from = "off_pressed", to = "off"},
        {name = "release",  from = "on_pressed", to = "on"},
        {name = "select",   from = "off", to = "on"},
        {name = "select",   from = "off_disabled", to = "on_disabled"},
        {name = "unselect", from = "on", to = "off"},
        {name = "unselect", from = "on_disabled", to = "off_disabled"},
    }, "off", options)

    extendUI.Common.makeNormalBtn(self)  -- this is bad design so...
    if(not images)then
        return
    end
    self:setButtonImage(SelectButton.ON, images["on"], true)
    self:setButtonImage(SelectButton.OFF, images["off"], true)
    self:setButtonImage(SelectButton.OFF_PRESSED, images["off_pressed"], true)
    self:setButtonImage(SelectButton.OFF_DISABLED, images["off_disabled"], true)
    -- self:setButtonImage(SelectButton.ON, images["on"], true)
    self:setButtonImage(SelectButton.ON_PRESSED, images["on_pressed"], true)
    self:setButtonImage(SelectButton.ON_DISABLED, images["on_disabled"], true) 
end

function SelectButton:setButtonImage(state, image, ignoreEmpty)
    assert(state == SelectButton.OFF
        or state == SelectButton.OFF_PRESSED
        or state == SelectButton.OFF_DISABLED
        or state == SelectButton.ON
        or state == SelectButton.ON_PRESSED
        or state == SelectButton.ON_DISABLED,
        string.format("SelectButton:setButtonImage() - invalid state %s", tostring(state)))
    SelectButton.super.setButtonImage(self, state, image, ignoreEmpty)
    if state == SelectButton.OFF then
        if not self.images_[SelectButton.OFF_PRESSED] then
            self.images_[SelectButton.OFF_PRESSED] = image
        end
        if not self.images_[SelectButton.OFF_DISABLED] then
            self.images_[SelectButton.OFF_DISABLED] = image
        end
    elseif state == SelectButton.ON then
        if not self.images_[SelectButton.ON_PRESSED] then
            self.images_[SelectButton.ON_PRESSED] = image
        end
        if not self.images_[SelectButton.ON_DISABLED] then
            self.images_[SelectButton.ON_DISABLED] = image
        end
    end

    return self
end

function SelectButton:isButtonSelected()
    return self.fsm_:canDoEvent("unselect")
end

function SelectButton:setButtonSelected(selected)
    if self:isButtonSelected() ~= selected then
        if selected then
            self.fsm_:doEventForce("select")
        else
            self.fsm_:doEventForce("unselect")
        end
        self:dispatchEvent({name = UIButton.STATE_CHANGED_EVENT, state = self.fsm_:getState()})
    end
    return self
end

function SelectButton:onTouch_(event,x,y) 
    -- local name, x, y = event.name, event.x, event.y
    if event == "began" then
        if not self:checkTouchInSprite_(x, y) then return false end
        self.fsm_:doEvent("press")
        self:dispatchEvent({name = UIButton.PRESSED_EVENT, x = x, y = y, touchInTarget = true})
        return true
    end

    local touchInTarget = self:checkTouchInSprite_(x, y)
    if event == "moved" then
        if touchInTarget and self.fsm_:canDoEvent("press") then
            self.fsm_:doEvent("press")
            self:dispatchEvent({name = UIButton.PRESSED_EVENT, x = x, y = y, touchInTarget = true})
        elseif not touchInTarget and self.fsm_:canDoEvent("release") then
            self.fsm_:doEvent("release")
            self:dispatchEvent({name = UIButton.RELEASE_EVENT, x = x, y = y, touchInTarget = false})
        end
    else
        if self.fsm_:canDoEvent("release") then
            self.fsm_:doEvent("release")
            self:dispatchEvent({name = UIButton.RELEASE_EVENT, x = x, y = y, touchInTarget = touchInTarget})
        end
        if event == "ended" and touchInTarget then
            self:setButtonSelected(self.fsm_:canDoEvent("select"))
            self:dispatchEvent({name = UIButton.CLICKED_EVENT, x = x, y = y, touchInTarget = true})
        end
    end
end

function SelectButton:getDefaultState_()
    local state = self.fsm_:getState()
    if state == SelectButton.ON or state == SelectButton.ON_DISABLED or state == SelectButton.ON_PRESSED then
        return {SelectButton.ON, SelectButton.OFF}
    else
        return {SelectButton.OFF, SelectButton.ON}
    end
end 

-- override
function SelectButton:updateButtonImage_()
    local state = self.fsm_:getState()
    local image = self.images_[state] 
    if not image then
        for _, s in pairs(self:getDefaultState_()) do
            image = self.images_[s]
            if image then break end
        end
    end 
    if image then 
        if self.currentImage_ ~= image then
            if self.sprite_ then
                self.sprite_:removeFromParentAndCleanup(true)
                self.sprite_ = nil
            end
            self.currentImage_ = image
            local isEmpty = (string.len(image) == 0 and true ) or false
            if self.scale9_ then
                if ( not isEmpty) then 
                    self.sprite_ = display.newScale9Sprite(image)
                    if not self.scale9Size_ then
                        local size = self.sprite_:getContentSize()
                        self.scale9Size_ = {size.width, size.height}
                    else
                        self.sprite_:setContentSize(CCSize(self.scale9Size_[1], self.scale9Size_[2]))
                    end
                else    --image == ""
                    self.sprite_ = display.newSprite() 
                    if not self.scale9Size_ then  
                        local size = self:getMaxContentSize()
                        self.sprite_:setContentSize(size ) 
                        self.scale9Size_ = {size.width, size.height} 
                    else 
                        self.sprite_:setContentSize(CCSize(self.scale9Size_[1], self.scale9Size_[2]))
                        local size = self.sprite_:getContentSize()
                        self.scale9Size_ = {size.width, size.height}  
                    end
                    self.sprite_:setCascadeBoundingBox(CCRectMake(0, 0, self.scale9Size_[1], self.scale9Size_[2]))
                end 
            else
                if (not isEmpty) then
                    self.sprite_ = display.newSprite(image)
                else --image == ""
                    self.sprite_ = display.newSprite() 
                    if not self.scale9Size_ then  
                        local size = self:getMaxContentSize()
                        self.sprite_:setContentSize(size ) 
                        self.scale9Size_ = {size.width, size.height}
                    else 
                        self.sprite_:setContentSize(CCSize(self.scale9Size_[1], self.scale9Size_[2]))
                        local size = self.sprite_:getContentSize()
                        self.scale9Size_ = {size.width, size.height}  
                    end
                    self.sprite_:setCascadeBoundingBox(CCRectMake(0, 0, self.scale9Size_[1], self.scale9Size_[2]))
                end                
            end 
            self:addChild(self.sprite_, UIButton.IMAGE_ZORDER)
        end

        self.sprite_:setAnchorPoint(self:getAnchorPoint())
        self.sprite_:setPosition(0, 0)
    else 
        -- echoError("SelectButton:updateButtonImage_() - not set image for state %s", state)
    end
end

-- 取所有状态图片最大的宽与高
-- <br>还有个隐患:on; off 先后的问题，后续...
function SelectButton:getMaxContentSize( )
    local image,size,tmpSprite
    size = CCSize(0,0)
    for _, image in pairs(self.images_) do  
        if image and string.len(image) ~= 0 then 
            tmpSprite = display.newSprite(image) 
            if (tmpSprite:getContentSize().width > size.width) then
                size.width = tmpSprite:getContentSize().width
            end
            if (tmpSprite:getContentSize().height > size.height ) then
                size.height =  tmpSprite:getContentSize().height
            end
        end
    end 
    tmpSprite = nil 
    return size
end

function SelectButton:getContentSize() 
    print("SelectButton:getContentSize",self.sprite_:getContentSize().width)  
    return self.sprite_:getContentSize();
end

return SelectButton