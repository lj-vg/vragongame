-- 封装ListView 
-- Author: anjun
-- Date: 2014-06-05 10:23:53
--
local ListView = class("ListView", function()
    return display.newNode()
end)

function ListView:ctor(options) 
	self._callBackHandle = options.callBackHandle 
	self:initView(options)
	self:initEvent()
end

function ListView:__listhandle( fn, table, a1, a2 ) 
	local r = nil 
	if (self._callBackHandle) then
		r = self._callBackHandle(fn, table, a1, a2)
	end
	return r
end

function ListView:initView(options)
	local listBg = options.listBg or display.newSprite()
	local size = options.size or CCSizeMake(display.width, display.height) 
	self._bg = options.bg or display.newSprite()	
	self._listHandle = CustomEventHandler:create( handler(self, self.__listhandle)  )
	self._list = CustomTableView:createWithHandler(self._listHandle, size,listBg )
	self._list:setBounceable(true) 
	self._list:setDirection(kCCScrollViewDirectionHorizontal)
    self._list:setAnchorPoint(CCPointMake(0, 0))
    self._list:setPosition(CCPointMake(0,0)) 
    self._list:setVerticalFillOrder(kCCTableViewFillTopDown)
    self._list:setClippingToBounds(true) 
    self._list:setBounceable(true) 	
    self._bg:addChild(self._list)  
    self:addChild(self._bg)   
    self._bg:align(display.BOTTOM_LEFT, 0,0)
end

function ListView:show(isMask,isLock)
	self:setVisible(true)
	self._list:setTouchEnabled(true)
	isMask = ( (isMask~=false) and true ) or false 
	if ( isMask ) then
		self:drawMask(display.width,display.height)
	end 
	self._isLock = ( (isLock~=false) and true ) or false  
	if ( self._isLock ) then
		app:enableScrollTableView(false) 
	end 
end

function ListView:hide()
	self:setVisible(false)
	self._list:setTouchEnabled(false)
	self:removeMask()
	if ( self._isLock ) then
		app:enableScrollTableView(true) 
	end 	 
end

-- 画遮罩
function ListView:drawMask(w, h)   
	self.mask_ = display.newSprite()
    self.mask_:setCascadeBoundingBox(CCRectMake(0, 0, w, h))
    self.mask_:setTouchEnabled(true)

    self.mask_:addTouchEventListener(function(event)
        if event == "began" then
            return true
        elseif event == "ended" then
            print("ListView Mask layer touch ended......")
        end
    end) 
    -- self:addChild(self.mask_,-1) 
	managers.LayerManager:addChildToLayer(UI_LAYER,self.mask_,false,false )         
end 
 -- 移除遮罩
function ListView:removeMask()
	if( self.mask_) then 
		self.mask_:removeTouchEventListener();	
		self.mask_:removeFromParentAndCleanup(true) 
		self.mask_ = nil; 
	end
end 

function ListView:getListView() 
	return self._list
end

-- 
function ListView:initEvent()

end 

function ListView:dispose() 

end

return ListView