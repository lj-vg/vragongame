--数据网格
--由２个CustomTableViewTitle合成
-- Author: anjun
-- Date: 2014-02-17 00:00:52
-- 
local scheduler = require("framework.scheduler")
local CustomDataGrid = class("CustomDataGrid", function()
    return display.newNode()  --用这个Ｔouch事件会用问题
    -- return display.newLayer()
end)
function CustomDataGrid:ctor(size,callFun,_leftTitleSprite,_rightTitleSprite) 
	self._leftCustomTableView= nil 
	self._rightCustomTableView = nil  
    self:setContentSize(size)
    self._handleCallFun = callFun    
    local leftTitleSprite = _leftTitleSprite
    local rightTitleSprite = _rightTitleSprite 
    local function defaultTitle()
        leftTitleSprite = display.newSprite()
        leftTitleSprite:setContentSize(CCSize(60,0))
        rightTitleSprite = display.newSprite()
    end   
    if ( (not leftTitleSprite) and (not rightTitleSprite) ) then
        defaultTitle()
    end
    size.height = size.height - leftTitleSprite:getContentSize().height
	self:initView(size,leftTitleSprite,rightTitleSprite )  
    self:initEvent()    
end 

-- handle
function CustomDataGrid:__rightHandle( fn, table, a1, a2,titleItemCount )
    if (fn == "scroll") then
        local offSet = table:getContentOffset()   
        local offSetOther = nil
        if ( self._leftCustomTableView ~= nil ) then  --关联左边
            offSetOther = self._leftCustomTableView:getContentOffset()
            if (offSet.y ~= offSetOther.y) then 
                self._leftCustomTableView:setContentOffset(CCPoint(offSetOther.x, offSet.y));  
            end          
        end
    end

    if ( self._handleCallFun ) then        
        return self._handleCallFun( "R",fn, table, a1, a2,titleItemCount)
    end 
end

function CustomDataGrid:__leftHandle( fn, table, a1, a2,titleItemCount )
    if (fn == "scroll") then
-- local offSet = table:getContentOffset()   
-- if ( self._rightCustomTableView ~= nil ) then --关联右边
--     local offSetOther = self._rightCustomTableView:getContentOffset()
--     if (offSet.y ~= offSetOther.y) then 
--         self._rightCustomTableView:setContentOffset(CCPoint(offSetOther.x, offSet.y));  
--     end          
-- end
    end
    if ( self._handleCallFun ) then
        return self._handleCallFun( "L",fn, table, a1, a2,titleItemCount )
    end
end

function CustomDataGrid:initView(size,leftTitleSprite,rightTitleSprite)    
    local leftSize = leftTitleSprite:getContentSize()
    local rightTableViewSize = CCSize(size.width -leftSize.width,size.height )
    local leftTableViewSize = CCSize(leftSize.width,size.height )
    -- right
    self._rightCustomTableView = extendUI.CustomTableViewTitle.new(handler(self, self.__rightHandle), rightTableViewSize, rightTitleSprite )
    self._rightCustomTableView:setPosition( CCPointMake(leftSize.width, 0) )
    self:addChild(self._rightCustomTableView)
-- self._rightCustomTableView:debugGrapic() 
    -- left     
    self._leftCustomTableView = extendUI.CustomTableViewTitle.new(handler(self, self.__leftHandle), leftTableViewSize, leftTitleSprite )
    self._leftCustomTableView:enableControl(false) --表左不主动
    self:addChild(self._leftCustomTableView)  
end

function CustomDataGrid:initEvent()  
end 

function CustomDataGrid:reloadData() 
    self._leftCustomTableView:reloadData()
    self._rightCustomTableView:reloadData()
end
function CustomDataGrid:gridCellAtIndex( index ) 
    local leftCell,rightCell = self._leftCustomTableView:cellAtIndex(index),self._rightCustomTableView:cellAtIndex(index)
    return leftCell,rightCell
end

function CustomDataGrid:enableControl( flag ) 
    self._rightCustomTableView:enableControl(flag)
end 

function CustomDataGrid:isScrolling() 
    return  self._rightCustomTableView:isScrolling()
end

return CustomDataGrid