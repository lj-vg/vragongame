--提示窗口管理器 
-- Author: anjun
-- Date: 2014-01-26 19:07:15
--
local AlertManager = class("AlertManager")
AlertManager.AlertManager_instance = nil

function AlertManager.getInstance()  
    if(not AlertManager_instance) then 
        AlertManager_instance = AlertManager.new() 
    end
    return AlertManager_instance
end

-- 简单文本提示框（OK,Cancel） 	
function AlertManager:showMsgAlert( option )  
	return self:createAlert(option);
end
--  only OK button
function AlertManager:showMsgAlertOK( option ) 
	option.showCancel = false 
	-- option.showOk = false
	return self:createAlert(option);	
end 
-- 文本提示框 
-- 需要优化放后
function AlertManager:createAlert( params ) 
	local alert =AlertWindow.new()  
    local msg       = params.msg or ""
    local okLable = params.okLable or "確定"
    local cancelLable = params.cancelLable or "取消"
    local closeButton 	   = params.closeButton or  cc.ui.UIPushButton.new(COMMON_BUTTON_EXIT)  
    local isHasClose = ( (params.isHasClose~=false) and true ) or false 
    local buttonToBottom   = params.buttonToBottom or 25
    local buttonGape  	   = params.buttonGape or 25
    Common.getNormalBtnSize(closeButton) 
	 local backgroundSize = params.backgroundSize or cc.size(400,300)
	 local titleBarSize = params.titleBarSize or cc.size(300,58)
	 local contentSize = params.contentSize or cc.size(390,240) 

    local background  = params.background or ( cc.ui.UIImage.new(COMMON_WINDOW_A_BG9, {scale9 = true})
            								 :setLayoutSize(backgroundSize.width,backgroundSize.height) )
    local okButton    = params.okButton or ( cc.ui.UIPushButton.new(COMMON_BUTTON_YELLOW, {scale9 = true}) 
										    :setButtonLabel(cc.ui.UILabel.new({
										        text = okLable,
										        size = 25, 
										    })) )	 
    Common.getNormalBtnSize(okButton)
    local cancelButton = params.cancelButton or( cc.ui.UIPushButton.new(COMMON_BUTTON_GREEN, {scale9 = true}) 
											    :setButtonLabel(cc.ui.UILabel.new({
											        text = cancelLable,
											        size = 25, 
											    })) )
    Common.getNormalBtnSize(cancelButton) 
    local showCancel = ( (params.showCancel~=false) and true ) or false 
    local showOk = ( (params.showOk~=false) and true ) or false
    local titleBar    = params.titleBar or ( cc.ui.UIImage.new(COMMON_WINDOW_A_TITLE9, {scale9 = true})
                							:setLayoutSize(titleBarSize.width,titleBarSize.height) )
    local titleTxt 	  = params.titleTxt or "提示" 
    local isMask = ( (params.isMask~=false) and true ) or false 
    local isCeneter = ( (params.isCeneter~=false) and true ) or false
	local contentSprite = params.contentSprite or display.newScale9Sprite(COMMON_WINDOW_A_PANEL9, 0, 0, cc.size(contentSize.width,contentSize.height))
	-- cc.ui.UIImage.new(COMMON_WINDOW_A_PANEL9, {scale9 = true})  --do you know why 0  ?  
 	--                    :setLayoutSize() 	 
 	contentSprite:setAnchorPoint( cc.p(0,0))

 	alert:setBackground(background) 
	alert:setContentSprite(contentSprite) 	 
	alert:setTitleBar(titleBar)  
	alert:setCloseButton(closeButton) 
	alert:setIsHasClose(isHasClose) 
	alert:setButtonToBottom(buttonToBottom)
	alert:setButtonGape(buttonGape)  
	alert:setCancelButton(cancelButton)
	alert:setOkButton(okButton) 
	alert:setShowOK(showOk) 
	alert:setShowCancel(showCancel)  

	local _callBackFun = params.callBack 
	local function __alertRepons( e ) 
		local alertWindow = e[1]
		if (alertWindow ~= nil and isMask) then				 
			LayerManager:removeMaskFromLayer()
		end
		if (_callBackFun ~= nil ) then				
			_callBackFun(e);
			_callBackFun = nil;
		end	 
		if (params.okCallFun and e[2]) then
			params.okCallFun(e)
		end
		if (params.cancelCallFun and (not e[2])) then
			params.cancelCallFun(e)
		end
	end
	alert:setCallBackFunction(__alertRepons)  

	local lbl = cc.ui.UILabel.new({
        text =msg,
        size=22,
        }) 
        :align(display.BOTTOM_LEFT)    
   	alert:addContentChild(lbl,1)        --do you know why 1  ?   
	local x = (alert:getWinContentSize().width -  lbl:getContentSize().width) /2
	local y = 0
	if ( showOk ) then
		y = okButton:getContentSize().height 
	elseif showCancel then 
		y = cancelButton:getContentSize().height 
	end  
	y =  y + buttonToBottom+(alert:getWinContentSize().height -y -buttonToBottom -lbl:getContentSize().height)/2 
	lbl:pos(x, y)
	LayerManager:addChildToLayer( {child=alert,isCeneter=isCeneter,isMask=isMask} )   
	-- alert:addContentChild(lbl)  --do you know why 2 ?  
	alert:setTitleTxtStr(titleTxt) --do you know why 3 ?  
	-- Common.debugDraw(alert.contentSprite_)
	return alert;
end  

-- 显示消息提示框		
function AlertManager:showMsgTip( option )
	local msg = option.msg or ""
	local isHide = ( (option.isHide~=false) and true ) or false
	local showTime = option.showTime or 0.5 
	local liveTime = option.liveTime or (showTime+1)  
	if ( self.alertMsgTips_) then 
		self:hideMsgTip();
	end
	local lbl = cc.ui.UILabel.new({
        text =msg,
        size=26,
        }) 
        :align(display.LEFT_CENTER) 
	self.alertMsgTips_ =AlertWindow.new()
	local background = cc.ui.UIImage.new(ROOM_HINT_BG9, {scale9 = true})
            							:setLayoutSize(lbl:getContentSize().width+30,lbl:getContentSize().height+30) 
	self.alertMsgTips_:setBackground( background )
	self.alertMsgTips_:setShowCancel(false)
	self.alertMsgTips_:setShowOK(false) 
	self.alertMsgTips_:setIsHasClose(false)

    self.alertMsgTips_:addBackgroundChild(lbl) 
	local x = (self.alertMsgTips_:getContentSize().width -  lbl:getContentSize().width) /2
	local y = self.alertMsgTips_:getContentSize().height /2    
	lbl:pos(x,y)
	LayerManager:addChildToLayer( {child=self.alertMsgTips_,isCeneter=true} )  
    self.alertMsgTips_:setPositionY( display.top )  
    -- effect show
    local spawn = Common.Spawn({   --执行 
        cc.FadeIn:create(showTime),
    	cc.MoveTo:create(showTime, cc.p(self.alertMsgTips_:getPositionX(), display.top - self.alertMsgTips_:getContentSize().height)),   

    })         
    self.alertMsgTips_:setOpacity(0)
    transition.execute(self.alertMsgTips_, spawn, {time=showTime,easing = "IN"})	  
	if (isHide) then -- 自动消失		
	   	local __removeTip = function () 
	  		self:hideMsgTip( showTime )	 
	  		removeTipHander = nil		 
		end
		local removeTipHander =scheduler.performWithDelayGlobal(__removeTip, liveTime)				
	end
    self:clearTarget(self.alertMsgTips_,function() 
		if (removeTipHander) then
			scheduler.unscheduleGlobal(removeTipHander) 
		end
		removeTipHander = nil    	
    end) 
	return self.alertMsgTips_
end	

-- 隐藏消息提示框
-- 与上面的成对使用 
function AlertManager:hideMsgTip(hidetime)
	hidetime = hidetime or 0.2
	if (self.alertMsgTips_) then	
	    local function __callBack( )   
	    	LayerManager:removeChildFromLayer( self.alertMsgTips_ )
			self.alertMsgTips_ = nil	       
	    end   
	    -- local sequence = transition.sequence({   --顺序执行
	    --     -- cc.DelayTime:create( hidetime/4 ),
	    --     cc.FadeTo:create(hidetime,0),
	    -- 	cc.MoveTo:create(hidetime, cc.p(self.alertMsgTips_:getPositionX(), display.top)),   
	    -- })    
	    local spawn = Common.Spawn({   --同时执行
	    	cc.FadeTo:create(hidetime,0),   
	    	cc.MoveTo:create(hidetime, cc.p(self.alertMsgTips_:getPositionX(), display.top)),
	    })    
	    transition.execute(self.alertMsgTips_, spawn, {onComplete=__callBack,time=hidetime,easing = "OUT"})	    
	end  
end  

-- 显示居中消息提示框		
function AlertManager:showCenterMsgTip( msg,delay) 
	delay = delay or 2 
	local alertMsgTips = AlertWindow.new() 
	local lbl = cc.ui.UILabel.new({
        text =msg, 
        })     
    local size = cc.size(lbl:getContentSize().width+40,lbl:getContentSize().height+40 )
 	local background = display.newScale9Sprite("common/combox_input.png", 0, 0,size) 
 	background:setAnchorPoint( cc.p(0,0))           							
	alertMsgTips:setBackground( background )
	alertMsgTips:setShowCancel(false)
	alertMsgTips:setShowOK(false) 
	alertMsgTips:setIsHasClose(false)  
    alertMsgTips:addBackgroundChild(lbl,1) 
    lbl:align(display.CENTER,size.width/2,size.height/2) 
	LayerManager:addChildToLayer( {child=alertMsgTips,isCeneter=true} )  
    -- effect show
   	local __removeTip = function () 
		LayerManager:removeChildFromLayer( alertMsgTips ) 
		alertMsgTips = nil
		removeTipHander = nil
	end
	local removeTipHander = scheduler.performWithDelayGlobal(__removeTip, delay)  
    self:clearTarget(alertMsgTips,function () 
		if (removeTipHander) then
			scheduler.unscheduleGlobal(removeTipHander) 
		end
		removeTipHander = nil    	
    end)
	return alertMsgTips
end

function AlertManager:clearTarget(target,func) 
	target:addNodeEventListener(cc.NODE_EVENT, function(event) 
	    print(event.name)
	    if event.name == "cleanup" then
	    	func()
	    end
	end)

end 

return AlertManager 